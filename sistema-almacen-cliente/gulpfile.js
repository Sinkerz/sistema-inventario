var gulp = require("gulp");
var tslint = require('gulp-tslint');
var tsc = require("gulp-typescript");
var tsProject = tsc.createProject("tsconfig.json");
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var uglify = require('gulp-uglify');
var runSequence = require('run-sequence');
var cleanCSS = require("gulp-clean-css");
var concat = require('gulp-concat');
var replace = require('gulp-replace');
var minifyHtml = require("gulp-minify-html");

var version = '2.0';

gulp.task('tslint', function() {
    return gulp.src("src/**/*.ts")
        .pipe(tslint({
            formatter: 'prose'
        }))
        .pipe(tslint.report());
});

gulp.task("buildTS", ["tslint"], function() {
    var tsResult = gulp.src("src/**/*.ts")
        .pipe(tsProject());
    return tsResult.js
        .pipe(replace(".html'", ".html?v=" + version + "'"))
        .pipe(replace('.css', '.css?v=' + version))
        //.pipe(uglify())
        .pipe(gulp.dest("build"));
});

gulp.task('processJS', function() {
    gulp.src(["node_modules/core-js/client/shim.min.js",
            "node_modules/zone.js/dist/zone.js",
            "node_modules/systemjs/dist/system.src.js"
        ], {
            base: '.'
        })
        .pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest("build/js"));

    gulp.src("src/*.js")
        .pipe(replace('node_modules', 'js'))
        .pipe(replace('$version', version))
        .pipe(uglify())
        .pipe(gulp.dest("build"));
});

gulp.task('processCSS', function() {
    gulp.src(["node_modules/primeng/resources/themes/omega/theme.css",
            "node_modules/primeng/resources/primeng.min.css",
            "node_modules/font-awesome/css/font-awesome.css",
            "src/app/recurso/css/principal.css"
        ], {
            base: '.'
        })
        .pipe(cleanCSS())
        .pipe(concat('app.min.css'))
        .pipe(replace('../../../../font-awesome/', ''))
        .pipe(replace('../../../../../src/app/recurso', '..'))
        .pipe(gulp.dest("build/css"))
});


gulp.task('copyAngularCSS', function() {
    gulp.src("src/app/recurso/css/componente/*.css")
        .pipe(cleanCSS())
        .pipe(gulp.dest("build/app/recurso/css/componente"))
});

gulp.task("buildCSS", ['copyAngularCSS', 'processCSS']);


gulp.task('copyJSLibraries', function() {
    gulp.src("src/lib/**/*.js")
        .pipe(uglify())
        .pipe(gulp.dest("build/lib"));
});

gulp.task('copyLibraries', function() {
    gulp.src("src/lib/**/*.png")
        .pipe(gulp.dest("build/lib"));
    gulp.src("src/lib/**/*.properties")
        .pipe(gulp.dest("build/lib"));
    gulp.src("src/lib/**/*.css")
        .pipe(cleanCSS())
        .pipe(gulp.dest("build/lib"));
    gulp.src("src/lib/**/*.html")
        .pipe(minifyHtml())
        .pipe(gulp.dest("build/lib"));
});

gulp.task('copyAngularLibraries', function() {
    gulp.src(['@angular/core/bundles/core.umd.js',
            '@angular/common/bundles/common.umd.js',
            '@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http/bundles/http.umd.js',
            '@angular/router/bundles/router.umd.js',
            '@angular/forms/bundles/forms.umd.js',
            '@angular/animations/bundles/animations.umd.js',
            '@angular/animations/bundles/animations-browser.umd.js',
            '@angular/platform-browser/bundles/platform-browser-animations.umd.js',
            'rxjs/util/*.js',
            'rxjs/symbol/*.js',
            'rxjs/observable/*.js',
            'rxjs/operator/*.js',
            'rxjs/add/operator/*.js',
            'rxjs/operators/*.js',
            'primeng/components/**/*.js',
            'primeng/primeng.js',
            'rxjs/Observable.js',
            'rxjs/Subject.js',
            'rxjs/BehaviorSubject.js',
            'rxjs/Subscriber.js',
            'rxjs/InnerSubscriber.js',
            'rxjs/OuterSubscriber.js',
            'rxjs/Notification.js',
            'rxjs/Subscription.js',
            'rxjs/Observer.js',
            'rxjs/SubjectSubscription.js',
            'decimal.js/decimal.min.js'
        ], {
            cwd: "node_modules/**"
        })
        .pipe(uglify())
        .pipe(gulp.dest("build/js"));
});

gulp.task('copyHTML', function() {
    gulp.src("src/index.html")
        .pipe(replace('<!--css-url-->', '<link rel="stylesheet" type="text/css" href="css/app.min.css" />'))
        .pipe(replace('<!--js-url-->', '<script src="js/app.min.js"></script>'))
        .pipe(replace('<!--commet-before-->', '<!--'))
        .pipe(replace('<!--commet-end-->', '-->'))
        .pipe(gulp.dest("build"));

    gulp.src("src/app/**/*.html")
        // .pipe(minifyHtml())
        .pipe(gulp.dest("build/app"));

});

gulp.task('copyRSC', function() {
    gulp.src("src/app/recurso/icono/*.svg")
        .pipe(gulp.dest("build/icono"));

    gulp.src("src/app/recurso/icono/car.svg")
        .pipe(gulp.dest("build/app/recurso/icono/"));

    del("build/icono/car.svg");

    gulp.src("src/**/*.jpg")
        .pipe(gulp.dest("build"));

    gulp.src("src/**/*.png")
        .pipe(gulp.dest("build"));

    gulp.src(["node_modules/font-awesome/fonts/*.*", "node_modules/primeng/resources/themes/omega/fonts/*.*"])
        .pipe(gulp.dest("build/css/fonts"));

    return gulp.src("src/**/*.gif")
        .pipe(gulp.dest("build"));
});

gulp.task('clean', function() {
    return del("build/**");
});

gulp.task("build", ['buildTS', 'copyJSLibraries', 'copyLibraries', 'copyAngularLibraries', 'copyHTML', 'buildCSS', 'processJS', 'copyRSC']);

gulp.task('cleanBuild', function() {
    runSequence('clean', 'build');
});