import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModulo } from './app/modulo/app';

platformBrowserDynamic().bootstrapModule(AppModulo);
