import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MenubarModule, DataTableModule, SharedModule, ButtonModule, InputTextModule, FieldsetModule,
   CheckboxModule, MultiSelectModule, CalendarModule,
  TabViewModule, DropdownModule, PanelModule, SplitButtonModule, PanelMenuModule, ToolbarModule, 
  ConfirmDialogModule, ConfirmationService, DialogModule, OverlayPanelModule, DataListModule,
  TreeModule
} from 'primeng/primeng';

//Modulos
import { Route } from './route';

//Componentes
import { App } from '../componente/principal/app';
import { Menu } from '../componente/otro/menu';
import { TipoUsuarioBuscador } from '../componente/buscador/tipo.usuario.buscador';
import { TipoUsuarioFormulario } from '../componente/formulario/tipo.usuario.formulario';
import { TipoUsuario } from '../componente/principal/tipo.usuario';
import { UnidadBuscador } from '../componente/buscador/unidad.buscador';
import { UnidadFormulario } from '../componente/formulario/unidad.formulario';
import { Unidad } from '../componente/principal/unidad';
import { TipoDocumentoBuscador } from '../componente/buscador/tipo.documento.buscador';
import { TipoDocumentoFormulario } from '../componente/formulario/tipo.documento.formulario';
import { TipoDocumento } from '../componente/principal/tipo.documento';
import { SucursalBuscador } from '../componente/buscador/sucursal.buscador';
import { SucursalFormulario } from '../componente/formulario/sucursal.formulario';
import { Sucursal } from '../componente/principal/sucursal';
import { TipoMotivoBuscador } from '../componente/buscador/tipo.motivo.buscador';
import { TipoMotivoFormulario } from '../componente/formulario/tipo.motivo.formulario';
import { TipoMotivo } from '../componente/principal/tipo.motivo';
import { MarcaBuscador } from '../componente/buscador/marca.buscador';
import { MarcaFormulario } from '../componente/formulario/marca.formulario';
import { Marca } from '../componente/principal/marca';
import { CategoriaBuscador } from '../componente/buscador/categoria.buscador';
import { CategoriaFormulario } from '../componente/formulario/categoria.formulario';
import { Categoria } from '../componente/principal/categoria';
import { UsuarioBuscador } from '../componente/buscador/usuario.buscador';
import { UsuarioFormulario } from '../componente/formulario/usuario.formulario';
import { Usuario } from '../componente/principal/usuario';
import { PersonaBuscador } from '../componente/buscador/persona.buscador';
import { PersonaFormulario } from '../componente/formulario/persona.formulario';
import { Persona } from '../componente/principal/persona';
import { TrabajadorBuscador } from '../componente/buscador/trabajador.buscador';
import { TrabajadorFormulario } from '../componente/formulario/trabajador.formulario';
import { Trabajador } from '../componente/principal/trabajador';
import { ClienteBuscador } from '../componente/buscador/cliente.buscador';
import { ClienteFormulario } from '../componente/formulario/cliente.formulario';
import { Cliente } from '../componente/principal/cliente';
import { ProductoBuscador } from '../componente/buscador/producto.buscador';
import { ProductoFormulario } from '../componente/formulario/producto.formulario';
import { Producto } from '../componente/principal/producto';
import { ProveedorBuscador } from '../componente/buscador/proveedor.buscador';
import { ProveedorFormulario } from '../componente/formulario/proveedor.formulario';
import { Proveedor } from '../componente/principal/proveedor';
import { ProductoProveedorBuscador } from '../componente/buscador/producto.proveedor.buscador';
import { OrdenCompraBuscador } from '../componente/buscador/orden.compra.buscador';
import { OrdenCompra } from '../componente/principal/orden.compra';
import { ProductoRequerimientoTransferenciaBuscador } from '../componente/buscador/producto.requerimiento.transferencia.buscador';
import { RequerimientoTransferenciaBuscador } from '../componente/buscador/requerimiento.transferencia.buscador';
import { RequerimientoTransferencia } from '../componente/principal/requerimiento.transferencia';


import { Logeo } from '../componente/principal/logeo';
import { Inicio } from '../componente/principal/inicio';
import { Tabs } from '../componente/otro/tabs';
import { Tab } from '../componente/otro/tab';
import { Paginacion } from '../componente/otro/paginacion';
import { ConfirmDialog } from '../componente/otro/confirm.dialog';
import { MessageDialog } from '../componente/otro/message.dialog';
import { Permiso } from '../componente/principal/permiso';
import { PdfVisualizador } from '../componente/otro/visualizador.pdf';

//Modales
import { PdfVisualizadorModal } from '../componente/modal/visualizador.pdf.modal';
import { UnidadBuscadorModal } from '../componente/modal/unidad.buscador.modal';
import { TipoMotivoBuscadorModal } from '../componente/modal/tipo.motivo.buscador.modal';
import { TipoDocumentoBuscadorModal } from '../componente/modal/tipo.documento.buscador.modal';
import { PersonaBuscadorModal } from '../componente/modal/persona.buscador.modal';
import { SucursalBuscadorModal } from '../componente/modal/sucursal.buscador.modal';
import { TrabajadorBuscadorModal } from '../componente/modal/trabajador.buscador.modal';
import { TipoUsuarioBuscadorModal } from '../componente/modal/tipo.usuario.buscador.modal';
import { MarcaBuscadorModal } from '../componente/modal/marca.buscador.modal';
import { CategoriaBuscadorModal } from '../componente/modal/categoria.buscador.modal';
import { ClienteBuscadorModal } from '../componente/modal/cliente.buscador.modal';
import { ProveedorBuscadorModal } from '../componente/modal/proveedor.buscador.modal';
import { ProductoBuscadorModal } from '../componente/modal/producto.buscador.modal';
import { OrdenCompraBuscadorModal } from '../componente/modal/orden.compra.buscador.modal';
import { ProductoCantidadFormularioModal } from '../componente/modal/producto.cantidad.formulario.modal';
import { ProductoProveedorBuscadorModal } from '../componente/modal/producto.proveedor.buscador.modal';
import { ProductoRequerimientoTransferenciaBuscadorModal } from '../componente/modal/producto.requerimiento.transferencia.buscador.modal';
import { RequerimientoTransferenciaBuscadorModal } from '../componente/modal/requerimiento.transferencia.buscador.modal';
import { ProductoCantidadBuscadorModal } from '../componente/modal/producto.cantidad.buscador.modal';

//Servicios
import { ApiServicio } from '../servicio/api.servicio';
import { UtilServicio } from '../servicio/util.servicio';


//Directivas
import { DynamicTab } from '../directiva/dynamic.tab';
import { PasswordModule } from 'primeng/components/password/password';
import { Mayuscula } from '../directiva/mayuscula';
import { Entero } from '../directiva/entero';
import { Decimal } from '../directiva/decimal';

@NgModule({
  imports: [HttpModule, Route, BrowserModule, MenubarModule, ButtonModule, InputTextModule,
    DataTableModule, SharedModule, CalendarModule, FormsModule,
    FieldsetModule, DialogModule, CheckboxModule, TabViewModule, DropdownModule, PanelModule,
    ReactiveFormsModule, BrowserAnimationsModule,
    SplitButtonModule, PanelMenuModule, ToolbarModule, ConfirmDialogModule, OverlayPanelModule,
    DataListModule, PasswordModule, TreeModule],
  declarations: [
    App, PdfVisualizador, Mayuscula, Entero, Decimal, Menu, PdfVisualizadorModal, MessageDialog, ConfirmDialog,
    TipoUsuarioBuscador, TipoUsuarioFormulario, TipoUsuario, Logeo, Inicio, Tabs, Tab, Paginacion, DynamicTab,
    UnidadBuscador, UnidadFormulario, Unidad, TipoDocumentoBuscador, TipoDocumentoFormulario, TipoDocumento,
    SucursalBuscador, SucursalFormulario, Sucursal, TipoMotivoBuscador, TipoMotivoFormulario, TipoMotivo,
    MarcaBuscador, MarcaFormulario, Marca, CategoriaBuscador,
    CategoriaFormulario, Categoria, UsuarioBuscador, UsuarioFormulario, Usuario,
    UnidadBuscadorModal, CategoriaBuscadorModal, MarcaBuscadorModal, TipoMotivoBuscadorModal,
    TipoDocumentoBuscadorModal, TipoUsuarioBuscadorModal, PersonaBuscadorModal, SucursalBuscadorModal,
    TrabajadorBuscadorModal, Permiso, PersonaBuscador, Persona, PersonaFormulario, Trabajador,
    TrabajadorFormulario, TrabajadorBuscador, Cliente, ClienteFormulario, ClienteBuscador, Producto,
    ProductoBuscador, ProductoFormulario, Proveedor, ProveedorFormulario, ProveedorBuscador,
    ClienteBuscadorModal, ProveedorBuscadorModal, ProductoBuscadorModal, ProveedorBuscadorModal,
    ProductoBuscadorModal, OrdenCompraBuscadorModal, ProductoCantidadFormularioModal, ProductoProveedorBuscadorModal,
    ProductoRequerimientoTransferenciaBuscadorModal, RequerimientoTransferenciaBuscadorModal, 
    ProductoCantidadBuscadorModal, ProductoProveedorBuscador, OrdenCompraBuscador, OrdenCompra,
    ProductoRequerimientoTransferenciaBuscador, RequerimientoTransferenciaBuscador, RequerimientoTransferencia],
  bootstrap: [App],
  entryComponents: [Tab],
  providers: [ApiServicio, UtilServicio, ConfirmationService, ConfirmDialog, MessageDialog]
})
export class AppModulo { }
