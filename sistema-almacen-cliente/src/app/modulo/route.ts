import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Logeo } from '../componente/principal/logeo';
import { Inicio } from '../componente/principal/inicio';

const appRoutes: Routes = [
{  path: '',   redirectTo: '/logeo', pathMatch: 'full' },
  {
    path: 'logeo',
    component: Logeo
  },
   {
    path: 'inicio',
    component: Inicio
   }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: true, 
        useHash: true
      }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [

  ]
})
export class Route { }