import { Injectable, ElementRef } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { RespuestaServicio } from '../util/respuesta.servicio';
import { SelectItem } from 'primeng/primeng';
import { Router } from '@angular/router';
import { UsuarioEntidad } from '../entidad/usuario';
import { BusquedaPaginada } from '../util/busqueda.paginada';

import * as Decimal from 'decimal.js';

@Injectable()
export class UtilServicio {

    MONTH_NAMES: any = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    DAY_NAMES: any = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');


    nuevo(botones) {
        botones.nuevo = true;
        botones.guardar = false;
        botones.modificar = true;
        botones.eliminar = true;
        botones.cancelar = false;
    }

    cancelar(botones) {
        botones.nuevo = false;
        botones.guardar = true;
        botones.modificar = true;
        botones.eliminar = true;
        botones.cancelar = true;
    }

    modificar(botones) {
        botones.nuevo = true;
        botones.guardar = false;
        botones.modificar = true;
        botones.eliminar = true;
        botones.cancelar = false;
    }

    seleccionar(botones) {
        botones.nuevo = true;
        botones.guardar = true;
        botones.modificar = false;
        botones.eliminar = false;
        botones.cancelar = false;
    }

    setFocus(inputFocus: ElementRef) {
        setTimeout(() => {
            inputFocus.nativeElement.focus();
        }, 0);
    }

    selectInput(inputFocus: ElementRef) {
        setTimeout(() => {
            const inputElem = <HTMLInputElement>inputFocus.nativeElement;
            inputElem.select();
        }, 0);
    }

    getDateProperties() {
        return {
            firstDayOfWeek: 0,
            dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
            dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: 'Hoy',
            clear: 'Limpiar'
        };
    }

    formatBusquedaPaginadaParaEnviar(busquedaPaginada: BusquedaPaginada<any>, paginaActual: number): BusquedaPaginada<any> {
        return {
            paginaActual: paginaActual,
            buscar: busquedaPaginada.buscar,
            cantidadPorPagina: busquedaPaginada.cantidadPorPagina
        }
    }

    esNullOUndefined(valor: any): boolean {
        return typeof valor === "undefined" || valor == null;
    }

    esNullOUndefinedOVacio(valor: any): boolean {
        return this.esNullOUndefined(valor) || valor == "";
    }

    esNumero(strNumber: any): boolean {
        if (strNumber == null) return false;
        if (strNumber == undefined) return false;
        if (typeof strNumber === "number" && !isNaN(strNumber)) return true;
        if (strNumber == "") return false;
        if (strNumber === "") return false;
        let psInt, psFloat;
        psInt = parseInt(strNumber);
        psFloat = parseFloat(strNumber);
        return !isNaN(strNumber) && !isNaN(psFloat);
    }

    haSeleccionadoEntidad(entidad: any): boolean {
        if (this.esNullOUndefined(entidad)) {
            return false;
        }
        if (this.esNullOUndefinedOVacio(entidad.id)) {
            return false;
        }
        if (!this.esNumero(entidad.id)) {
            return false;
        }
        return true;
    }

    esArrayVacio(array): boolean {
        return this.esNullOUndefined(array) || array.length === 0;
    }

    dataDeServerEsCorrecta(data: RespuestaServicio): boolean {
        return !this.esNullOUndefined(data) && !this.esNullOUndefinedOVacio(data.estado) && data.estado === "exito";
    }

    esCadenaLongitudValida(cadena: string, longitud: number, esObligatorio: boolean = true) {
        if (esObligatorio) {
            return !this.esNullOUndefinedOVacio(cadena) && cadena.length == longitud;
        } else if (this.esNullOUndefinedOVacio(cadena)) {
            return true;
        } else {
            return cadena.length == longitud;
        }
    }

    redondear(numero: decimal.Decimal, nDecimales: number) {
        let multiplo = Decimal.pow(10, nDecimales);
        return multiplo.times(numero).round().div(multiplo);
    }

    showLoader() {
        document.getElementById("loader").style.display = "block";
    }

    hideLoader() {
        document.getElementById("loader").style.display = "none";
    }

    obtenerPropiedadDeEntidad(entidad: any, propiedad: string) {
        let subPropiedades, resultado;
        resultado = entidad;
        if (!this.esNullOUndefinedOVacio(propiedad)) {
            subPropiedades = propiedad.split('.');
            for (let i = 0; i < subPropiedades.length; i++) {
                let obj = subPropiedades[i];
                resultado = resultado[obj];
            }
        }
        return resultado;
    }

    meterResultadoConsultaEnListaItems(resultado: RespuestaServicio, listaItems: SelectItem[], propiedadMostrar: string = "nombre", propiedadClave: string = undefined) {
        resultado.data && resultado.data.forEach(element => {
            let etiqueta = this.obtenerPropiedadDeEntidad(element, propiedadMostrar);
            let clave = this.obtenerPropiedadDeEntidad(element, propiedadClave);
            listaItems.push({ label: etiqueta, value: clave });
        });
    }

    buscarEnArregloPorPropiedad(xbuscar, arreglo, propiedad: string = '', incluyeEliminados: boolean = false, indiceExcluido: number = -1): number {
        let element, cpm;
        if (!this.esArrayVacio(arreglo)) {
            for (let i = 0; i < arreglo.length; i++) {
                element = arreglo[i];
                if (incluyeEliminados || (!incluyeEliminados && !element.eliminar)) {
                    cpm = this.obtenerPropiedadDeEntidad(element, propiedad);
                    if ((cpm == xbuscar) && (indiceExcluido != i)) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    valorDefaultFiltroDropDown(array: any[], filtrosBusqueda: any, propiedadFiltro: string) {
        if (!this.esArrayVacio(array)) {
            return this.asignarValorPropiedad(filtrosBusqueda, propiedadFiltro, array[0].value);
        }
    }

    asignarValorPropiedad(entidad: any, propiedad: string, valor: any) {
        let subPropiedades, resultado, i;
        resultado = entidad;
        if (!this.esNullOUndefinedOVacio(propiedad)) {
            subPropiedades = propiedad.split('.');
            for (i = 0; i < subPropiedades.length - 1; i++) {
                let obj = subPropiedades[i];
                resultado = resultado[obj];
            }
            resultado[subPropiedades[i]] = valor;
        } else if (propiedad === "") {
            resultado = valor;
        }
        return resultado;
    }

    esRucValido(ruc) {
        let patt = /^(10|20|15|17)[0-9]{9}$/;
        if (ruc && !patt.test(ruc)) {
            return false;
        }
        return this.validarFormatoRucSunat(ruc);
    }
    trim(cadena) {
        let cadena2 = "";
        let len = cadena.length;
        for (let i = 0; i <= len; i++)
            if (cadena.charAt(i) != " ") { cadena2 += cadena.charAt(i); }
        return cadena2;
    }
    validarFormatoRucSunat(valor) {
        valor = this.trim(valor)
        if (this.esNumero(valor)) {
            if (valor.length == 8) {
                let suma = 0
                for (let i = 0; i < valor.length - 1; i++) {
                    let digito = valor.charAt(i) - 0;
                    if (i == 0) suma += (digito * 2)
                    else suma += (digito * (valor.length - i))
                }
                let resto = suma % 11;
                if (resto == 1) resto = 11;
                if (resto + (valor.charAt(valor.length - 1) - 0) == 11) {
                    return true
                }
            } else if (valor.length == 11) {
                let suma = 0
                let x = 6
                for (let i = 0; i < valor.length - 1; i++) {
                    if (i == 4) x = 8
                    let digito = valor.charAt(i) - 0;
                    x--;
                    if (i == 0) suma += (digito * x)
                    else suma += (digito * x)
                }
                let resto = suma % 11;
                resto = 11 - resto

                if (resto >= 10) resto = resto - 10;
                if (resto == valor.charAt(valor.length - 1) - 0) {
                    return true
                }
            }
        }
        return false
    }

    obtenerUsuarioLogeado(): any {
        let user = localStorage.getItem('usuario');
        return (<any>Object).assign(new UsuarioEntidad(), JSON.parse(user));
    }

    iniciarSesion(router: Router, result: RespuestaServicio) {
        localStorage.setItem('usuario', JSON.stringify(result.data));
        router.navigateByUrl('inicio');
    }

    cerrarSesion(router: Router) {
        localStorage.removeItem('usuario');
        router.navigateByUrl('logeo');
    }
    eliminarUsuarioSession() {
        localStorage.removeItem('usuario');
    }

    /* FECHAS */
    getDateFromFormat(val, format) {
        val = val + "";
        format = format + "";
        let i_val = 0;
        let i_format = 0;
        let c = "";
        let token = "";
        let token2 = "";
        let x, y;
        let now: any = new Date();
        let year: any = now.getFullYear();
        let month: any = now.getMonth() + 1;
        let date: any = 1;
        let hh: any = now.getHours();
        let mm: any = now.getMinutes();
        let ss: any = now.getSeconds();
        let ampm = "";

        while (i_format < format.length) {
            // Get next token from format string
            c = format.charAt(i_format);
            token = "";
            while ((format.charAt(i_format) == c) && (i_format < format.length)) {
                token += format.charAt(i_format++);
            }
            // Extract contents of value based on format token
            if (token == "yyyy" || token == "yy" || token == "y") {
                if (token == "yyyy") {
                    x = 4;
                    y = 4;
                }
                if (token == "yy") {
                    x = 2;
                    y = 2;
                }
                if (token == "y") {
                    x = 2;
                    y = 4;
                }
                year = this._getInt(val, i_val, x, y);
                if (year == null) {
                    return 0;
                }
                i_val += year.length;
                if (year.length == 2) {
                    if (year > 70) {
                        year = 1900 + (year - 0);
                    }
                    else {
                        year = 2000 + (year - 0);
                    }
                }
            }
            else if (token == "MMM" || token == "NNN") {
                month = 0;
                for (let i = 0; i < this.MONTH_NAMES.length; i++) {
                    let month_name = this.MONTH_NAMES[i];
                    if (val.substring(i_val, i_val + month_name.length).toLowerCase() == month_name.toLowerCase()) {
                        if (token == "MMM" || (token == "NNN" && i > 11)) {
                            month = i + 1;
                            if (month > 12) {
                                month -= 12;
                            }
                            i_val += month_name.length;
                            break;
                        }
                    }
                }
                if ((month < 1) || (month > 12)) {
                    return 0;
                }
            }
            else if (token == "EE" || token == "E") {
                for (let i = 0; i < this.DAY_NAMES.length; i++) {
                    let day_name = this.DAY_NAMES[i];
                    if (val.substring(i_val, i_val + day_name.length).toLowerCase() == day_name.toLowerCase()) {
                        i_val += day_name.length;
                        break;
                    }
                }
            }
            else if (token == "MM" || token == "M") {
                month = this._getInt(val, i_val, token.length, 2);
                if (month == null || (month < 1) || (month > 12)) {
                    return 0;
                }
                i_val += month.length;
            }
            else if (token == "dd" || token == "d") {
                date = this._getInt(val, i_val, token.length, 2);
                if (date == null || (date < 1) || (date > 31)) {
                    return 0;
                }
                i_val += date.length;
            }
            else if (token == "hh" || token == "h") {
                hh = this._getInt(val, i_val, token.length, 2);
                if (hh == null || (hh < 1) || (hh > 12)) {
                    return 0;
                }
                i_val += hh.length;
            }
            else if (token == "HH" || token == "H") {
                hh = this._getInt(val, i_val, token.length, 2);
                if (hh == null || (hh < 0) || (hh > 23)) {
                    return 0;
                }
                i_val += hh.length;
            }
            else if (token == "KK" || token == "K") {
                hh = this._getInt(val, i_val, token.length, 2);
                if (hh == null || (hh < 0) || (hh > 11)) {
                    return 0;
                }
                i_val += hh.length;
            }
            else if (token == "kk" || token == "k") {
                hh = this._getInt(val, i_val, token.length, 2);
                if (hh == null || (hh < 1) || (hh > 24)) {
                    return 0;
                }
                i_val += hh.length;
                hh--;
            }
            else if (token == "mm" || token == "m") {
                mm = this._getInt(val, i_val, token.length, 2);
                if (mm == null || (mm < 0) || (mm > 59)) {
                    return 0;
                }
                i_val += mm.length;
            }
            else if (token == "ss" || token == "s") {
                ss = this._getInt(val, i_val, token.length, 2);
                if (ss == null || (ss < 0) || (ss > 59)) {
                    return 0;
                }
                i_val += ss.length;
            }
            else if (token == "a") {
                if (val.substring(i_val, i_val + 2).toLowerCase() == "am") {
                    ampm = "AM";
                }
                else if (val.substring(i_val, i_val + 2).toLowerCase() == "pm") {
                    ampm = "PM";
                }
                else {
                    return 0;
                }
                i_val += 2;
            }
            else {
                if (val.substring(i_val, i_val + token.length) != token) {
                    return 0;
                }
                else {
                    i_val += token.length;
                }
            }
        }
        // If there are any trailing characters left in the value, it doesn't match
        if (i_val != val.length) {
            return 0;
        }
        // Is date valid for month?
        if (month == 2) {
            // Check for leap year
            if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) { // leap year
                if (date > 29) {
                    return 0;
                }
            }
            else {
                if (date > 28) {
                    return 0;
                }
            }
        }
        if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
            if (date > 30) {
                return 0;
            }
        }
        // Correct hours value
        if (hh < 12 && ampm == "PM") {
            hh = hh - 0 + 12;
        }
        else if (hh > 11 && ampm == "AM") {
            hh -= 12;
        }
        let newdate = new Date(year, month - 1, date, hh, mm, ss);
        return newdate.getTime();
    }

    _isInteger(val) {
        let digits = "1234567890";
        for (let i = 0; i < val.length; i++) {
            if (digits.indexOf(val.charAt(i)) == -1) {
                return false;
            }
        }
        return true;
    }
    _getInt(str, i, minlength, maxlength) {
        for (let x = maxlength; x >= minlength; x--) {
            let token = str.substring(i, i + x);
            if (token.length < minlength) {
                return null;
            }
            if (this._isInteger(token)) {
                return token;
            }
        }
        return null;
    }

    parseDate(val) {
        let preferEuro = (arguments.length == 2) ? arguments[1] : false;
        let generalFormats = new Array('dd-MM-yyyy', 'y-M-d', 'MMM d, y', 'MMM d,y', 'y-MMM-d', 'd-MMM-y', 'MMM d');
        let monthFirst = new Array('M/d/y', 'M-d-y', 'M.d.y', 'MMM-d', 'M/d', 'M-d');
        let dateFirst = new Array('d/M/y', 'd-M-y', 'd.M.y', 'd-MMM', 'd/M', 'd-M');
        let checkList = new Array('generalFormats', preferEuro ? 'dateFirst' : 'monthFirst', preferEuro ? 'monthFirst' : 'dateFirst');
        let list = { generalFormats: generalFormats, monthFirst: monthFirst, dateFirst: dateFirst };
        let d = null;
        for (let i = 0; i < checkList.length; i++) {
            let l = list[checkList[i]];
            for (let j = 0; j < l.length; j++) {
                d = this.getDateFromFormat(val, l[j]);
                if (d != 0) {
                    return new Date(d);
                }
            }
        }
        return null;
    }

    agregarDias(date: Date, days: number) {
        return new Date(date.getTime() + days * 24 * 60 * 60 * 1000);
    }

    LZ(x) {
        return (x < 0 || x > 9 ? "" : "0") + x
    }

    esFechaValida(fecha) {
        if (fecha && this.esNumero(fecha.getTime())) {
            return true;
        }
        return false;
    }

    formatDate(date, format) {
        var result = "";
        if (this.esFechaValida(date) && date != undefined && format != undefined) {
            format = format + "";
            var i_format = 0;
            var c = "";
            var token = "";

            var M = date.getMonth() + 1;
            var y: any = date.getFullYear() + "";
            var d = date.getDate();
            var E = date.getDay();
            var H = date.getHours();
            var m = date.getMinutes();
            var s = date.getSeconds();
            var yyyy, yy, MMM, MM, dd, hh, h, mm, ss, ampm, HH, H, KK, K, kk, k;
            // Convert real date parts into formatted versions
            var value = new Object();
            if (y.length < 4) {
                y = "" + (y - 0 + 1900);
            }
            value["y"] = "" + y;
            value["yyyy"] = y;
            value["yy"] = y.substring(2, 4);
            value["M"] = M;
            value["MM"] = this.LZ(M);
            value["MMM"] = this.MONTH_NAMES[M - 1];
            value["NNN"] = this.MONTH_NAMES[M + 11];
            value["d"] = d;
            value["dd"] = this.LZ(d);
            value["E"] = this.DAY_NAMES[E + 7];
            value["EE"] = this.DAY_NAMES[E];
            value["H"] = H;
            value["HH"] = this.LZ(H);
            if (H == 0) {
                value["h"] = 12;
            }
            else if (H > 12) {
                value["h"] = H - 12;
            }
            else {
                value["h"] = H;
            }
            value["hh"] = this.LZ(value["h"]);
            if (H > 11) {
                value["K"] = H - 12;
            } else {
                value["K"] = H;
            }
            value["k"] = H + 1;
            value["KK"] = this.LZ(value["K"]);
            value["kk"] = this.LZ(value["k"]);
            if (H > 11) {
                value["a"] = "PM";
            }
            else {
                value["a"] = "AM";
            }
            value["m"] = m;
            value["mm"] = this.LZ(m);
            value["s"] = s;
            value["ss"] = this.LZ(s);
            while (i_format < format.length) {
                c = format.charAt(i_format);
                token = "";
                while ((format.charAt(i_format) == c) && (i_format < format.length)) {
                    token += format.charAt(i_format++);
                }
                if (value[token] != null) {
                    result = result + value[token];
                }
                else {
                    result = result + token;
                }
            }
        }
        return result;
    }

}