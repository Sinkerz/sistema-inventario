import { Directive, ViewContainerRef,Inject } from '@angular/core';

@Directive({
  selector: '[dynamic-tab]'
})
export class DynamicTab {
  constructor(@Inject(ViewContainerRef) public viewContainer: ViewContainerRef){}
}