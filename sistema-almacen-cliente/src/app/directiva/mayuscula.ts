import { Output, Input, Directive, EventEmitter } from "@angular/core";

@Directive({
    selector: '[ngModel][mayuscula]',
    host: {
        "(input)": 'onInputChange($event)'
    }
})
export class Mayuscula {

    @Output() ngModelChange: EventEmitter<any> = new EventEmitter()
    value: any

    onInputChange($event) {
        this.value = $event.target.value.toUpperCase();
        this.ngModelChange.emit(this.value);
    }

}