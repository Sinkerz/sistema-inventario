import { Component, EventEmitter, Output, Inject } from '@angular/core';
import { PersonaEntidad } from '../../entidad/persona';
import { BusquedaPaginada } from '../../util/busqueda.paginada';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'persona-buscador',
    moduleId: module.id,
    templateUrl: '../../plantilla/buscador/persona.buscador.html'
})
export class PersonaBuscador {

    private busquedaPaginada: BusquedaPaginada<PersonaEntidad>;

    @Output() onSelect = new EventEmitter<PersonaEntidad>();
    @Output() respuestaBuscador = new EventEmitter<string>();

    constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.busquedaPaginada = new BusquedaPaginada<PersonaEntidad>();
    }

    consultar(pagina: number = 1) {
        this.utilServicio.showLoader();
        this.apiServicio.paginacion('persona', this.utilServicio.formatBusquedaPaginadaParaEnviar(this.busquedaPaginada, pagina))
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.busquedaPaginada = result.data;
                } else {
                    this.respuestaBuscador.emit(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaBuscador.emit('Error al conectar con el servidor');
            });
    }

    onRowSelect(event) {
        this.onSelect.emit(event.data);
    }

    getParametrosBusq() {
        return this.busquedaPaginada.buscar;
    }

    ngOnInit() {
        this.consultar();
    }
}