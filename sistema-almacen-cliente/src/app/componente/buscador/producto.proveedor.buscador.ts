import { Component, EventEmitter, Output, Inject } from '@angular/core';
import { ProductoDTOEntidad } from '../../entidad/dto/producto.dto';
import { DocumentoDTOEntidad } from '../../entidad/dto/documento.dto';
import { BusquedaPaginada } from '../../util/busqueda.paginada';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'producto-proveedor-buscador',
    moduleId: module.id,
    templateUrl: '../../plantilla/buscador/producto.proveedor.buscador.html'
})
export class ProductoProveedorBuscador {

    private busquedaPaginada: BusquedaPaginada<ProductoDTOEntidad>;

    @Output() onSelect = new EventEmitter<ProductoDTOEntidad>();
    @Output() openModal = new EventEmitter<any>();
    @Output() respuestaBuscador = new EventEmitter<string>();
    productoSeleccionado: ProductoDTOEntidad;
    proveedor: number;
    proveedoresTodos: boolean;
    compras: DocumentoDTOEntidad[];
    constructor( @Inject(ApiServicio) private apiServicio: ApiServicio, @Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.busquedaPaginada = new BusquedaPaginada<ProductoDTOEntidad>();
        this.busquedaPaginada.cantidadPorPagina = 5;
    }

    consultar(pagina: number = 1) {
        this.utilServicio.showLoader();
        this.apiServicio.paginacion('producto', this.utilServicio.formatBusquedaPaginadaParaEnviar(this.busquedaPaginada, pagina))
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.busquedaPaginada = result.data;
                } else {
                    this.respuestaBuscador.emit(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaBuscador.emit('Error al conectar con el servidor');
            });
    }

    onRowSelectProducto(event) {
        this.productoSeleccionado = event.data;
        this.obtenerCompras();
    }

    onRowSelect(event) {
        this.onSelect.emit({
            codigo: this.productoSeleccionado.codigo,
            id: this.productoSeleccionado.id,
            precioNormal: event.data.precio,
            marca: this.productoSeleccionado.marca,
            nombre: this.productoSeleccionado.nombre
        });
    }

    setProveedor(proveedor) {
        this.proveedor = proveedor;
        if (!this.utilServicio.esNumero(proveedor)) {
            this.proveedoresTodos = true;
        }
    }

    validarComprasObtenidas() {
        if (this.compras.length == 0 && this.utilServicio.haSeleccionadoEntidad(this.productoSeleccionado)) {
            this.onSelect.emit(this.productoSeleccionado);
        }
    }

    obtenerCompras() {
        if (this.utilServicio.haSeleccionadoEntidad(this.productoSeleccionado)) {
            this.utilServicio.showLoader();
            this.apiServicio.obtenerTodos(this.obtenerUrl())
                .then((result) => {
                    this.utilServicio.hideLoader();
                    if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                        this.compras = result.data;
                        this.validarComprasObtenidas();
                    } else {
                        this.respuestaBuscador.emit(result.mensaje);
                    }
                    this.utilServicio.hideLoader();
                }).catch((error) => {
                    this.utilServicio.hideLoader();
                    this.respuestaBuscador.emit('Error al conectar con el servidor');
                });
        }
    }

    obtenerUrl(): string {
        return "documentoCabecera/compra/producto/" + this.productoSeleccionado.id + (this.proveedoresTodos ? "" : ("/proveedor/" + this.proveedor));
    }

    ngOnInit() {

    }
} 