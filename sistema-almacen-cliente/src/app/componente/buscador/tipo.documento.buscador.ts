import { Component, EventEmitter, Output, Inject } from '@angular/core';
import { TipoDocumentoEntidad } from '../../entidad/tipo.documento';
import { BusquedaPaginada } from '../../util/busqueda.paginada';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'tipo-documento-buscador',
    moduleId: module.id,
    templateUrl: '../../plantilla/buscador/tipo.documento.buscador.html'
})
export class TipoDocumentoBuscador {

    private busquedaPaginada: BusquedaPaginada<TipoDocumentoEntidad>;

    @Output() onSelect = new EventEmitter<TipoDocumentoEntidad>();
    @Output() respuestaBuscador = new EventEmitter<string>();

constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.busquedaPaginada = new BusquedaPaginada<TipoDocumentoEntidad>();
    }

    consultar(pagina: number = 1) {
        this.utilServicio.showLoader();
        this.apiServicio.paginacion('tipoDocumento', this.utilServicio.formatBusquedaPaginadaParaEnviar(this.busquedaPaginada, pagina))
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.busquedaPaginada = result.data;
                } else {
                    this.respuestaBuscador.emit(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaBuscador.emit('Error al conectar con el servidor');
            });
    }

    onRowSelect(event) {
        this.onSelect.emit(event.data);
    }

    getParametrosBusq() {
        return this.busquedaPaginada.buscar;
    }

    ngOnInit() {
        this.consultar();
    }

}