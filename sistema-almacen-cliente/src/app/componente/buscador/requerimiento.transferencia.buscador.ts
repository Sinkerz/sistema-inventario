import { Component, EventEmitter, Output, Inject, Input } from '@angular/core';
import { DocumentoDTOEntidad } from '../../entidad/dto/documento.dto';
import { BusquedaPaginada } from '../../util/busqueda.paginada';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'requerimiento-transferencia-buscador',
    moduleId: module.id,
    templateUrl: '../../plantilla/buscador/requerimiento.transferencia.buscador.html'
})
export class RequerimientoTransferenciaBuscador {

    private busquedaPaginada: BusquedaPaginada<DocumentoDTOEntidad>;

    @Output() onSelect = new EventEmitter<DocumentoDTOEntidad>();
    @Output() respuestaBuscador = new EventEmitter<string>();
    sucursalDestinoId: number;
    es: any;

    constructor( @Inject(ApiServicio) private apiServicio: ApiServicio, @Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.busquedaPaginada = new BusquedaPaginada<DocumentoDTOEntidad>();
        this.es = this.utilServicio.getDateProperties();
    }

    consultar(pagina: number = 1) {
        this.utilServicio.showLoader();
        this.busquedaPaginada.buscar.sucursalDestinoId = this.sucursalDestinoId;
        this.apiServicio.paginacion('documentoCabecera/requerimientoTransferencia', this.utilServicio.formatBusquedaPaginadaParaEnviar(this.busquedaPaginada, pagina))
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.busquedaPaginada = result.data;
                } else {
                    this.respuestaBuscador.emit(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaBuscador.emit('Error al conectar con el servidor');
            });
    }

    setSucursalDestinoId(sucursalDestinoId: number) {
        this.sucursalDestinoId = sucursalDestinoId;
    }
 
    onRowSelect(event) {
        this.onSelect.emit(event.data);
    }

    ngOnInit() {
        this.consultar();
    }
}