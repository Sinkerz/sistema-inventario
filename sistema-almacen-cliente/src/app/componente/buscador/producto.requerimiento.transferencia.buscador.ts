import { Component, EventEmitter, Output, Inject } from '@angular/core';
import { ProductoDTOEntidad } from '../../entidad/dto/producto.dto';
import { ProductoSucursalEntidad } from '../../entidad/producto.sucursal';
import { BusquedaPaginada } from '../../util/busqueda.paginada';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'producto-requerimiento-transferencia-buscador',
    moduleId: module.id,
    templateUrl: '../../plantilla/buscador/producto.requerimiento.transferencia.buscador.html'
})
export class ProductoRequerimientoTransferenciaBuscador {

    private busquedaPaginada: BusquedaPaginada<ProductoDTOEntidad>;

    @Output() onSelect = new EventEmitter<ProductoDTOEntidad>();
    @Output() openModal = new EventEmitter<any>();
    @Output() respuestaBuscador = new EventEmitter<string>();

    productoSucursal: ProductoSucursalEntidad;

    sucursalOrigen: number;
    sucursalDestino: number;
    productosTodos: boolean;
    constructor( @Inject(ApiServicio) private apiServicio: ApiServicio, @Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.busquedaPaginada = new BusquedaPaginada<ProductoDTOEntidad>();
    }

    consultar(pagina: number = 1){
        if (this.productosTodos){
            this.mostrarTodos(pagina);
        } else {
            this.mostrarConsulta(pagina);
        }
    }

    mostrarConsulta(pagina: number = 1) {
        this.utilServicio.showLoader();
        this.busquedaPaginada.buscar.sucursalOrigenId = this.sucursalOrigen;
        this.busquedaPaginada.buscar.sucursalDestinoId = this.sucursalDestino;
        if (this.busquedaPaginada.buscar.marca == null){
            this.busquedaPaginada.buscar.marca = "";
        }
        this.apiServicio.paginacion('productoSucursal/requerimientoTransferencia', this.utilServicio.formatBusquedaPaginadaParaEnviar(this.busquedaPaginada, pagina))
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.busquedaPaginada = result.data;
                } else {
                    this.respuestaBuscador.emit(result.mensaje);
                }
                this.openModal.emit();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaBuscador.emit('Error al conectar con el servidor');
            });

    }

    mostrarTodos(pagina: number = 1) {
        this.utilServicio.showLoader();
        this.busquedaPaginada.buscar.sucursal = this.sucursalOrigen;
        this.apiServicio.paginacion('producto/venta', this.utilServicio.formatBusquedaPaginadaParaEnviar(this.busquedaPaginada, pagina))
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.busquedaPaginada = result.data;
                } else {
                    this.respuestaBuscador.emit(result.mensaje);
                }
                this.openModal.emit();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaBuscador.emit('Error al conectar con el servidor');
            });
    }

    onRowSelect(event) {
        this.onSelect.emit(event.data);
    }

    consultarConSucursal(sucursalOrig: number, sucursalDest: number) {
        this.sucursalOrigen = sucursalOrig;
        this.sucursalDestino = sucursalDest;
        this.consultar();
    }

    ngOnInit() {

    }
}