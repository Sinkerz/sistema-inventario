import { Component, EventEmitter, Output, Inject } from '@angular/core';
import { DocumentoDTOEntidad } from '../../entidad/dto/documento.dto';
import { BusquedaPaginada } from '../../util/busqueda.paginada';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'orden-compra-buscador',
    moduleId: module.id,
    templateUrl: '../../plantilla/buscador/orden.compra.buscador.html'
})
export class OrdenCompraBuscador {

    private busquedaPaginada: BusquedaPaginada<DocumentoDTOEntidad>;

    @Output() onSelect = new EventEmitter<DocumentoDTOEntidad>();
    @Output() respuestaBuscador = new EventEmitter<string>();

    constructor( @Inject(ApiServicio) private apiServicio: ApiServicio, @Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.busquedaPaginada = new BusquedaPaginada<DocumentoDTOEntidad>();
    }

    consultar(pagina: number = 1) {
        this.utilServicio.showLoader();
        this.apiServicio.paginacion('documentoCabecera/ordenCompra', this.utilServicio.formatBusquedaPaginadaParaEnviar(this.busquedaPaginada, pagina))
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.busquedaPaginada = result.data;
                } else {
                    this.respuestaBuscador.emit(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaBuscador.emit('Error al conectar con el servidor');
            });
    }

    onRowSelect(event) {
        this.onSelect.emit(event.data);
    }

    ngOnInit() {
        this.consultar();
    }
} 