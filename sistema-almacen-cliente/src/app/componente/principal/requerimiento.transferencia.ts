import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { MessageDialog } from '../otro/message.dialog';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { BusquedaPaginada } from '../../util/busqueda.paginada';
import { DocumentoCabeceraEntidad } from '../../entidad/documento.cabecera';
import { DocumentoDetalleEntidad } from '../../entidad/documento.detalle';
import { ProductoSucursalEntidad } from '../../entidad/producto.sucursal';
import { ProductoDTOEntidad } from '../../entidad/dto/producto.dto';
import { DocumentoDTOEntidad } from '../../entidad/dto/documento.dto';
import { RequerimientoDTOEntidad } from '../../entidad/dto/requerimiento.dto';
import { ProductoRequerimientoTransferenciaBuscadorModal } from '../modal/producto.requerimiento.transferencia.buscador.modal';
import { RequerimientoTransferenciaBuscadorModal } from '../modal/requerimiento.transferencia.buscador.modal';
import { PersonaEntidad } from '../../entidad/persona';
import { SucursalEntidad } from '../../entidad/sucursal';
import { TipoComprobanteEntidad } from '../../entidad/tipo.comprobante';


import * as Decimal from 'decimal.js';

@Component({
    selector: 'requerimiento-transferencia',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/requerimiento.transferencia.html'
})
export class RequerimientoTransferencia {

    botones;
    parametros: any;
    es: any;
    usuario: any;

    private busquedaPaginada: BusquedaPaginada<DocumentoCabeceraEntidad>;
    componentesInactivos: boolean;

    documentoCabecera: DocumentoCabeceraEntidad;
    productos: ProductoDTOEntidad[];
    producto: ProductoDTOEntidad;
    productoSucursal: ProductoSucursalEntidad;

    sucursalesOrigen: SelectItem[];
    sucursalesDestino: SelectItem[];

    @ViewChild(ProductoRequerimientoTransferenciaBuscadorModal) productoRequerimientoTransferenciaBuscadorModal;
    @ViewChild(RequerimientoTransferenciaBuscadorModal) requerimientoTransferenciaBuscadorModal;
    @ViewChild(MessageDialog) messageDialog;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;

    constructor( @Inject(ConfirmDialog) private confirmDialog: ConfirmDialog, @Inject(ApiServicio) private apiServicio: ApiServicio, @Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.botones = {};
        this.es = this.utilServicio.getDateProperties();
        this.parametros = { nombreReporte: "requerimiento_transferencia", formatoExportacion: "PDF" };
        this.busquedaPaginada = new BusquedaPaginada<DocumentoCabeceraEntidad>();
        this.usuario = this.utilServicio.obtenerUsuarioLogeado();
    }

    obtenerSucursales() {
        this.utilServicio.showLoader();
        this.apiServicio.obtenerTodos('sucursal')
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    if (result.data.length > 0) {
                        // Llenamos los combos
                        this.utilServicio.meterResultadoConsultaEnListaItems(result, this.sucursalesOrigen, 'nombre', 'id');
                        this.utilServicio.meterResultadoConsultaEnListaItems(result, this.sucursalesDestino, 'nombre', 'id');
                    }
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    abrirSelectorRequerimientoTransferencia() {
        this.requerimientoTransferenciaBuscadorModal.mostrarModal();
    }

    seleccionarRequerimientoTransferencia(requerimiento: DocumentoDTOEntidad) {
        this.utilServicio.showLoader();
        this.apiServicio.obtener('documentoCabecera/requerimientoTransferencia', requerimiento.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    if (this.utilServicio.haSeleccionadoEntidad(result.data)) {
                        this.cargarRequerimiento(result.data, requerimiento.documento);
                        this.documentoCabecera.id = requerimiento.id;
                        this.utilServicio.seleccionar(this.botones);
                    } else {
                        this.messageDialog.showDialog("No se encontro el requerimiento seleccionado.");
                    }
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    cargarRequerimiento(requerimiento: RequerimientoDTOEntidad, numero: string) {
        this.documentoCabecera.documentoRef = {
            id: requerimiento.id
        };
        this.productos = [];
        this.documentoCabecera.sucursal.id = requerimiento.sucursalOrigenId;
        this.documentoCabecera.sucursalDestino.id = requerimiento.sucursalDestinoId;
        requerimiento.productos.forEach(pro => {
            this.seleccionarProducto(pro);
        });
    }

    abrirSelectorProducto() {
        if (this.utilServicio.haSeleccionadoEntidad(this.documentoCabecera.sucursal)) {
            if (this.utilServicio.haSeleccionadoEntidad(this.documentoCabecera.sucursalDestino)) {
                if (this.documentoCabecera.sucursal.id != this.documentoCabecera.sucursalDestino.id) {
                    this.productoRequerimientoTransferenciaBuscadorModal.mostrarModal(this.documentoCabecera.sucursal.id, this.documentoCabecera.sucursalDestino.id);
                } else {
                    this.messageDialog.showDialog('El almacén de destino debe ser distinto al almacén de origen');
                }
            } else {
                this.messageDialog.showDialog("Debe seleccionar la sucursal de destino");
            }
        } else {
            this.messageDialog.showDialog("Debe seleccionar la sucursal de origen");
        }
    }

    seleccionarProducto(producto: ProductoDTOEntidad) {
        if (this.validarProducto(producto)) {
            this.productos.push({
                id: producto.id,
                codigo: producto.codigo,
                nombre: producto.nombre,
                marca: producto.marca,
                cantidad: producto.cantidad
            });
        }
    }

    obtenerProductoPorCodigo() {
        if (this.utilServicio.haSeleccionadoEntidad(this.documentoCabecera.sucursal)) {
            if (!this.utilServicio.esNullOUndefinedOVacio(this.producto.codigo)) {
                this.utilServicio.showLoader();
                this.apiServicio.obtener('producto/codigo', this.producto.codigo)
                    .then((result) => {
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            if (this.utilServicio.haSeleccionadoEntidad(result.data)) {
                                this.producto = result.data;
                            } else {
                                this.producto = new ProductoDTOEntidad();
                                this.messageDialog.showDialog("No se encontró producto con el código especificado.");
                            }
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                        this.utilServicio.hideLoader();
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        } else {
            this.messageDialog.showDialog("Primero debe seleccionar la sucursal de origen");
        }
    }

    validarProducto(producto: ProductoDTOEntidad) {
        return !(this.utilServicio.buscarEnArregloPorPropiedad(producto.id, this.productos, 'id') >= 0);
    }

    agregarProducto() {
        if (!this.utilServicio.haSeleccionadoEntidad(this.producto)) {
            this.messageDialog.showDialog('Debe seleccionar un producto');
            return;
        }
        if (!this.utilServicio.esNumero(this.producto.cantidad)) {
            this.messageDialog.showDialog('Debe ingresar una cantidad válida');
            this.producto.cantidad = null;
            return;
        }
        if (new Decimal(this.producto.cantidad).comparedTo(new Decimal(0)) != 1) {
            this.messageDialog.showDialog('Debe ingresar una cantidad mayor a cero');
            this.producto.cantidad = null;
            return;
        }
        if (!this.validarProducto(this.producto)) {
            this.messageDialog.showDialog('El producto ya fue agregado');
            this.producto = new ProductoDTOEntidad();
            return;
        }
        this.apiServicio.obtener('producto/stock/' + this.producto.id + '/sucursal', this.documentoCabecera.sucursal.id)
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    if (result.data >= this.producto.cantidad) {
                        this.seleccionarProducto(this.producto);
                        this.producto = new ProductoDTOEntidad();
                    } else {
                        this.messageDialog.showDialog("El producto no tiene stock suficiente. Stock: " + result.data);
                    }
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    eliminarProducto(index) {
        this.productos.splice(index, 1);
    }

    calcularTotales(pTotal: string, pMult: string = undefined): decimal.Decimal {
        let total = new Decimal(0);
        this.productos.forEach(pro => {
            if (this.utilServicio.esNumero(pro[pTotal])) {
                if (!pMult) {
                    total = total.plus(pro[pTotal]);
                } else if (this.utilServicio.esNumero(pro[pMult])) {
                    total = total.plus(this.utilServicio.redondear(new Decimal(pro[pTotal]).times(pro[pMult]), 2));
                }
            }
        });
        return this.utilServicio.redondear(total, 2);
    }

    obtenerTransferenciaParaGuardar(): DocumentoCabeceraEntidad {
        let documentoCabecera: DocumentoCabeceraEntidad = (<any>Object).assign({}, this.documentoCabecera);
        let documentoDetalle: DocumentoDetalleEntidad;
        documentoCabecera.precioTotal = new Decimal(0);
        documentoCabecera.valorDescuento = new Decimal(0);
        documentoCabecera.valorInafecto = new Decimal(0);
        documentoCabecera.documentoDetalles = [];
        this.productos.forEach(pro => {
            documentoCabecera.documentoDetalles.push({
                cantidad: this.utilServicio.redondear(pro.cantidad, 2),
                costoUnitario: new Decimal(0),
                productoServicio: {
                    id: pro.id,
                    nombre: pro.nombre
                },
                valorDscto: new Decimal(0),
                valorTotal: new Decimal(0)
            });
            documentoCabecera.precioTotal = new Decimal(0);
            documentoCabecera.valorDescuento = new Decimal(0);
            documentoCabecera.valorInafecto = new Decimal(0);
        });
        documentoCabecera.valorTotal = new Decimal(0);
        return documentoCabecera;
    }

    validarTransferencia() {
        if (!this.utilServicio.haSeleccionadoEntidad(this.documentoCabecera.sucursal)) {
            this.messageDialog.showDialog('Debe seleccionar sucursal de origen');
            return false;
        }
        if (!this.utilServicio.haSeleccionadoEntidad(this.documentoCabecera.sucursalDestino)) {
            this.messageDialog.showDialog('Debe seleccionar sucursal de destino');
            return false;
        }
        if (this.documentoCabecera.sucursal.id == this.documentoCabecera.sucursalDestino.id) {
            this.messageDialog.showDialog('El almacén de destino debe ser distinto al almacén de origen');
            return false;
        }
        if (this.productos.length == 0) {
            this.messageDialog.showDialog('Debe seleccionar al menos un producto');
            return false;
        }
        for (let i = 0; i < this.productos.length; i++) {
            if (!this.utilServicio.esNumero(this.productos[i].cantidad) || new Decimal(this.productos[i].cantidad).comparedTo(0) == 0) {
                this.messageDialog.showDialog('La cantidad del producto ' + this.productos[i].nombre + ' debe ser mayor a 0');
                return false;
            }
        }
        return true;
    }

    nuevo() {
        this.componentesInactivos = false;
        this.utilServicio.nuevo(this.botones);
        // Asignamos la sucursal por defecto
        this.documentoCabecera.sucursal.id = this.usuario.sucursalId;
    }

    guardar() {
        if (this.validarTransferencia()) {
            this.utilServicio.showLoader();
            this.apiServicio.guardar('documentoCabecera/requerimientoTransferencia', this.obtenerTransferenciaParaGuardar())
                .then((result) => {
                    if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                        this.documentoCabecera.id = result.data;
                    }
                    this.messageDialog.showDialog(result.mensaje);
                    this.utilServicio.hideLoader();
                    this.cancelar();
                }).catch((error) => {
                    this.utilServicio.hideLoader();
                    this.messageDialog.showDialog('Error al conectar al servidor');
                });
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.componentesInactivos = false;
    }

    consultar() {
        this.abrirSelectorRequerimientoTransferencia();
    }

    visualizar() {
        this.parametros.nombreReporte = "requerimiento_transferencia";
        this.parametros.documentoId = String(this.documentoCabecera.id);
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    cancelar() {
        this.componentesInactivos = true;
        this.utilServicio.cancelar(this.botones);
        this.limpiarFormulario();
        this.obtenerSucursales();
    }

    limpiarFormulario() {
        this.documentoCabecera = new DocumentoCabeceraEntidad();
        this.documentoCabecera.sucursal = new SucursalEntidad();
        this.documentoCabecera.sucursalDestino = new SucursalEntidad();
        this.documentoCabecera.tipoComprobante = new TipoComprobanteEntidad();
        this.productos = [];
        this.producto = new ProductoDTOEntidad();
        this.sucursalesOrigen = [];
        this.sucursalesDestino = [];
    }

    ngOnInit() {
        this.cancelar();
    }
}