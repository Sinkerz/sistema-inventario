import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { TipoUsuarioEntidad } from '../../entidad/tipo.usuario';
import { TipoUsuarioFormulario } from '../formulario/tipo.usuario.formulario';
import { TipoUsuarioBuscador } from '../buscador/tipo.usuario.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { MessageDialog } from '../otro/message.dialog';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';

@Component({
    selector: 'tipo-usuario',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/tipo.usuario.html'
})
export class TipoUsuario { 

    tipoUsuario: TipoUsuarioEntidad;
    botones;
    parametros: any;

    @ViewChild(TipoUsuarioFormulario) tipoUsuarioFormulario;
    @ViewChild(TipoUsuarioBuscador) tipoUsuarioBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

     constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.tipoUsuario = new TipoUsuarioEntidad();
        this.botones = {};
        this.parametros = {nombreReporte: "tipo_usuario", formatoExportacion: "PDF"}
    }
 
    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.tipoUsuarioFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.tipoUsuarioFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.tipoUsuarioFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.tipoUsuarioBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string){
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        // this.messageDialog.showDialog('esto es un mensaje');
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar el tipo de usuario?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('tipoUsuario', this.tipoUsuario.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.tipoUsuarioBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.tipoUsuario = new TipoUsuarioEntidad();
        this.utilServicio.cancelar(this.botones);
        this.tipoUsuarioFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.tipoUsuario.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre del tipo de usuario.');
            return false;
        }
        return true;
    }

    seleccionarTipoUsuario(tipoUsuario: TipoUsuarioEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('tipoUsuario', tipoUsuario.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.tipoUsuario = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar(){
        let paramBusq = this.tipoUsuarioBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}