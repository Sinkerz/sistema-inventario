import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { CategoriaEntidad } from '../../entidad/categoria';
import { CategoriaFormulario } from '../formulario/categoria.formulario';
import { CategoriaBuscador } from '../buscador/categoria.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { MessageDialog } from '../otro/message.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';

@Component({
    selector: 'categoria',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/categoria.html'
})
export class Categoria {

    categoria: CategoriaEntidad;
    botones;
    parametros: any;

    @ViewChild(CategoriaFormulario) categoriaFormulario;
    @ViewChild(CategoriaBuscador) categoriaBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

     constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.categoria = new CategoriaEntidad();
        this.botones = {};
        this.parametros = { nombreReporte: "categoria", formatoExportacion: "PDF" }
    }

    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.categoriaFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.categoriaFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.categoriaFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.categoriaBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
         this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string) {
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        // this.messageDialog.showDialog('esto es un mensaje');
        this.confirmDialog.confirm('¿Está seguro que desea eliminar la categoría?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('categoria', this.categoria.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.categoriaBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                         this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.utilServicio.cancelar(this.botones);
        this.categoriaFormulario.activarFormulario(false);
        this.categoriaFormulario.limpiarFormulario();
    }

    seleccionarCategoria(categoria: CategoriaEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('categoria', categoria.id)
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.categoria = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                 this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.categoria.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre de la categoría.');
            return false;
        }
        return true;
    }

    visualizar() {
        let paramBusq = this.categoriaBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}