import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild, Inject } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { Router } from '@angular/router';
import { UsuarioEntidad } from '../../entidad/usuario';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { MessageDialog } from '../otro/message.dialog';
import { RespuestaServicio } from '../../util/respuesta.servicio';

@Component({
    selector: 'login',
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    templateUrl: '../../plantilla/principal/logeo.html',
    styleUrls: ['../../recurso/css/componente/logeo.css']
})
export class Logeo implements OnInit {

    @ViewChild(MessageDialog) messageDialog: MessageDialog;

    usuario: UsuarioEntidad;

    windowHeight: string;

    error: string;

    sucursales: SelectItem[];

    sucursal: any;

    @ViewChild("toFocus") inputFocus: ElementRef;

    constructor( @Inject(Router) private router: Router, @Inject(ApiServicio) private apiServicio: ApiServicio, @Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.windowHeight = window.innerHeight + "px";
        this.utilServicio.eliminarUsuarioSession();
    }

    ngOnInit() {
        this.limpiarFormulario();
    }

    limpiarFormulario() {
        this.usuario = new UsuarioEntidad();
        this.sucursales = [];
        this.sucursal = {};
        this.usuario.trabajador = null;
        this.usuario.usuarioAccesos = null;
        this.utilServicio.setFocus(this.inputFocus);
    }

    validarLogin() {
        //this.utilServicio.showLoader();
        this.usuario.sucursalId = this.sucursal.id;
        this.apiServicio.consultar('usuario/logeo', this.usuario)
            .then((result) => {
               // this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.iniciarSesion(result);
                } else {
                    this.error = result.mensaje;
                }
            }).catch((error) => {
                //this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    obtenerSucursales() {
        if (!this.utilServicio.esNullOUndefinedOVacio(this.usuario.login)) {
           // this.utilServicio.showLoader();
            this.apiServicio.obtenerTodos('usuario/sucursal/' + this.usuario.login)
                .then((result) => {
                    //this.utilServicio.hideLoader();
                    if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                        if (result.data.length > 0) {
                            this.sucursales = [];
                            this.utilServicio.meterResultadoConsultaEnListaItems(result, this.sucursales);
                            this.sucursal = result.data[0];
                        } else {
                            this.sucursales = [{ label: "SIN SUCURSALES", value: {} }];
                        }
                    } else {
                        this.error = result.mensaje;
                    }
                }).catch((error) => {
                    //this.utilServicio.hideLoader();
                    this.messageDialog.showDialog('Error al conectar con el servidor');
                });
        }
    }

    esFormularioValido() {
        return !this.utilServicio.esNullOUndefinedOVacio(this.usuario.login) && !this.utilServicio.esNullOUndefinedOVacio(this.usuario.clave) && this.utilServicio.esNumero(this.sucursal.id);
    }

    private iniciarSesion(result: RespuestaServicio) {
        this.utilServicio.iniciarSesion(this.router, result);
    }

}
