import { Component, ViewChild, Input, ViewEncapsulation, Inject } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { Tabs } from '../otro/tabs';
import { Router } from '@angular/router';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'inicio',
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    templateUrl: '../../plantilla/principal/inicio.html',
    styleUrls: ['../../recurso/css/componente/inicio.css']
})
export class Inicio {

    @ViewChild(Tabs) tabs;

    @ViewChild('tipo.usuario')
    tipoUsuarioTemplate: TemplateRef<any>;

    @ViewChild('unidad')
    unidadTemplate: TemplateRef<any>;

    @ViewChild('tipo.documento')
    tipoDocumentoTemplate: TemplateRef<any>;

    @ViewChild('sucursal')
    sucursalTemplate: TemplateRef<any>;

    @ViewChild('tipo.motivo')
    tipoMotivoTemplate: TemplateRef<any>;

    @ViewChild('marca')
    marcaTemplate: TemplateRef<any>;

    @ViewChild('categoria')
    categoriaTemplate: TemplateRef<any>;

    @ViewChild('usuario')
    usuarioTemplate: TemplateRef<any>;

    @ViewChild('permiso')
    permisoTemplate: TemplateRef<any>;

    @ViewChild('persona')
    personaTemplate: TemplateRef<any>;

    @ViewChild('proveedor')
    proveedorTemplate: TemplateRef<any>;

    @ViewChild('trabajador')
    trabajadorTemplate: TemplateRef<any>;

    @ViewChild('producto')
    productoTemplate: TemplateRef<any>;

    @ViewChild('requerimiento.transferencia')
    requerimientoTransferenciaTemplate: TemplateRef<any>;

    @ViewChild('orden.compra')
    ordenCompraTemplate: TemplateRef<any>;



    constructor( @Inject(Router) private router: Router, @Inject(UtilServicio) private utilServicio: UtilServicio, @Inject(ApiServicio) private apiServicio: ApiServicio) {
        this.apiServicio.setHeaders();
    }

    cerrarSesion() {
        this.utilServicio.cerrarSesion(this.router);
    }

    openTab(data: any) {
        let url = data.url;
        let titulo = data.titulo;
        let template: TemplateRef<any>;
        if (url == 'tipo_usuario') {
            template = this.tipoUsuarioTemplate;
        } else if (url == 'unidad') {
            template = this.unidadTemplate;
        } else if (url == 'tipo_documento') {
            template = this.tipoDocumentoTemplate;
        } else if (url == 'sucursal') {
            template = this.sucursalTemplate;
        } else if (url == 'tipo_motivo') {
            template = this.tipoMotivoTemplate;
        } else if (url == 'marca') {
            template = this.marcaTemplate;
        } else if (url == 'categoria') {
            template = this.categoriaTemplate;
        } else if (url == 'usuario') {
            template = this.usuarioTemplate;
        } else if (url == 'permiso') {
            template = this.permisoTemplate;
        } else if (url == 'persona') {
            template = this.personaTemplate;
        } else if (url == 'trabajador') {
            template = this.trabajadorTemplate;
        } else if (url == 'producto') {
            template = this.productoTemplate;
        } else if (url == 'proveedor') {
            template = this.proveedorTemplate;
        }else if (url == 'requerimiento_transferencia') {
            template = this.requerimientoTransferenciaTemplate;
        } else if (url == 'orden_compra') {
            template = this.ordenCompraTemplate;
        }
        this.tabs.openTab(titulo, template, url);
    }
}
