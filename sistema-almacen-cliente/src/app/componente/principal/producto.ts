import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { ProductoEntidad } from '../../entidad/producto';
import { ProductoFormulario } from '../formulario/producto.formulario';
import { ProductoBuscador } from '../buscador/producto.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { MessageDialog } from '../otro/message.dialog';

@Component({
    selector: 'producto',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/producto.html'
})
export class Producto {

    producto: ProductoEntidad;
    botones;
    parametros: any;

    @ViewChild(ProductoFormulario) productoFormulario;
    @ViewChild(ProductoBuscador) productoBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

    constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.producto = new ProductoEntidad();
        this.botones = {};
        this.parametros = { nombreReporte: "producto", formatoExportacion: "PDF" }
    }

    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.productoFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.productoFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.productoFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaProducto: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaProducto)) {
            this.messageDialog.showDialog(respuestaProducto.mensaje);
            this.cancelar();
            this.productoBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaProducto.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string) {
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar el producto?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('producto', this.producto.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.productoBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.producto = new ProductoEntidad();
        this.utilServicio.cancelar(this.botones);
        this.productoFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.producto.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre del producto.');
            return false;
        }
        if (!this.utilServicio.haSeleccionadoEntidad(this.producto.unidad)) {
            this.messageDialog.showDialog('Debe seleccionar la unidad.');
            return false;
        }
        if (!this.utilServicio.haSeleccionadoEntidad(this.producto.categoria)) {
            this.messageDialog.showDialog('Debe seleccionar la categoria.');
            return false;
        }
        if (!this.utilServicio.haSeleccionadoEntidad(this.producto.marca)) {
            this.messageDialog.showDialog('Debe seleccionar la marca.');
            return false;
        }
        return true;
    }

    seleccionarProducto(Producto: ProductoEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('producto', Producto.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.producto = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar() {
        let paramBusq = this.productoBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.parametros.codigo = paramBusq.codigo;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

} 