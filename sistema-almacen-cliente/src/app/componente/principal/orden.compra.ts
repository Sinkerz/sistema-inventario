import { Component, Input, Output, EventEmitter, ViewChild, Inject } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { MessageDialog } from '../otro/message.dialog';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { DocumentoCabeceraEntidad } from '../../entidad/documento.cabecera';
import { DocumentoDetalleEntidad } from '../../entidad/documento.detalle';
import { ProveedorEntidad } from '../../entidad/proveedor';
import { TipoMonedaEntidad } from '../../entidad/tipo.moneda';
import { ProductoDTOEntidad } from '../../entidad/dto/producto.dto';
import { SucursalEntidad } from '../../entidad/sucursal';
import { PersonaEntidad } from '../../entidad/persona';
import { ProveedorBuscadorModal } from '../modal/proveedor.buscador.modal';
import { ProductoProveedorBuscadorModal } from '../modal/producto.proveedor.buscador.modal';
import { OrdenCompraBuscadorModal } from '../modal/orden.compra.buscador.modal';
import * as Decimal from 'decimal.js';

@Component({
    selector: 'orden-compra',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/orden.compra.html'
})

export class OrdenCompra {

    botones;
    parametros: any;

    documentoCabecera: DocumentoCabeceraEntidad;
    documentoDetalle: DocumentoDetalleEntidad;
    tiposMoneda: SelectItem[];
    productos: ProductoDTOEntidad[];
    usuarioSession: any;
    producto: ProductoDTOEntidad;
    tipoMoneda: string;

    private componentesInactivos: boolean;

    @Input() proveedor: ProveedorEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild(ProveedorBuscadorModal) proveedorBuscadorModal;
    @ViewChild(OrdenCompraBuscadorModal) ordenCompraBuscadorModal;
    @ViewChild(ProductoProveedorBuscadorModal) productoProveedorBuscadorModal;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

    constructor( @Inject(ConfirmDialog) private confirmDialog: ConfirmDialog, @Inject(ApiServicio) private apiServicio: ApiServicio, @Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.tiposMoneda = [];
        this.usuarioSession = this.utilServicio.obtenerUsuarioLogeado();
        this.componentesInactivos = false;
        this.botones = {};
        this.parametros = { nombreReporte: "orden_compra", formatoExportacion: "PDF" };
    }

    obtenerTodosTipoMoneda() {
        this.utilServicio.showLoader();
        this.apiServicio.obtenerTodos('tipoMoneda')
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.utilServicio.meterResultadoConsultaEnListaItems(result, this.tiposMoneda, 'nombre', 'id');
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    cambioTipoMoneda() {
        for (let i = 0; i < this.tiposMoneda.length; i++) {
            if (this.documentoCabecera.tipoMoneda.id == this.tiposMoneda[i].value) {
                this.tipoMoneda = this.tiposMoneda[i].label;
            }
        }
    }

    guardar() {
        if (this.validarProceso()) {
            this.utilServicio.showLoader();
            this.apiServicio.guardar('documentoCabecera/ordenCompra', this.obtenerOrdenCompraGuardar())
                .then((result) => {
                    if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                        this.documentoCabecera.id = result.data;
                        this.componentesInactivos = true;
                        this.utilServicio.cancelar(this.botones);
                    }
                    this.messageDialog.showDialog(result.mensaje);
                    this.utilServicio.hideLoader();
                }).catch((error) => {
                    this.utilServicio.hideLoader();
                    this.messageDialog.showDialog('Error al guardar la orden de compra');
                    console.log(error);
                });
        }
    }

    obtenerOrdenCompraGuardar() {
        let documentoCabecera: DocumentoCabeceraEntidad = (<any>Object).assign({}, this.documentoCabecera);
        let documentoDetalle: DocumentoDetalleEntidad;
        let total: decimal.Decimal;
        documentoCabecera.precioTotal = new Decimal(0);
        documentoCabecera.valorDescuento = new Decimal(0);
        documentoCabecera.valorInafecto = new Decimal(0);
        documentoCabecera.documentoDetalles = [];
        documentoCabecera.valorTotal = new Decimal(0);
        documentoCabecera.sucursal.id = this.usuarioSession.sucursalId;
        documentoCabecera.clientePuntos = null;
        //documentoCabecera.mecanico = null;
        this.productos.forEach(pro => {
            total = this.utilServicio.redondear(pro.cantidad, 2).times(pro.precioNormal);
            documentoCabecera.documentoDetalles.push({
                cantidad: this.utilServicio.redondear(pro.cantidad, 2),
                costoUnitario: pro.precioNormal,
                productoServicio: {
                    id: pro.id
                },
                valorDscto: new Decimal(0),
                valorTotal: total,
                valorInafecto: new Decimal(0),
                valorVenta: new Decimal(0)
            });

            documentoCabecera.valorTotal = documentoCabecera.valorTotal.plus(total);
            if (this.utilServicio.esNumero(pro.precioNormal)) {
                documentoCabecera.precioTotal = new Decimal(pro.precioNormal).plus(documentoCabecera.precioTotal);
            }
        });
        return documentoCabecera;
    }

    validarProceso() {
        if (!this.utilServicio.haSeleccionadoEntidad(this.documentoCabecera.persona)) {
            this.messageDialog.showDialog('Debe seleccionar el proveedor');
            return false;
        }
        if (!this.utilServicio.haSeleccionadoEntidad(this.documentoCabecera.tipoMoneda)) {
            this.messageDialog.showDialog('Debe seleccionar el tipo de moneda');
            return false;
        }
        if (this.productos.length == 0) {
            this.messageDialog.showDialog('Debe seleccionar al menos un producto');
            return false;
        }
        for (let i = 0; i < this.productos.length; i++) {
            if (!this.utilServicio.esNumero(this.productos[i].precioNormal)) {
                this.messageDialog.showDialog('El precio del producto ' + this.productos[i].nombre + ' no es válido');
                return false;
            }
            if (!this.utilServicio.esNumero(this.productos[i].cantidad) || new Decimal(this.productos[i].cantidad).comparedTo(0) == 0) {
                this.messageDialog.showDialog('La cantidad del producto ' + this.productos[i].nombre + ' debe ser mayor a 0');
                return false;
            }
        }
        return true;
    }

    abrirSelectorOrdenCompra() {
        this.ordenCompraBuscadorModal.mostrarModal();
    }

    seleccionarOrdenCompra(ordenCompra: DocumentoCabeceraEntidad) {
        // this.documentoCabecera = {
        // }

    }

    abrirSelectorProveedor() {
        this.proveedorBuscadorModal.mostrarModal();
    }

    seleccionarProveedor(proveedor: ProveedorEntidad) {
        this.documentoCabecera.persona = {
            id: proveedor.persona.id,
            nombre: proveedor.persona.nombre,
            //  numeroDocumento: proveedor.persona.numeroDocumento
        }
    }

    abrirSelectorProducto() {
        this.productoProveedorBuscadorModal.mostrarModal(this.documentoCabecera.persona.id);
    }

    seleccionarProducto(producto: ProductoDTOEntidad) {
        if (this.validarProducto(producto)) {
            this.productos.push({
                id: producto.id,
                codigo: producto.codigo,
                nombre: producto.nombre,
                marca: producto.marca,
                precioNormal: new Decimal(producto.precioNormal),
                cantidad: producto.cantidad
            });
        }
    }

    eliminarProducto(index) {
        this.productos.splice(index, 1);
    }

    validarProducto(producto: ProductoDTOEntidad) {
        return !(this.utilServicio.buscarEnArregloPorPropiedad(producto.id, this.productos, 'id') >= 0);
    }

    limpiarFormulario() {
        this.componentesInactivos = false;
        this.documentoCabecera = new DocumentoCabeceraEntidad();
        this.documentoCabecera.sucursal = new SucursalEntidad();
        this.documentoCabecera.persona = new PersonaEntidad();
        this.documentoCabecera.tipoMoneda = new TipoMonedaEntidad();
        this.documentoDetalle = new DocumentoDetalleEntidad();
        this.producto = new ProductoDTOEntidad();
        this.productos = [];
        this.tipoMoneda = '';
    }

    agregarProducto() {
        if (!this.utilServicio.haSeleccionadoEntidad(this.producto)) {
            this.messageDialog.showDialog('Debe seleccionar un producto');
            return;
        }
        if (!this.utilServicio.esNumero(this.producto.cantidad)) {
            this.messageDialog.showDialog('Debe ingresar una cantidad válida');
            this.producto.cantidad = null;
            return;
        }
        if (new Decimal(this.producto.cantidad).comparedTo(new Decimal(0)) != 1) {
            this.messageDialog.showDialog('Debe ingresar una cantidad mayor a cero');
            this.producto.cantidad = null;
            return;
        }
        if (!this.validarProducto(this.producto)) {
            this.messageDialog.showDialog('El producto ya fue agregado');
            this.producto = new ProductoDTOEntidad();
            return;
        }
        this.seleccionarProducto(this.producto);
        this.producto = new ProductoDTOEntidad();
    }

    obtenerProductoPorCodigo() {
        if (!this.utilServicio.esNullOUndefinedOVacio(this.producto.codigo)) {
            this.utilServicio.showLoader();
            this.apiServicio.obtener('producto/codigo', this.producto.codigo)
                .then((result) => {
                    if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                        if (this.utilServicio.haSeleccionadoEntidad(result.data)) {
                            this.producto = result.data;
                        } else {
                            this.producto = new ProductoDTOEntidad();
                            this.messageDialog.showDialog("No se encontró producto con el código especificado.");
                        }
                    } else {
                        this.messageDialog.showDialog(result.mensaje);
                    }
                    this.utilServicio.hideLoader();
                }).catch((error) => {
                    this.utilServicio.hideLoader();
                    this.messageDialog.showDialog('Error al conectar con el servidor');
                });
        }
    }

    calcularTotales(pTotal: string, pMult: string = undefined): decimal.Decimal {
        let total = new Decimal(0);
        this.productos.forEach(pro => {
            if (this.utilServicio.esNumero(pro[pTotal])) {
                if (!pMult) {
                    total = total.plus(pro[pTotal]);
                } else if (this.utilServicio.esNumero(pro[pMult])) {
                    total = total.plus(this.utilServicio.redondear(new Decimal(pro[pTotal]).times(pro[pMult]), 2));
                }
            }
        });
        return this.utilServicio.redondear(total, 2);
    }

    multiplicarValores(v1: number, v2: number): decimal.Decimal {
        v1 = this.utilServicio.esNumero(v1) ? v1 : 0;
        v2 = this.utilServicio.esNumero(v2) ? v2 : 0;
        return this.utilServicio.redondear(new Decimal(v1).times(v2), 2);
    }

    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.limpiarFormulario();
        this.componentesInactivos = false;
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.componentesInactivos = false;
    }
    cancelar() {
        this.utilServicio.cancelar(this.botones);
        this.limpiarFormulario();
        this.componentesInactivos = true;
    }

    buscar() {
        this.abrirSelectorOrdenCompra();
    }

    ngOnInit() {
        this.cancelar();
        this.obtenerTodosTipoMoneda();
    }
    visualizar() {
        this.parametros.documentoId = String(this.documentoCabecera.id);
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }
}