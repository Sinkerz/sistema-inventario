import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { ClienteEntidad } from '../../entidad/cliente';
import { ClienteFormulario } from '../formulario/cliente.formulario';
import { ClienteBuscador } from '../buscador/cliente.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { MessageDialog } from '../otro/message.dialog';

@Component({
    selector: 'cliente',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/cliente.html'
})
export class Cliente {

    cliente: ClienteEntidad;
    botones;
    parametros: any;

    @ViewChild(ClienteFormulario) clienteFormulario;
    @ViewChild(ClienteBuscador) clienteBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

    constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.cliente = new ClienteEntidad();
        this.botones = {};
        this.parametros = {nombreReporte: "cliente", formatoExportacion: "PDF"}
    }
 
    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.clienteFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.clienteFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.clienteFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.clienteBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string){
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar el cliente?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('cliente', this.cliente.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.clienteBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.cliente = new ClienteEntidad();
        this.utilServicio.cancelar(this.botones);
        this.clienteFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (!this.utilServicio.haSeleccionadoEntidad(this.cliente.persona)) {
            this.messageDialog.showDialog('Debe seleccionar la persona.');
            return false;
        }
        return true;
    }

    seleccionarCliente(cliente: ClienteEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('cliente', cliente.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.cliente = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar(){
        let paramBusq = this.clienteBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.parametros.apellidos = paramBusq.apellidos;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}