import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { PersonaEntidad } from '../../entidad/persona';
import { PersonaFormulario } from '../formulario/persona.formulario';
import { PersonaBuscador } from '../buscador/persona.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { MessageDialog } from '../otro/message.dialog';

@Component({
    selector: 'persona',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/persona.html'
})
export class Persona {

    persona: PersonaEntidad;
    botones;
    parametros: any;

    @ViewChild(PersonaFormulario) personaFormulario;
    @ViewChild(PersonaBuscador) personaBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

     constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.persona = new PersonaEntidad();
        this.botones = {};
        this.parametros = { nombreReporte: "persona", formatoExportacion: "PDF" }
    }

    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.personaFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.personaFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.personaFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.personaBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string) {
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar a la persona?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('persona', this.persona.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.personaBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.persona = new PersonaEntidad();
        this.utilServicio.cancelar(this.botones);
        this.personaFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.persona.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre del persona.');
            return false;
        }
        if (!this.utilServicio.haSeleccionadoEntidad(this.persona.tipoDocumento)) {
            this.messageDialog.showDialog('Debe seleccionar el tipo de documento.');
            return false;
        }
        if (this.utilServicio.trim(this.persona.tipoDocumento.abreviatura) == "DNI" && String(this.persona.numeroDocumento).length != 8) {
            this.messageDialog.showDialog('Debe ingresar un número de documento válido.');
            return false;
        }
        if (this.utilServicio.trim(this.persona.tipoDocumento.abreviatura) == "RUC" && !this.utilServicio.esRucValido(this.persona.numeroDocumento)) {
            this.messageDialog.showDialog('Debe ingresar un número de documento válido.');
            return false;
        }
        return true;
    }

    seleccionarPersona(persona: PersonaEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('persona', persona.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.persona = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar() {
        let paramBusq = this.personaBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.parametros.apellidos = paramBusq.apellidos;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}