import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { TipoMotivoEntidad } from '../../entidad/tipo.motivo';
import { TipoMotivoFormulario } from '../formulario/tipo.motivo.formulario';
import { TipoMotivoBuscador } from '../buscador/tipo.motivo.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { MessageDialog } from '../otro/message.dialog';

@Component({
    selector: 'tipo-motivo',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/tipo.motivo.html'
})
export class TipoMotivo { 

    tipoMotivo: TipoMotivoEntidad;
    botones;
    parametros: any;

    @ViewChild(TipoMotivoFormulario) tipoMotivoFormulario;
    @ViewChild(TipoMotivoBuscador) tipoMotivoBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

    constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.tipoMotivo = new TipoMotivoEntidad();
        this.botones = {};
        this.parametros = {nombreReporte: "tipo_motivo", formatoExportacion: "PDF"}
    }
 
    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.tipoMotivoFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.tipoMotivoFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.tipoMotivoFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.tipoMotivoBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string){
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar el tipo de motivo?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('tipoMotivo', this.tipoMotivo.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.tipoMotivoBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.tipoMotivo = new TipoMotivoEntidad();
        this.utilServicio.cancelar(this.botones);
        this.tipoMotivoFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.tipoMotivo.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre del tipo de motivo.');
            return false;
        }
        return true;
    }

    seleccionarTipoMotivo(tipoMotivo: TipoMotivoEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('tipoMotivo', tipoMotivo.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.tipoMotivo = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar(){
        let paramBusq = this.tipoMotivoBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}