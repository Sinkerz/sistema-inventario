import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsuarioEntidad } from '../../entidad/usuario';
import { UsuarioFormulario } from '../formulario/usuario.formulario';
import { UsuarioBuscador } from '../buscador/usuario.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { MessageDialog } from '../otro/message.dialog';

@Component({
    selector: 'usuario',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/usuario.html'
})
export class Usuario {

    usuario: UsuarioEntidad;
    botones;
    parametros: any;

    @ViewChild(UsuarioFormulario) usuarioFormulario;
    @ViewChild(UsuarioBuscador) usuarioBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

  constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.usuario = new UsuarioEntidad();
        this.botones = {};
        this.parametros = { nombreReporte: "usuario", formatoExportacion: "PDF" }
    }

    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.usuarioFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.usuarioFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.usuarioFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.usuarioBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string) {
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar el usuario?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('usuario', this.usuario.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.usuarioBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.usuario = new UsuarioEntidad();
        this.utilServicio.cancelar(this.botones);
        this.usuarioFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (!this.utilServicio.haSeleccionadoEntidad(this.usuario.trabajador)) {
            this.messageDialog.showDialog('Debe seleccionar el trabajador.');
            return false;
        }
        return true;
    }

    seleccionarUsuario(usuario: UsuarioEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('usuario', usuario.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.usuario = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar() {
        let paramBusq = this.usuarioBuscador.getParametrosBusq();
        this.parametros.login = paramBusq.login;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}