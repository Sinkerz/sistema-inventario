import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MarcaEntidad } from '../../entidad/marca';
import { MarcaFormulario } from '../formulario/marca.formulario';
import { MarcaBuscador } from '../buscador/marca.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { MessageDialog } from '../otro/message.dialog';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';

@Component({
    selector: 'marca',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/marca.html'
})
export class Marca {

    marca: MarcaEntidad;
    botones;
    parametros: any;

    @ViewChild(MarcaFormulario) marcaFormulario;
    @ViewChild(MarcaBuscador) marcaBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

     constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.marca = new MarcaEntidad();
        this.botones = {};
        this.parametros = {nombreReporte: "marca", formatoExportacion: "PDF"}
    }
 
    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.marcaFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.marcaFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.marcaFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.cancelar();
            this.marcaBuscador.consultar();
        }
        this.messageDialog.showDialog(respuestaServicio.mensaje);
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string){
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        // this.messageDialog.showDialog('esto es un mensaje');
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar la marca?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('marca', this.marca.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.marcaBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.marca = new MarcaEntidad();
        this.utilServicio.cancelar(this.botones);
        this.marcaFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.marca.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre de la marca.');
            return false;
        }
        return true;
    }

    seleccionarMarca(marca: MarcaEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('marca', marca.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.marca = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar(){
        let paramBusq = this.marcaBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }
}