import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { TipoDocumentoEntidad } from '../../entidad/tipo.documento';
import { TipoDocumentoFormulario } from '../formulario/tipo.documento.formulario';
import { TipoDocumentoBuscador } from '../buscador/tipo.documento.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { MessageDialog } from '../otro/message.dialog';

@Component({ 
    selector: 'tipo-documento',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/tipo.documento.html'
})
export class TipoDocumento {

    tipoDocumento: TipoDocumentoEntidad;
    botones;
    parametros: any;

    @ViewChild(TipoDocumentoFormulario) tipoDocumentoFormulario;
    @ViewChild(TipoDocumentoBuscador) tipoDocumentoBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

    constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.tipoDocumento = new TipoDocumentoEntidad();
        this.botones = {};
        this.parametros = {nombreReporte: "tipo_documento", formatoExportacion: "PDF"}
    }
 
    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.tipoDocumentoFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.tipoDocumentoFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.tipoDocumentoFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.tipoDocumentoBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string){
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar el tipo de documento?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('tipoDocumento', this.tipoDocumento.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.tipoDocumentoBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.tipoDocumento = new TipoDocumentoEntidad();
        this.utilServicio.cancelar(this.botones);
        this.tipoDocumentoFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.tipoDocumento.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre del tipo de documento.');
            return false;
        }
        if(!this.utilServicio.esCadenaLongitudValida(this.tipoDocumento.codigoSunat,2,false)){
            this.messageDialog.showDialog('La longitud del código de sunat debe ser 2.');
            return false;
        }
        return true;
    }

    seleccionarTipoDocumento(tipoDocumento: TipoDocumentoEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('tipoDocumento', tipoDocumento.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.tipoDocumento = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar(){
        let paramBusq = this.tipoDocumentoBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.parametros.abreviatura = paramBusq.abreviatura;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}