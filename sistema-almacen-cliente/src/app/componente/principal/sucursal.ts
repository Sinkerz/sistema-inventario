import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { SucursalEntidad } from '../../entidad/sucursal';
import { SucursalFormulario } from '../formulario/sucursal.formulario';
import { SucursalBuscador } from '../buscador/sucursal.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { MessageDialog } from '../otro/message.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';

@Component({
    selector: 'sucursal',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/sucursal.html'
})
export class Sucursal {

    sucursal: SucursalEntidad;
    botones;
    parametros: any;

    @ViewChild(SucursalFormulario) sucursalFormulario;
    @ViewChild(SucursalBuscador) sucursalBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

    constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.sucursal = new SucursalEntidad();
        this.botones = {};
        this.parametros = { nombreReporte: "sucursal", formatoExportacion: "PDF" }
    }

    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.sucursalFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.sucursalFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.sucursalFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.sucursalBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string) {
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Está seguro que desea eliminar la sucursal?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('sucursal', this.sucursal.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.sucursalBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.utilServicio.cancelar(this.botones);
        this.sucursalFormulario.activarFormulario(false);
        this.sucursalFormulario.limpiarFormulario();
    }

    seleccionarSucursal(sucursal: SucursalEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('sucursal', sucursal.id)
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.sucursal = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.sucursal.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre de la sucursal.');
            return false;
        }
        if (this.utilServicio.esNullOUndefinedOVacio(this.sucursal.direccion)) {
            this.messageDialog.showDialog('Debe ingresar la dirección de la sucursal.');
            return false;
        }
         if (this.utilServicio.esNullOUndefinedOVacio(this.sucursal.direccion) || this.sucursal.serie.length != 4) {
            this.messageDialog.showDialog('Debe ingresar la dirección de la sucursal.');
            return false;
        }
        return true;
    }

    visualizar() {
        let paramBusq = this.sucursalBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}