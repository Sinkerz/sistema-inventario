import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { ProveedorEntidad } from '../../entidad/proveedor';
import { ProveedorFormulario } from '../formulario/proveedor.formulario';
import { ProveedorBuscador } from '../buscador/proveedor.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { MessageDialog } from '../otro/message.dialog';

@Component({
    selector: 'proveedor',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/proveedor.html'
})
export class Proveedor {

    proveedor: ProveedorEntidad;
    botones;
    parametros: any;

    @ViewChild(ProveedorFormulario) proveedorFormulario;
    @ViewChild(ProveedorBuscador) proveedorBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

   constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.proveedor = new ProveedorEntidad();
        this.botones = {};
        this.parametros = {nombreReporte: "proveedor", formatoExportacion: "PDF"}
    }
 
    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.proveedorFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.proveedorFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.proveedorFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.proveedorBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string){
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Está seguro que desea eliminar el proveedor?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('proveedor', this.proveedor.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.proveedorBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.proveedor = new ProveedorEntidad();
        this.utilServicio.cancelar(this.botones);
        this.proveedorFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (!this.utilServicio.haSeleccionadoEntidad(this.proveedor.persona)) {
            this.messageDialog.showDialog('Debe seleccionar la persona.');
            return false;
        }
        return true;
    }

    seleccionarProveedor(proveedor: ProveedorEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('proveedor', proveedor.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.proveedor = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar(){
        let paramBusq = this.proveedorBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.parametros.apellidos = paramBusq.apellidos;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}