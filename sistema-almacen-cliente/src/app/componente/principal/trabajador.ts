import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { TrabajadorEntidad } from '../../entidad/trabajador';
import { TrabajadorFormulario } from '../formulario/trabajador.formulario';
import { TrabajadorBuscador } from '../buscador/trabajador.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';
import { MessageDialog } from '../otro/message.dialog';

@Component({
    selector: 'trabajador',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/trabajador.html'
})
export class Trabajador {

    trabajador: TrabajadorEntidad;
    botones;
    parametros: any;

    @ViewChild(TrabajadorFormulario) trabajadorFormulario;
    @ViewChild(TrabajadorBuscador) trabajadorBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

  constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.trabajador = new TrabajadorEntidad();
        this.botones = {};
        this.parametros = {nombreReporte: "trabajador", formatoExportacion: "PDF"}
    }
 
    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.trabajadorFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.trabajadorFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.trabajadorFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.trabajadorBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string){
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        this.confirmDialog.confirm('¿Esta seguro que desea eliminar el trabajador?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('trabajador', this.trabajador.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.trabajadorBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                        this.utilServicio.hideLoader();
                        this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.trabajador = new TrabajadorEntidad();
        this.utilServicio.cancelar(this.botones);
        this.trabajadorFormulario.activarFormulario(false);
    }

    private validarFormulario(): boolean {
        if (!this.utilServicio.haSeleccionadoEntidad(this.trabajador.persona)) {
            this.messageDialog.showDialog('Debe seleccionar la persona.');
            return false;
        }
        return true;
    }

    seleccionarTrabajador(trabajador: TrabajadorEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('trabajador', trabajador.id)
            .then((result) => {
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    result.data.fechaNacimiento = this.utilServicio.parseDate(result.data.fechaNacimiento);
                    result.data.fechaIngreso = this.utilServicio.parseDate(result.data.fechaIngreso);
                    result.data.fechaSalida = this.utilServicio.parseDate(result.data.fechaSalida);
                    this.trabajador = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
                this.utilServicio.hideLoader();
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    visualizar(){
        let paramBusq = this.trabajadorBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.parametros.apellidos = paramBusq.apellidos;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }

}