import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Message } from 'primeng/primeng';
import { UnidadEntidad } from '../../entidad/unidad';
import { UnidadFormulario } from '../formulario/unidad.formulario';
import { UnidadBuscador } from '../buscador/unidad.buscador';
import { ConfirmDialog } from '../otro/confirm.dialog';
import { MessageDialog } from '../otro/message.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { PdfVisualizadorModal } from '../modal/visualizador.pdf.modal';

@Component({
    selector: 'unidad',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/unidad.html'
})
export class Unidad {

    unidad: UnidadEntidad;
    botones;
    parametros: any;

    @ViewChild(UnidadFormulario) unidadFormulario;
    @ViewChild(UnidadBuscador) unidadBuscador;
    @ViewChild(PdfVisualizadorModal) pdfVisualizadorModal;
    @ViewChild(MessageDialog) messageDialog;

 constructor(@Inject(ConfirmDialog) private confirmDialog: ConfirmDialog,@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.unidad = new UnidadEntidad();
        this.botones = {};
        this.parametros = { nombreReporte: "unidad", formatoExportacion: "PDF" }
    }

    nuevo() {
        this.utilServicio.nuevo(this.botones);
        this.unidadFormulario.activarFormulario(true);
    }

    guardar() {
        if (this.validarFormulario()) {
            this.unidadFormulario.guardar();
        }
    }

    modificar() {
        this.utilServicio.modificar(this.botones);
        this.unidadFormulario.activarFormulario(true);
    }

    respuestaFormulario(respuestaServicio: RespuestaServicio) {
        if (this.utilServicio.dataDeServerEsCorrecta(respuestaServicio)) {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
            this.cancelar();
            this.unidadBuscador.consultar();
        } else {
            this.messageDialog.showDialog(respuestaServicio.mensaje);
        }
    }

    respuestaError(error: string) {
        this.messageDialog.showDialog(error);
    }

    respuestaBuscador(mensaje: string) {
        this.messageDialog.showDialog(mensaje);
    }

    eliminar() {
        // this.messageDialog.showDialog('esto es un mensaje');
        this.confirmDialog.confirm('¿Está seguro que desea eliminar la unidad?').then((result) => {
            if (result) {
                this.utilServicio.showLoader();
                this.apiServicio.eliminar('unidad', this.unidad.id)
                    .then((result) => {
                        this.utilServicio.hideLoader();
                        if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                            this.messageDialog.showDialog(result.mensaje);
                            this.cancelar();
                            this.unidadBuscador.consultar();
                        } else {
                            this.messageDialog.showDialog(result.mensaje);
                        }
                    }).catch((error) => {
                         this.messageDialog.showDialog('Error al conectar con el servidor');
                    });
            }
        });
    }

    cancelar() {
        this.utilServicio.cancelar(this.botones);
        this.unidadFormulario.activarFormulario(false);
        this.unidadFormulario.limpiarFormulario();
    }

    seleccionarUnidad(unidad: UnidadEntidad) {
        this.utilServicio.showLoader();
        this.cancelar();
        this.apiServicio.obtener('unidad', unidad.id)
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.unidad = result.data;
                    this.utilServicio.seleccionar(this.botones);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    private validarFormulario(): boolean {
        if (this.utilServicio.esNullOUndefinedOVacio(this.unidad.nombre)) {
            this.messageDialog.showDialog('Debe ingresar el nombre de la unidad.');
            return false;
        }
        if (this.utilServicio.esNullOUndefinedOVacio(this.unidad.abreviatura)) {
            this.messageDialog.showDialog('Debe ingresar la abreviatura de la unidad.');
            return false;
        }
        if(!this.utilServicio.esCadenaLongitudValida(this.unidad.codSunat,3,false)){
            this.messageDialog.showDialog('La longitud del código de sunat debe ser 3.');
            return false;
        }
        return true;
    }

    visualizar() {
        let paramBusq = this.unidadBuscador.getParametrosBusq();
        this.parametros.nombre = paramBusq.nombre;
        this.pdfVisualizadorModal.verReporte(this.parametros);
    }

    ngOnInit() {
        this.cancelar();
    }
 
}