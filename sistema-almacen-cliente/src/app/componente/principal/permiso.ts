import { Component, OnInit, ViewChild,Inject } from '@angular/core';
import { TreeNode, SelectItem } from 'primeng/primeng';
import { TipoUsuarioEntidad } from '../../entidad/tipo.usuario';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { MessageDialog } from '../otro/message.dialog';

@Component({
    selector: 'permiso',
    moduleId: module.id,
    templateUrl: '../../plantilla/principal/permiso.html'
})

export class Permiso {

    nodos: TreeNode[];
    tiposUsuario: SelectItem[];
    idTipoUsuarioSel: number;
    selectedNodes: TreeNode[];
    @ViewChild(MessageDialog) messageDialog;

     constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {

    }

    ngOnInit() {
        this.selectedNodes = [];
        this.tiposUsuario = [];
        this.obtenerTiposUsuario();
    }

    nodeSelect(event) {
        let nodo: TreeNode = event.node;
        nodo.data.estado = true;
        this.selectChilds(nodo.children);
        this.selectParent(nodo.parent);
    }

    selectParent(nodo: TreeNode) {
        if (nodo) {
            nodo.data.estado = true;
            this.selectParent(nodo.parent);
        }
    }

    nodeUnSelect(event) {
        let nodo: TreeNode = event.node;
        nodo.data.estado = false;
        this.unSelectChilds(nodo.children);
    }

    selectChilds(nodos: TreeNode[]) {
        nodos && nodos.forEach(element => {
            element.data.estado = true;
            this.selectChilds(element.children);
        });
    }

    unSelectChilds(nodos: TreeNode[]) {
        nodos && nodos.forEach(element => {
            element.data.estado = false;
            this.unSelectChilds(element.children);
        });
    }

    private setNodosSelected(nodos: TreeNode[]) {
        nodos && nodos.forEach(element => {
            if (element.data.estado) {
                this.selectedNodes.push(element);
            }
            this.setNodosSelected(element.children);
        });
    }

    private obtenerTiposUsuario() {
        this.utilServicio.showLoader();
        this.apiServicio.obtenerTodos('tipoUsuario')
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.tiposUsuario = [];
                    this.utilServicio.meterResultadoConsultaEnListaItems(result, this.tiposUsuario, "nombre", "id");
                    this.idTipoUsuarioSel = this.tiposUsuario.length > 0 ? this.tiposUsuario[0].value : null;
                    this.cambioTipoUsuario();
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    private obtenerPermisos(idTipoUsuario: number) {
        this.utilServicio.showLoader();
        this.apiServicio.obtener('tipoUsuarioMenuOpcion/permisoMenu', idTipoUsuario)
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.nodos = result.data;
                    this.setNodosSelected(this.nodos);
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });
    }

    private cambioTipoUsuario() {
        !this.utilServicio.esNullOUndefinedOVacio(this.idTipoUsuarioSel) && this.obtenerPermisos(this.idTipoUsuarioSel);
    }

    private guardar() {
        let nodosGuardar: any = [];
        this.setNodosParaGuardar(this.nodos, nodosGuardar);
        this.utilServicio.showLoader();
        this.apiServicio.guardar('tipoUsuarioMenuOpcion/permisoMenu', nodosGuardar)
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.cambioTipoUsuario();
                }
                this.messageDialog.showDialog(result.mensaje);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar al servidor');
            });
        console.log(nodosGuardar);
    }

    private setNodosParaGuardar(nodos: TreeNode[], nodosGuardar: any[]) {
        nodos && nodos.forEach(element => {
            if (element.data.estado != element.data.estadoInicial) {
                nodosGuardar.push({
                    data: {
                        id: element.data.id,
                        estado: element.data.estado
                    }
                });
            }
            this.setNodosParaGuardar(element.children, nodosGuardar);
        });
    }
}