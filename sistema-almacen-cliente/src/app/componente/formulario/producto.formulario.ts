import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import { ProductoEntidad } from '../../entidad/producto';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';
import { UnidadEntidad } from '../../entidad/unidad';
import { CategoriaEntidad } from '../../entidad/categoria';
import { MarcaEntidad } from '../../entidad/marca';
import { UnidadBuscadorModal } from '../modal/unidad.buscador.modal';
import { MarcaBuscadorModal } from '../modal/marca.buscador.modal';
import { CategoriaBuscadorModal } from '../modal/categoria.buscador.modal';

@Component({
    selector: 'producto-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/producto.formulario.html'
})
export class ProductoFormulario {
    
    private componentesInactivos: boolean;
    @Input() producto: ProductoEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild("toFocus") inputFocus: ElementRef;
    @ViewChild(UnidadBuscadorModal) unidadBuscadorModal;
    @ViewChild(MarcaBuscadorModal) marcaBuscadorModal;
    @ViewChild(CategoriaBuscadorModal) categoriaBuscadorModal;

    constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    guardar() {
        this.utilServicio.showLoader();
        this.apiServicio.guardar('producto', this.producto)
            .then((result) => {
                this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });
    } 

    abrirSelectorUnidad() {
        this.unidadBuscadorModal.mostrarModal();
    }

    seleccionarUnidad(unidad: UnidadEntidad) {
        this.producto.unidad = {
            id : unidad.id,
            nombre : unidad.nombre
        };
    }

    abrirSelectorCategoria() {
        this.categoriaBuscadorModal.mostrarModal();
    }

    seleccionarCategoria(categoria: CategoriaEntidad) {
        this.producto.categoria = {
            id : categoria.id,
            nombre : categoria.nombre
        };
    }

    abrirSelectorMarca() {
        this.marcaBuscadorModal.mostrarModal();
    }

    seleccionarMarca(marca: MarcaEntidad) {
        this.producto.marca = {
            id : marca.id,
            nombre : marca.nombre
        };
    }
} 