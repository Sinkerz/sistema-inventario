import { Component, Input, Output, EventEmitter, ViewChild, Inject } from '@angular/core';
import { ProveedorEntidad } from '../../entidad/proveedor';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';
import { PersonaBuscadorModal } from '../modal/persona.buscador.modal';
import { PersonaEntidad } from '../../entidad/persona';

@Component({
    selector: 'proveedor-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/proveedor.formulario.html'
})
export class ProveedorFormulario {
    private componentesInactivos: boolean;
    @Input() proveedor: ProveedorEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild(PersonaBuscadorModal) personaBuscadorModal;


  constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
    }

    guardar() {
        //this.utilServicio.showLoader();
        this.apiServicio.guardar('proveedor', this.proveedor)
            .then((result) => {
                //this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
               // this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });
    }

    abrirSelectorPersona() {
        this.personaBuscadorModal.mostrarModal();
    }

    seleccionarPersona(persona: PersonaEntidad) {
        this.proveedor.persona = {
            id: persona.id,
            nombre: persona.nombre
        };
    }
}