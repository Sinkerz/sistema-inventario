import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import { SucursalEntidad } from '../../entidad/sucursal';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'sucursal-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/sucursal.formulario.html'
})
export class SucursalFormulario {
    private componentesInactivos: boolean;
    @Input() sucursal: SucursalEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild("toFocus") inputFocus: ElementRef;

    constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    limpiarFormulario() {
        this.sucursal = new SucursalEntidad();
    }

    guardar() {

        //this.utilServicio.showLoader();
        this.apiServicio.guardar('sucursal', this.sucursal)
            .then((result) => {
                //this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
               // this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });

    }

}