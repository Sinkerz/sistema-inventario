import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import { TipoUsuarioEntidad } from '../../entidad/tipo.usuario';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'tipo-usuario-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/tipo.usuario.formulario.html'
})
export class TipoUsuarioFormulario {

    private componentesInactivos: boolean;
    @Input() tipoUsuario: TipoUsuarioEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    es: any;
    @ViewChild("toFocus") inputFocus: ElementRef;
    
   constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    guardar() {
        this.utilServicio.showLoader();
        this.apiServicio.guardar('tipoUsuario', this.tipoUsuario)
            .then((result) => {
                this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });
    } 

  
    
    ngOnInit() {
        this.es = this.utilServicio.getDateProperties();
    }

}