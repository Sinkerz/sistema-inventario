import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import { TipoMotivoEntidad } from '../../entidad/tipo.motivo';
import { ApiServicio } from '../../servicio/api.servicio';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { UtilServicio } from '../../servicio/util.servicio';


@Component({
    selector: 'tipo-motivo-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/tipo.motivo.formulario.html'
})
export class TipoMotivoFormulario {

     private componentesInactivos: boolean;
    @Input() tipoMotivo: TipoMotivoEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild("toFocus") inputFocus: ElementRef;

    constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    guardar() {
        this.utilServicio.showLoader();
        this.apiServicio.guardar('tipoMotivo', this.tipoMotivo)
            .then((result) => {
                this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });
    } 

}