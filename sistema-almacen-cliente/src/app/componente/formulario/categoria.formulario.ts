import { Component, Input, Output, EventEmitter, ViewChild,ElementRef,Inject } from '@angular/core';
import { CategoriaEntidad } from '../../entidad/categoria';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';


@Component({
    selector: 'categoria-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/categoria.formulario.html'
})
export class CategoriaFormulario {
    private componentesInactivos: boolean;
    @Input() categoria: CategoriaEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild("toFocus") inputFocus: ElementRef;

    constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    limpiarFormulario() {
        this.categoria = new CategoriaEntidad();
    }

    guardar() {
        this.utilServicio.showLoader();
        this.apiServicio.guardar('categoria', this.categoria)
            .then((result) => {
                this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });

    }


}