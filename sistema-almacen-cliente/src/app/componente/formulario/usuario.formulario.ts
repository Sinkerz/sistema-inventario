import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import { UsuarioEntidad } from '../../entidad/usuario';
import { UsuarioAccesoEntidad } from '../../entidad/usuario.acceso';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';
import { TipoUsuarioBuscadorModal } from '../modal/tipo.usuario.buscador.modal';
import { TrabajadorBuscadorModal } from '../modal/trabajador.buscador.modal';
import { TrabajadorEntidad } from '../../entidad/trabajador';
import { TipoUsuarioEntidad } from '../../entidad/tipo.usuario';
import { SelectItem } from 'primeng/primeng';

@Component({
    selector: 'usuario-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/usuario.formulario.html'
})
export class UsuarioFormulario {

    private componentesInactivos: boolean;
    @Input() usuario: UsuarioEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild(TrabajadorBuscadorModal) trabajadorBuscadorModal;
    @ViewChild(TipoUsuarioBuscadorModal) tipoUsuarioBuscadorModal;
    @ViewChild("toFocus") inputFocus: ElementRef;

    usuarioAcceso: UsuarioAccesoEntidad;
    indiceTipoUsuarioSel: number;

    constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
        this.cancelarUsuarioAcceso();
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    guardar() {
        this.utilServicio.showLoader();
        this.apiServicio.guardar('usuario', this.usuario)
            .then((result) => {
                this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });
    }

    abrirSelectorTrabajador() {
        this.trabajadorBuscadorModal.mostrarModal();
    }

    seleccionarTrabajador(trabajador: TrabajadorEntidad) {
        this.usuario.trabajador = {
            id: trabajador.id,
            persona : {
                nombre : trabajador.persona.nombre
            }
        };
    } 

    abrirSelectorTipoUsuario() {
        this.tipoUsuarioBuscadorModal.mostrarModal();
    }

    seleccionarTipoUsuario(tipoUsuario: TipoUsuarioEntidad) {
        this.usuarioAcceso = {
            tipoUsuario: {
                id: tipoUsuario.id,
                nombre: tipoUsuario.nombre
            }
        };
    }

    agregarUsuarioAcceso() {
        if (this.validarUsuarioAcceso()) {
            this.usuario.usuarioAccesos.push((<any>Object).assign({}, this.usuarioAcceso));
            this.cancelarUsuarioAcceso();
        }
    }

    modificarUsuarioAcceso() {
        if (this.validarUsuarioAcceso()) {
            if (this.indiceTipoUsuarioSel < 0) {
                this.respuestaError.emit('Debe seleccionar el tipo de usuario.');
                return;
            }
            this.usuario.usuarioAccesos[this.indiceTipoUsuarioSel] =(<any>Object).assign({}, this.usuarioAcceso);
            this.cancelarUsuarioAcceso();
        }
    }

    quitarUsuarioAcceso() {
        if (this.indiceTipoUsuarioSel < 0) {
            this.respuestaError.emit('Debe seleccionar el tipo de usuario.');
            return false;
        }
        if (this.utilServicio.haSeleccionadoEntidad(this.usuario)) {
            this.usuario.usuarioAccesos[this.indiceTipoUsuarioSel].eliminar = true;
        } else {
            this.usuario.usuarioAccesos.splice(this.indiceTipoUsuarioSel, 1);
        }
        this.cancelarUsuarioAcceso();
    }

    seleccionarUsuarioAcceso(indice: number) {
        this.cancelarUsuarioAcceso();
        this.indiceTipoUsuarioSel = indice;
        this.usuarioAcceso = (<any>Object).assign({}, this.usuario.usuarioAccesos[indice]);
        this.seleccionarTipoUsuario(this.usuarioAcceso.tipoUsuario);
    }

    validarUsuarioAcceso() {
        if (!this.utilServicio.haSeleccionadoEntidad(this.usuarioAcceso.tipoUsuario)) {
            this.respuestaError.emit('Debe seleccionar el tipo de usuario.');
            return false;
        } else if (this.utilServicio.buscarEnArregloPorPropiedad(this.usuarioAcceso.tipoUsuario.id, this.usuario.usuarioAccesos, 'tipoUsuario.id', false, this.indiceTipoUsuarioSel) >= 0) {
            this.respuestaError.emit('Ya ha agregado el tipo de usuario.');
            return false;
        }
        return true;
    }
    
    cancelarUsuarioAcceso() {
        this.usuarioAcceso = new UsuarioAccesoEntidad();
        this.indiceTipoUsuarioSel = -1;
    }

}