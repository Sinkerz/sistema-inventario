import { Component, Input, Output, EventEmitter, ViewChild, Inject } from '@angular/core';
import { TrabajadorEntidad } from '../../entidad/trabajador';
import { TrabajadorSucursalEntidad } from '../../entidad/trabajador.sucursal';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';
import { PersonaBuscadorModal } from '../modal/persona.buscador.modal';
import { SucursalBuscadorModal } from '../modal/sucursal.buscador.modal';
import { PersonaEntidad } from '../../entidad/persona';
import { SucursalEntidad } from '../../entidad/sucursal';

@Component({
    selector: 'trabajador-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/trabajador.formulario.html'
})
export class TrabajadorFormulario {
    private componentesInactivos: boolean;
    @Input() trabajador: TrabajadorEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild(PersonaBuscadorModal) personaBuscadorModal;
    @ViewChild(SucursalBuscadorModal) sucursalBuscadorModal;

    es: any;
    trabajadorSucursal: TrabajadorSucursalEntidad;
    indiceSucursalSel: number;

constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
        this.es = this.utilServicio.getDateProperties();
        this.cancelarTrabajadorSucursal();
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
    }

    guardar() {
        this.utilServicio.showLoader();
        this.apiServicio.guardar('trabajador', this.trabajador)
            .then((result) => {
                this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });
    }

    abrirSelectorPersona() {
        this.personaBuscadorModal.mostrarModal();
    }

    seleccionarPersona(persona: PersonaEntidad) {
        this.trabajador.persona = {
            id: persona.id,
            nombre: persona.nombre
        };
    }

    abrirSelectorSucursal() {
        this.sucursalBuscadorModal.mostrarModal();
    }

    seleccionarSucursal(sucursal: SucursalEntidad) {
        this.trabajadorSucursal = {
            sucursal: {
                id: sucursal.id,
                nombre: sucursal.nombre
            }
        };
    }

    agregarTrabajadorSucursal() {
        if (this.validarTrabajadorSucursal()) {
            this.trabajador.trabajadorSucursales.push((<any>Object).assign({}, this.trabajadorSucursal));
            this.cancelarTrabajadorSucursal();
        }
    }

    modificarTrabajadorSucursal() {
        if (this.validarTrabajadorSucursal()) {
            if (this.indiceSucursalSel < 0) {
                this.respuestaError.emit('Debe seleccionar la sucursal.');
                return;
            }
            this.trabajador.trabajadorSucursales[this.indiceSucursalSel] = (<any>Object).assign({}, this.trabajadorSucursal);
            this.cancelarTrabajadorSucursal();
        }
    }

    quitarTrabajadorSucursal() {
        if (this.indiceSucursalSel < 0) {
            this.respuestaError.emit('Debe seleccionar la sucursal a quitar.');
            return false;
        }
        if (this.utilServicio.haSeleccionadoEntidad(this.trabajador)) {
            this.trabajador.trabajadorSucursales[this.indiceSucursalSel].eliminar = true;
        } else {
            this.trabajador.trabajadorSucursales.splice(this.indiceSucursalSel, 1);
        }
        this.cancelarTrabajadorSucursal();
    }

    seleccionarTrabajadorSucursal(indice: number) {
        this.cancelarTrabajadorSucursal();
        this.indiceSucursalSel = indice;
        this.trabajadorSucursal = (<any>Object).assign({}, this.trabajador.trabajadorSucursales[indice]);
        this.seleccionarSucursal(this.trabajadorSucursal.sucursal);
    }

    validarTrabajadorSucursal() {
        if (!this.utilServicio.haSeleccionadoEntidad(this.trabajadorSucursal.sucursal)) {
            this.respuestaError.emit('Debe seleccionar la sucursal.');
            return false;
        } else if (this.utilServicio.buscarEnArregloPorPropiedad(this.trabajadorSucursal.sucursal.id, this.trabajador.trabajadorSucursales, 'sucursal.id', true, this.indiceSucursalSel) >= 0) {
            this.respuestaError.emit('Ya ha agregado la sucursal.');
            return false;
        }
        return true;
    }

    cancelarTrabajadorSucursal() {
        this.trabajadorSucursal = new TrabajadorSucursalEntidad();
        this.indiceSucursalSel = -1;
    }

}