import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import { UnidadEntidad } from '../../entidad/unidad';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'unidad-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/unidad.formulario.html'
})
export class UnidadFormulario {
    private componentesInactivos: boolean;
    @Input() unidad: UnidadEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild("toFocus") inputFocus: ElementRef;

  constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    limpiarFormulario() {
        this.unidad = new UnidadEntidad();
    }

    guardar() {
        this.utilServicio.showLoader();
        this.apiServicio.guardar('unidad', this.unidad)
            .then((result) => {
                this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });

    }

}