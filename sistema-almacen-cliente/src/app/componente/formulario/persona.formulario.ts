import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import { PersonaEntidad } from '../../entidad/persona';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';
import { TipoDocumentoBuscadorModal } from '../modal/tipo.documento.buscador.modal';
import { TipoDocumentoEntidad } from '../../entidad/tipo.documento';
import { SelectItem } from 'primeng/primeng';

@Component({
    selector: 'persona-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/persona.formulario.html'
})
export class PersonaFormulario {
    private componentesInactivos: boolean;
    @Input() persona: PersonaEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild(TipoDocumentoBuscadorModal) tipoDocumentoBuscadorModal;
    private tiposPersona: SelectItem[];
    @ViewChild("toFocus") inputFocus: ElementRef;
    
   constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
        this.tiposPersona = [
            {
                label : "Natural",
                value : "N"
            },
             {
                label : "Jurídica",
                value : "J"
            }
        ];
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    guardar() {
        //this.utilServicio.showLoader();
        this.apiServicio.guardar('persona', this.persona)
            .then((result) => {
                //this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
               // this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });
    } 

    abrirSelectorTipoDocumento() {
        this.tipoDocumentoBuscadorModal.mostrarModal();
    }

    seleccionarTipoDocumento(tipoDocumento: TipoDocumentoEntidad) {
        this.persona.tipoDocumento = {
            id : tipoDocumento.id,
            nombre : tipoDocumento.nombre,
            abreviatura : tipoDocumento.abreviatura
        };
    }


}