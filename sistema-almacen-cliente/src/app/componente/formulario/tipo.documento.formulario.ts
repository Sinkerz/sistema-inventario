import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import { TipoDocumentoEntidad } from '../../entidad/tipo.documento';
import { RespuestaServicio } from '../../util/respuesta.servicio';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'tipo-documento-formulario',
    moduleId: module.id,
    templateUrl: '../../plantilla/formulario/tipo.documento.formulario.html'
})
export class TipoDocumentoFormulario {
    
    private componentesInactivos: boolean;
    @Input() tipoDocumento: TipoDocumentoEntidad;
    @Output() respuestaFormulario = new EventEmitter<RespuestaServicio>();
    @Output() respuestaError = new EventEmitter<string>();
    @ViewChild("toFocus") inputFocus: ElementRef;

    constructor(@Inject(ApiServicio) private apiServicio: ApiServicio,@Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.componentesInactivos = false;
    }

    activarFormulario(valor: boolean) {
        this.componentesInactivos = !valor;
        this.utilServicio.setFocus(this.inputFocus);
    }

    guardar() {
        this.utilServicio.showLoader();
        this.apiServicio.guardar('tipoDocumento', this.tipoDocumento)
            .then((result) => {
                this.utilServicio.hideLoader();
                this.respuestaFormulario.emit(result);
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.respuestaError.emit('Error al conectar al servidor');
            });
    } 
 
}