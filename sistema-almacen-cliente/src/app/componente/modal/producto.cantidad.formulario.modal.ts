import { Component, Output, EventEmitter, ViewChild, ElementRef, Inject } from '@angular/core';
import * as Decimal from 'decimal.js';
import { UtilServicio } from '../../servicio/util.servicio';

@Component({
    selector: 'producto-cantidad-formulario-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/producto.cantidad.formulario.modal.html'
})
export class ProductoCantidadFormularioModal {

    display: boolean = false;
    @Output() agregarCantidadProducto = new EventEmitter<decimal.Decimal>();
    cantidad: decimal.Decimal;
    disponibleDecimales: boolean;
    @ViewChild("toFocus") inputFocus: ElementRef;

    constructor( @Inject(UtilServicio) private utilServicio: UtilServicio) {
    }

    agregarProducto() {
        if (this.utilServicio.esNumero(this.cantidad) && new Decimal(this.cantidad).comparedTo(0) == 1) {
            this.agregarCantidadProducto.emit(new Decimal(this.cantidad));
            this.display = false;
        }
    }

    mostrarModal(disponibleDecimales) {
        this.disponibleDecimales = disponibleDecimales;
        this.cantidad = new Decimal(1);
        this.utilServicio.setFocus(this.inputFocus);
        this.utilServicio.selectInput(this.inputFocus);
        this.display = true;
    }
}