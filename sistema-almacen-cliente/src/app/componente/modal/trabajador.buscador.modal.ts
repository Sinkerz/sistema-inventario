import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { TrabajadorEntidad } from '../../entidad/trabajador';
import { TrabajadorBuscador } from '../buscador/trabajador.buscador';

@Component({
    selector: 'trabajador-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/trabajador.buscador.modal.html'
})
export class TrabajadorBuscadorModal {

    display: boolean = false;
    @Output() seleccionarTrabajador = new EventEmitter<TrabajadorEntidad>();
    @ViewChild(TrabajadorBuscador) trabajadorBuscador;

    constructor() {
    }

    cambioTrabajador(trabajador: TrabajadorEntidad) {
        this.seleccionarTrabajador.emit(trabajador);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.trabajadorBuscador.consultar();
    }
}