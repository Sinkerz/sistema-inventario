import { Component, Output, EventEmitter } from '@angular/core';
import { UnidadEntidad } from '../../entidad/unidad';

@Component({
    selector: 'unidad-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/unidad.buscador.modal.html'
})
export class UnidadBuscadorModal {

    display: boolean = false;
    @Output() seleccionarUnidad = new EventEmitter<UnidadEntidad>();

    constructor() {
    }

    cambioUnidad(unidad: UnidadEntidad) {
        this.seleccionarUnidad.emit(unidad);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
    }

}