import { Component, Output, EventEmitter } from '@angular/core';
import { MarcaEntidad } from '../../entidad/marca';

@Component({
    selector: 'marca-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/marca.buscador.modal.html'
})
export class MarcaBuscadorModal {

    display: boolean = false;
    @Output() seleccionarMarca = new EventEmitter<MarcaEntidad>();

    constructor() {
    }

    cambioMarca(marca: MarcaEntidad) {
        this.seleccionarMarca.emit(marca);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
    }

}