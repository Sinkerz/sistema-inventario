import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { TipoMotivoEntidad } from '../../entidad/tipo.motivo';
import { TipoMotivoBuscador } from '../buscador/tipo.motivo.buscador';

@Component({
    selector: 'tipo-motivo-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/tipo.motivo.buscador.modal.html'
})
export class TipoMotivoBuscadorModal {

    display: boolean = false;
    @Output() seleccionarTipoMotivo = new EventEmitter<TipoMotivoEntidad>();
    @ViewChild(TipoMotivoBuscador) tipoMotivoBuscador;

    constructor() {
    }

    cambioTipoMotivo(tipoMotivo: TipoMotivoEntidad) {
        this.seleccionarTipoMotivo.emit(tipoMotivo);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.tipoMotivoBuscador.consultar();
    }

}