import { Component,ViewChild} from '@angular/core';
import { PdfVisualizador } from '../otro/visualizador.pdf';

@Component({
    selector: 'pdf-visualizador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/visualizador.pdf.modal.html'
})
export class PdfVisualizadorModal{

    display: boolean = false;

    @ViewChild(PdfVisualizador) pdfVisualizador;

    constructor() {
    
    }
    
    verReporte(parametros:any) {
        this.pdfVisualizador.visualizarReporte("reporte",parametros);
        this.display = true;
    }

    onHide(){
        this.pdfVisualizador.reset();
    }
}