import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ProductoEntidad } from '../../entidad/producto';
import { ProductoBuscador } from '../buscador/producto.buscador';

@Component({
    selector: 'producto-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/producto.buscador.modal.html'
})
export class ProductoBuscadorModal {

    display: boolean = false;
    @Output() seleccionarProducto = new EventEmitter<ProductoEntidad>();
    @ViewChild(ProductoBuscador) productoBuscador;

    constructor() {
    }

    cambioProducto(producto: ProductoEntidad) {
        this.seleccionarProducto.emit(producto);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.productoBuscador.consultar();
    }

}