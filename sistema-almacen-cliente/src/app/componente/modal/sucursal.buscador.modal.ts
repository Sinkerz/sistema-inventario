import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { SucursalEntidad } from '../../entidad/sucursal';
import { SucursalBuscador } from '../buscador/sucursal.buscador';

@Component({
    selector: 'sucursal-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/sucursal.buscador.modal.html'
})
export class SucursalBuscadorModal {

    display: boolean = false;
    @Output() seleccionarSucursal = new EventEmitter<SucursalEntidad>();
    @ViewChild(SucursalBuscador) sucursalBuscador;

    constructor() {
    }

    cambioSucursal(sucursal: SucursalEntidad) {
        this.seleccionarSucursal.emit(sucursal);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.sucursalBuscador.consultar();
    }

}