import { Component, Output, EventEmitter, ViewChild, Inject } from '@angular/core';
import { ProductoDTOEntidad } from '../../entidad/dto/producto.dto';
import { ProductoBuscador } from '../buscador/producto.buscador';
import { ProductoCantidadFormularioModal } from './producto.cantidad.formulario.modal';
import { UtilServicio } from '../../servicio/util.servicio';
import { MessageDialog } from '../otro/message.dialog';

import * as Decimal from 'decimal.js';

@Component({
    selector: 'producto-cantidad-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/producto.cantidad.buscador.modal.html'
})
export class ProductoCantidadBuscadorModal {

    display: boolean = false;
    @Output() seleccionarProducto = new EventEmitter<ProductoDTOEntidad>();
    @ViewChild(ProductoBuscador) productoBuscador;
    @ViewChild(ProductoCantidadFormularioModal) productoCantidadFormularioModal;
    @ViewChild(MessageDialog) messageDialog;
    productoSeleccionado: ProductoDTOEntidad;
    constructor( @Inject(UtilServicio) private utilServicio: UtilServicio) {
    }

    cambioProducto(producto: ProductoDTOEntidad) {
        this.productoSeleccionado = producto;
        this.productoCantidadFormularioModal.mostrarModal(true);
        //this.display = false;
    }

    agregarCantidad(cantidad: decimal.Decimal) {
        this.utilServicio.showLoader();
        this.productoSeleccionado.cantidad = cantidad;
        this.seleccionarProducto.emit(this.productoSeleccionado);
        this.utilServicio.hideLoader();
    }

    mostrarModal() {
        this.productoBuscador.consultar();
        this.display = true;
    }

}