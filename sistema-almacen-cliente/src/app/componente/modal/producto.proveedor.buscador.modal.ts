import { Component, Output, EventEmitter, ViewChild, Inject } from '@angular/core';
import { ProductoDTOEntidad } from '../../entidad/dto/producto.dto';
import { ProductoProveedorBuscador } from '../buscador/producto.proveedor.buscador';
import { ProductoCantidadFormularioModal } from './producto.cantidad.formulario.modal';
import { UtilServicio } from '../../servicio/util.servicio';
import { MessageDialog } from '../otro/message.dialog';

import * as Decimal from 'decimal.js';

@Component({
    selector: 'producto-proveedor-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/producto.proveedor.buscador.modal.html'
})
export class ProductoProveedorBuscadorModal {

    display: boolean = false;
    @Output() seleccionarProducto = new EventEmitter<ProductoDTOEntidad>();
    @ViewChild(ProductoProveedorBuscador) productoProveedorBuscador;
    @ViewChild(ProductoCantidadFormularioModal) productoCantidadFormularioModal;
    @ViewChild(MessageDialog) messageDialog;
    productoSeleccionado: ProductoDTOEntidad;
    sucursal: number;
    constructor(@Inject(UtilServicio) private utilServicio: UtilServicio) {
    }

    cambioProducto(producto: ProductoDTOEntidad) {
        this.productoSeleccionado = producto;
        this.productoCantidadFormularioModal.mostrarModal(true);
        //this.display = false;
    }

    agregarCantidad(cantidad: decimal.Decimal) {
        this.utilServicio.showLoader();
        this.productoSeleccionado.cantidad = cantidad;
        this.seleccionarProducto.emit(this.productoSeleccionado);
        this.utilServicio.hideLoader();
    }

    mostrarModal(proveedor: number) {
        this.productoProveedorBuscador.setProveedor(proveedor);
        this.productoProveedorBuscador.consultar();
        this.display = true;
    } 

    openModal() {
        this.display = true;
    }

}