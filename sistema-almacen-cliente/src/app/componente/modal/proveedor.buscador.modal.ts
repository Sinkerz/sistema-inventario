import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ProveedorEntidad } from '../../entidad/proveedor';
import { ProveedorBuscador } from '../buscador/proveedor.buscador';

@Component({
    selector: 'proveedor-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/proveedor.buscador.modal.html'
})
export class ProveedorBuscadorModal {

    display: boolean = false;
    @Output() seleccionarProveedor = new EventEmitter<ProveedorEntidad>();
    @ViewChild(ProveedorBuscador) proveedorBuscador;

    constructor() {
        
    }

    cambioProveedor(proveedor: ProveedorEntidad) {
        this.seleccionarProveedor.emit(proveedor);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.proveedorBuscador.consultar();
    }

}