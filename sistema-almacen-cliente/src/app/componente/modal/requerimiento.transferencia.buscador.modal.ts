import { Component, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { DocumentoDTOEntidad } from '../../entidad/dto/documento.dto';
import { RequerimientoTransferenciaBuscador } from '../buscador/requerimiento.transferencia.buscador';

@Component({
    selector: 'requerimiento-transferencia-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/requerimiento.transferencia.buscador.modal.html'
})
export class RequerimientoTransferenciaBuscadorModal {

    display: boolean = false;
    @Output() seleccionarRequerimientoTransferencia = new EventEmitter<DocumentoDTOEntidad>();
    @ViewChild(RequerimientoTransferenciaBuscador) requerimientoTransferenciaBuscador;

    constructor() {
    }

    cambioRequerimientoTransferencia(documento: DocumentoDTOEntidad) {
        this.seleccionarRequerimientoTransferencia.emit(documento);
        this.display = false;
    }

    mostrarModal(sucursalDestinoId: number = null) {
        this.requerimientoTransferenciaBuscador.setSucursalDestinoId(sucursalDestinoId);
        this.display = true;
        this.requerimientoTransferenciaBuscador.consultar();
    }

}