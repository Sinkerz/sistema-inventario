import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { TipoUsuarioEntidad } from '../../entidad/tipo.usuario';
import { TipoUsuarioBuscador } from '../buscador/tipo.usuario.buscador';

@Component({
    selector: 'tipo-usuario-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/tipo.usuario.buscador.modal.html'
})
export class TipoUsuarioBuscadorModal {

    display: boolean = false;
    @Output() seleccionarTipoUsuario = new EventEmitter<TipoUsuarioEntidad>();
    @ViewChild(TipoUsuarioBuscador) tipoUsuarioBuscador;

    constructor() {
    }

    cambioTipoUsuario(tipoUsuario: TipoUsuarioEntidad) {
        this.seleccionarTipoUsuario.emit(tipoUsuario);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.tipoUsuarioBuscador.consultar();
    }

}