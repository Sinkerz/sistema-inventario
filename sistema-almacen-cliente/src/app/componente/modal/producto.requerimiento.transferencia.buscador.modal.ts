import { Component, Output, EventEmitter, ViewChild, Inject } from '@angular/core';
import { ProductoDTOEntidad } from '../../entidad/dto/producto.dto';
import { ProductoRequerimientoTransferenciaBuscador } from '../buscador/producto.requerimiento.transferencia.buscador';
import { ProductoCantidadFormularioModal } from './producto.cantidad.formulario.modal';
import { ApiServicio } from '../../servicio/api.servicio';
import { UtilServicio } from '../../servicio/util.servicio';
import { MessageDialog } from '../otro/message.dialog';

import * as Decimal from 'decimal.js';

@Component({
    selector: 'producto-requerimiento-transferencia-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/producto.requerimiento.transferencia.buscador.modal.html'
})
export class ProductoRequerimientoTransferenciaBuscadorModal {

    display: boolean = false;
    @Output() seleccionarProducto = new EventEmitter<ProductoDTOEntidad>();
    @ViewChild(ProductoRequerimientoTransferenciaBuscador) productoRequerimientoTransferenciaBuscador;
    @ViewChild(ProductoCantidadFormularioModal) productoCantidadFormularioModal;
    @ViewChild(MessageDialog) messageDialog;
    productoSeleccionado: ProductoDTOEntidad;
    sucursalOrigen: number;
    sucursalDestino: number;
    constructor( @Inject(ApiServicio) private apiServicio: ApiServicio, @Inject(UtilServicio) private utilServicio: UtilServicio) {
    }

    cambioProducto(producto: ProductoDTOEntidad) {
        this.productoSeleccionado = producto;
        this.productoCantidadFormularioModal.mostrarModal(producto.disponibleDecimales);
        //this.display = false;
    }

    agregarCantidad(cantidad: decimal.Decimal) {
        this.utilServicio.showLoader();
        this.apiServicio.obtener('producto/stock/' + this.productoSeleccionado.id + '/sucursal', this.sucursalOrigen)
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    if (result.data >= cantidad) {
                        this.productoSeleccionado.cantidad = cantidad;
                        this.seleccionarProducto.emit(this.productoSeleccionado);
                    } else {
                        this.messageDialog.showDialog("El producto no tiene stock suficiente");
                    }
                } else {
                    this.messageDialog.showDialog(result.mensaje);
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
                this.messageDialog.showDialog('Error al conectar con el servidor');
            });

    }

    mostrarModal(sucursalOrig: number, sucursalDest: number) {
        this.sucursalOrigen = sucursalOrig;
        this.sucursalDestino = sucursalDest;
        this.productoRequerimientoTransferenciaBuscador.consultarConSucursal(this.sucursalOrigen, this.sucursalDestino);
    }

    openModal() {
        this.display = true;
    }

}