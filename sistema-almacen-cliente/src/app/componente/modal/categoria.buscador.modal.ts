import { Component, Output, EventEmitter } from '@angular/core';
import { CategoriaEntidad } from '../../entidad/categoria';

@Component({
    selector: 'categoria-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/categoria.buscador.modal.html'
})
export class CategoriaBuscadorModal {

    display: boolean = false;
    @Output() seleccionarCategoria = new EventEmitter<CategoriaEntidad>();

    constructor() {
    }

    cambioCategoria(categoria: CategoriaEntidad) {
        this.seleccionarCategoria.emit(categoria);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
    }

}