import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { DocumentoDTOEntidad } from '../../entidad/dto/documento.dto';
import { OrdenCompraBuscador } from '../buscador/orden.compra.buscador';

@Component({
    selector: 'orden-compra-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/orden.compra.buscador.modal.html'
})
export class OrdenCompraBuscadorModal {

    display: boolean = false;
    @Output() seleccionarOrdenCompra = new EventEmitter<DocumentoDTOEntidad>();
    @ViewChild(OrdenCompraBuscador) ordenCompraBuscador;

    constructor() {
    }

    cambioOrdenCompra(documento: DocumentoDTOEntidad) {
        this.seleccionarOrdenCompra.emit(documento);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.ordenCompraBuscador.consultar();
    }

}