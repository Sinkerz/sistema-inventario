import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ClienteEntidad } from '../../entidad/cliente';
import { ClienteBuscador } from '../buscador/cliente.buscador';

@Component({
    selector: 'cliente-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/cliente.buscador.modal.html'
})
export class ClienteBuscadorModal {

    display: boolean = false;
    @Output() seleccionarCliente = new EventEmitter<ClienteEntidad>();
    @ViewChild(ClienteBuscador) clienteBuscador;

    constructor() {
    }

    cambioCliente(cliente: ClienteEntidad) {
        this.seleccionarCliente.emit(cliente);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.clienteBuscador.consultar();
    }

}