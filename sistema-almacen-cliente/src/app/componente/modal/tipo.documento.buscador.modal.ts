import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { TipoDocumentoEntidad } from '../../entidad/tipo.documento';
import { TipoDocumentoBuscador } from '../buscador/tipo.documento.buscador';

@Component({
    selector: 'tipo-documento-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/tipo.documento.buscador.modal.html'
})
export class TipoDocumentoBuscadorModal {

    display: boolean = false;
    @Output() seleccionarTipoDocumento = new EventEmitter<TipoDocumentoEntidad>();
    @ViewChild(TipoDocumentoBuscador) tipoDocumentoBuscador;

    constructor() {
    }

    cambioTipoDocumento(tipoDocumento: TipoDocumentoEntidad) {
        this.seleccionarTipoDocumento.emit(tipoDocumento);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.tipoDocumentoBuscador.consultar();
    }

}