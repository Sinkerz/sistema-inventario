import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { PersonaEntidad } from '../../entidad/persona';
import { PersonaBuscador } from '../buscador/persona.buscador';

@Component({
    selector: 'persona-buscador-modal',
    moduleId: module.id,
    templateUrl: '../../plantilla/modal/persona.buscador.modal.html'
})
export class PersonaBuscadorModal {

    display: boolean = false;
    @Output() seleccionarPersona = new EventEmitter<PersonaEntidad>();
    @ViewChild(PersonaBuscador) personaBuscador;

    constructor() {
    }

    cambioPersona(persona: PersonaEntidad) {
        this.seleccionarPersona.emit(persona);
        this.display = false;
    }

    mostrarModal() {
        this.display = true;
        this.personaBuscador.consultar();
    }

}