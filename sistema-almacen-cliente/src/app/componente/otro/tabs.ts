import { Component, ViewChild, ComponentFactoryResolver, ViewContainerRef, ChangeDetectorRef, Inject } from '@angular/core';

import { Tab } from './tab';
import { DynamicTab } from '../../directiva/dynamic.tab';

@Component({
  selector: 'tabs',
  template: `
      <p-tabView id="main-tab-container" [hidden]="tabs==0" (onClose)="closeTab($event)" (onChange)="changeTab($event)">
            <p-tabPanel class="main-tab" header="{{tab.title}}" [selected]="tab.active" [closable]="true" *ngFor="let tab of tabs">
            </p-tabPanel>
        </p-tabView>
    <ng-content></ng-content> 
    <ng-template dynamic-tab #container></ng-template>
  `
})
export class Tabs {
  tabs: Tab[] = [];
  openTabs: string[] = [];

  @ViewChild(DynamicTab)
  dynamicTabPlaceholder: DynamicTab;

  constructor( @Inject(ComponentFactoryResolver) private _componentFactoryResolver: ComponentFactoryResolver, @Inject(ChangeDetectorRef) private cdRef: ChangeDetectorRef) { }

  ngAfterViewChecked() { this.cdRef.detectChanges(); }

  openTab(title: string, template, url: string) {
    let indexTabOpen;
    if (this.getUrlOpenTab(url) != url) {
      let componentFactory = this._componentFactoryResolver.resolveComponentFactory(Tab);
      let viewContainerRef = this.dynamicTabPlaceholder.viewContainer;
      let componentRef = viewContainerRef.createComponent(componentFactory);

      let instance: Tab = componentRef.instance as Tab;
      instance.title = title;
      instance.template = template;

      this.tabs.push(componentRef.instance as Tab);

      this.openTabs.push(url);

      indexTabOpen = this.tabs.length - 1;
    } else {
      indexTabOpen = this.openTabs.indexOf(url);
    }

    this.selectTab(this.tabs[indexTabOpen]);
  }

  changeTab(event) {
    this.selectTab(this.tabs[event.index]);
  }

  selectTab(tab: Tab) {
    if (tab) {
      this.tabs.forEach(tab => tab.active = false);
      tab.active = true;
    }
  }

  getUrlOpenTab(url) {
    return this.openTabs.find(u => u == url);
  }

  closeTab(event) {
    this.tabs.splice(event.index, 1);
    this.openTabs.splice(event.index, 1);
    let viewContainerRef = this.dynamicTabPlaceholder.viewContainer;
    viewContainerRef.remove(event.index);
    this.selectTab(this.tabs[0]);
  }

}