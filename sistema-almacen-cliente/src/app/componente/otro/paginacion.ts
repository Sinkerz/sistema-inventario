import { Component, Input, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { BusquedaPaginada } from '../../util/busqueda.paginada';

@Component({
    selector: 'paginacion',
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    templateUrl: '../../plantilla/otro/paginacion.html',
    styleUrls: ['../../recurso/css/componente/paginacion.css']
})
export class Paginacion {

    @Input() paginaActual: number;
    @Input() totalPaginas: number;
    @Output() cambioPaginacion = new EventEmitter<number>();


    private cambioPagina() {
        if (this.paginaActual > this.totalPaginas) {
            this.paginaActual = this.totalPaginas;
        } else if (this.paginaActual <= 1) {
            this.paginaActual = 1;
        }
        this.cambioPaginacion.emit(this.paginaActual);
    }

    primeraPagina() {
        if (this.paginaActual > 1) {
            this.cambioPaginacion.emit(1);
        }
    }

    anteriorPagina() {
        let page = this.paginaActual;
        page--;
        if (!(page < 1)) {
            this.cambioPaginacion.emit(page);
        }
    }

    siguientePagina() {
        let page = this.paginaActual;
        page++;
        if (!(page > this.totalPaginas)) {
            this.cambioPaginacion.emit(page);
        }
    }

    ultimaPagina() {
        if (this.paginaActual != this.totalPaginas) {
            this.cambioPaginacion.emit(this.totalPaginas);
        }
    }


}