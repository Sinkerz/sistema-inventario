import { Component, OnInit, Output, EventEmitter, ViewEncapsulation,Inject } from '@angular/core';
import { MenuItem } from 'primeng/primeng';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';

@Component({
    selector: 'menu',
    moduleId: module.id,
    encapsulation: ViewEncapsulation.None,
    templateUrl: '../../plantilla/otro/menu.html',
    styleUrls: ['../../recurso/css/componente/menu.css']
})
export class Menu implements OnInit {

    private itemsMenu: MenuItem[];
    private itemsButton: MenuItem[];
    private itemsMenuMovil: MenuItem[];
    private openMenuMovil: boolean = false;
    usuarioSucursal : string;

    @Output() onSelectMenuItem = new EventEmitter<any>();
    @Output() onCloseSession = new EventEmitter();

    constructor(@Inject(UtilServicio) private utilServicio: UtilServicio,@Inject(ApiServicio) private apiServicio: ApiServicio) {

    };

    ngOnInit() {
        let usuario = this.utilServicio.obtenerUsuarioLogeado();
        this.usuarioSucursal = usuario.login + " | " + usuario.sucursal;
        if (this.utilServicio.haSeleccionadoEntidad(usuario)) {
            this.obtenerMenu(usuario.id);
        } else {
            this.closeSession();
        }

        this.itemsButton = [
            {
                label: 'Salir del sistema', icon: 'fa-sign-out', command: () => {
                    this.closeSession();
                }
            },
        ];
    }

    private obtenerMenu(idTipoUsuario: number) {
        this.utilServicio.showLoader();
        this.apiServicio.obtener('tipoUsuarioMenuOpcion/menu', idTipoUsuario)
            .then((result) => {
                this.utilServicio.hideLoader();
                if (this.utilServicio.dataDeServerEsCorrecta(result)) {
                    this.itemsMenu = result.data;
                    this.formatMenu(this.itemsMenu);
                    this.itemsMenuMovil = (<any>Object).assign([], result.data);
                    this.itemsMenuMovil.push({
                        label: 'Salir del sistema', icon: 'fa-sign-out', command: () => {
                            this.closeSession();
                        }
                    }, );
                } else {
                }
            }).catch((error) => {
                this.utilServicio.hideLoader();
            });
    }

    private formatMenu(items) {
        items && items.forEach((element, index) => {
            if (!this.utilServicio.esNullOUndefinedOVacio(element.badge)) {
                element.command = (event) => { this.openTab(element.badge, element.label) }
            }
            this.formatMenu(element.items);
        });
    }

    openTab(url, titulo) {
        this.onSelectMenuItem.emit({ url: url, titulo: titulo });
        this.openMenuMovil = false;
    }

    closeSession() {
        this.onCloseSession.emit();
    }
}
