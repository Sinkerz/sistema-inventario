import { Component, Input } from '@angular/core';

@Component({
  selector: 'tab',
  template: `
    <div class="mb-50 mt-5" [hidden]="!active" >
      <ng-content></ng-content>
      <ng-container *ngIf="template"
        [ngTemplateOutlet]="template"
      >
      </ng-container>
    </div>
  `
})
export class Tab {
  @Input('tabTitle') title: string;
  @Input() active = true;
  @Input() template;
}