import { Component, Input, ViewChild,Inject } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';

@Component({
  selector: 'pdf-visualizador',
  moduleId: module.id,
  templateUrl: '../../plantilla/otro/visualizador.pdf.html',
})
export class PdfVisualizador {

  id: string;
  pdfUrl: any;
  parametrosRecibidos: any;

  constructor(@Inject(DomSanitizer) private domSanitizer: DomSanitizer,@Inject(UtilServicio) private utilServicio: UtilServicio,@Inject(ApiServicio) private apiServicio: ApiServicio) {
    this.pdfUrl = this.domSanitizer.bypassSecurityTrustResourceUrl("./lib/pdf.js/loader.html");
  }

  reset() {
    this.pdfUrl = this.domSanitizer.bypassSecurityTrustResourceUrl("./lib/pdf.js/loader.html");
  }

  visualizarReporte(nombre: string, parametros: any) {
    this.pdfUrl = this.domSanitizer.bypassSecurityTrustResourceUrl("./lib/pdf.js/loader.html");
    this.parametrosRecibidos = parametros;
    this.id = 'visualizador-' + parametros.nombreReporte + "-" + (new Date().getTime())
    this.apiServicio.reporte(nombre, parametros)
      .then((result) => {
        if (result) {
          let file = new Blob([result], { type: 'application/pdf' });
          let url = URL.createObjectURL(file);
          this.pdfUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('./lib/pdf.js/web/viewer.html?file=' + url);
        } else {
          this.pdfUrl = this.domSanitizer.bypassSecurityTrustResourceUrl("./lib/pdf.js/error.html");
        }
      }).catch((error) => {
        this.pdfUrl = this.domSanitizer.bypassSecurityTrustResourceUrl("./lib/pdf.js/error.html");
      });
  }
}