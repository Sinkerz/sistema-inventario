import { ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';
import { Component,Inject } from "@angular/core";

@Component({
    selector: 'confirm-dialog',
    moduleId: module.id,
    template: '<p-confirmDialog #cd><p-footer>'
    + '<button type="button" pButton icon="fa-close" label="No" (click)="cd.reject()"></button>'
    + '<button type="button" pButton icon="fa-check" label="Sí" (click)="cd.accept()"></button>'
    + '</p-footer>'
    + '</p-confirmDialog>'
})
export class ConfirmDialog {

    constructor(@Inject(ConfirmationService) private confirmationService: ConfirmationService) { }

    confirm(mensaje: string, titulo: string = 'Sistema de almacén', icono: string = 'fa fa-question-circle'): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.confirmationService.confirm({
                message: mensaje,
                header: titulo,
                icon: icono,
                accept: () => {
                    resolve(true);
                },
                reject: () => {
                    resolve(false);
                }
            });
        });
    }
}
