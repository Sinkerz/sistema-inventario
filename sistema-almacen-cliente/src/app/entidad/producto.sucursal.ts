import { SucursalEntidad } from "./sucursal";
import { ProductoEntidad } from "./producto";

import * as Decimal from 'decimal.js';

export class ProductoSucursalEntidad {

    id: number;
    stockMinimo: decimal.Decimal;
    sucursal: SucursalEntidad;
    producto: ProductoEntidad;

    constructor() {
        this.sucursal = new SucursalEntidad();
        this.producto = new ProductoEntidad();
    }
}