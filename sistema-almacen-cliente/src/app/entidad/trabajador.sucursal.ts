import { TrabajadorEntidad } from "./trabajador";
import { SucursalEntidad } from "./sucursal";

export class TrabajadorSucursalEntidad {

    id?: number;
    trabajador?: TrabajadorEntidad;
    sucursal: SucursalEntidad;
    eliminar?: boolean;
    constructor() {
        this.sucursal = new SucursalEntidad();
    }
}