import { SucursalEntidad } from "./sucursal";
import { PersonaEntidad } from "./persona";

import { ClienteEntidad } from "./cliente";
import { TipoComprobanteEntidad } from "./tipo.comprobante";
import { TipoMonedaEntidad } from "./tipo.moneda";
import { DocumentoDetalleEntidad } from "./documento.detalle";
import { MotivoEntidad } from './motivo'

import * as Decimal from 'decimal.js';

export class DocumentoCabeceraEntidad {

    id?: number;
    sucursal?: SucursalEntidad;
    sucursalDestino?: SucursalEntidad;
    persona?: PersonaEntidad;

    placa?: string;
    fechaEmision?: Date;
    numero?: number;
    serie?: string;
    precioTotal?: decimal.Decimal;
    valorDescuento?: decimal.Decimal;
    valorTotal?: decimal.Decimal;
    tipoComprobante?: TipoComprobanteEntidad;
    documentoDetalles?: DocumentoDetalleEntidad[];
    clientePuntos?: ClienteEntidad;
    valorInafecto?: decimal.Decimal;
    valorVenta?: decimal.Decimal;
    documentoRef?: DocumentoCabeceraEntidad;
    tipoMoneda?: TipoMonedaEntidad;
    fechaVencimiento?: Date;
    fechaIngreso?: Date;
    motivo?: MotivoEntidad;
    constructor() {
        this.documentoDetalles = [];
        this.valorTotal = new Decimal(0);
        this.precioTotal = new Decimal(0);
        this.valorVenta = new Decimal(0);
    }
}