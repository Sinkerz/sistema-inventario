import { ProductoEntidad } from "./producto";
import { PersonaEntidad } from "./persona";


import * as Decimal from 'decimal.js';

export class DocumentoDetalleEntidad {

    id?: number;
    cantidad: decimal.Decimal;
    costoUnitario: decimal.Decimal;
    productoServicio: ProductoEntidad;
    valorDscto: decimal.Decimal;
    valorTotal: decimal.Decimal;
    valorInafecto?: decimal.Decimal;
    valorVenta?: decimal.Decimal;
    constructor() {
        this.productoServicio = new ProductoEntidad();
        this.valorTotal = new Decimal(0);
        this.valorInafecto = new Decimal(0);
        this.valorDscto = new Decimal(0);
        this.valorVenta = new Decimal(0);
    }
}