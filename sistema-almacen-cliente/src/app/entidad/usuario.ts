import { TrabajadorEntidad } from "./trabajador";
import { UsuarioAccesoEntidad } from "./usuario.acceso";

export class UsuarioEntidad {

    id: number;
    sucursalId ?: number;
    login?: string;
    clave?: string;
    trabajador?: TrabajadorEntidad;
    usuarioAccesos?: UsuarioAccesoEntidad[];
    
    constructor() {
        this.usuarioAccesos = [];
        this.trabajador = new TrabajadorEntidad();
    }
}