import { TipoDocumentoEntidad } from "./tipo.documento";

export class PersonaEntidad {

    id?: number;
    nombre: string;
    apellidos?: string;
    direccion?: string;
    telefono?: string;
    tipoPersona?: string;
    email?: string;
    tipoDocumento?: TipoDocumentoEntidad;
    numeroDocumento?: string;
    constructor() {
        this.tipoDocumento = new TipoDocumentoEntidad();
        this.tipoPersona = "N";
    }
}