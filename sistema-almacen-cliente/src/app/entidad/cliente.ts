import { PersonaEntidad } from "./persona";

export class ClienteEntidad {

    id?: number;
    web?: string;
    persona: PersonaEntidad;
    constructor() {
        this.persona = new PersonaEntidad();
    }
}