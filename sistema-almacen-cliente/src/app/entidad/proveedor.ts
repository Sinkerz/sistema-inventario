import { PersonaEntidad } from "./persona";

export class ProveedorEntidad {

    id?: number;
    web?: string;
    persona: PersonaEntidad;
    
    constructor() {
        this.persona = new PersonaEntidad();
    }
}