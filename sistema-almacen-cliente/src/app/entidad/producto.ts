import { UnidadEntidad } from "./unidad";
import { CategoriaEntidad } from "./categoria";
import { MarcaEntidad } from "./marca";

export class ProductoEntidad {

    id: number;
    nombre?: String;
    codigo?: string;
    disponibleParaVenta?: boolean;
    disponibleEnDecimales?: boolean;
    tipo?: string;
    afectoIgv?: boolean;
    unidad?: UnidadEntidad;
    categoria?: CategoriaEntidad;
    marca?: MarcaEntidad;
    constructor() {
        this.tipo = "P";
        this.disponibleParaVenta = true;
        this.afectoIgv = true;
        this.unidad = new UnidadEntidad();
        this.categoria = new CategoriaEntidad();
        this.marca = new MarcaEntidad();
    }
}