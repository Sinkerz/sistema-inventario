import { PersonaEntidad } from "./persona";
import { TrabajadorSucursalEntidad } from "./trabajador.sucursal";

export class TrabajadorEntidad {

    id: number;
    fechaNacimiento?: Date;
    fechaIngreso?: Date;
    fechaSalida?: Date;
    persona: PersonaEntidad;
    trabajadorSucursales? : TrabajadorSucursalEntidad[];
    constructor() {
        this.persona = new PersonaEntidad();
        this.trabajadorSucursales =[];
    }
}