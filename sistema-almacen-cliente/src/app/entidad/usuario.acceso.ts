import { TipoUsuarioEntidad } from "./tipo.usuario";
import { UsuarioEntidad } from "./usuario";

export class UsuarioAccesoEntidad {

    id?: number;
    usuario?: UsuarioEntidad;
    tipoUsuario: TipoUsuarioEntidad;
    eliminar?: boolean;
    constructor() {
        this.tipoUsuario = new TipoUsuarioEntidad();
    }
}