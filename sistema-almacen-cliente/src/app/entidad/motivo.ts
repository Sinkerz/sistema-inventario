import { TipoMotivoEntidad } from "./tipo.motivo";

export class MotivoEntidad {

    id?: number;
    nombre?: String;
    tipoMotivo?: TipoMotivoEntidad;
    codigoSunat?: string;
    constructor() {
        
    }
}