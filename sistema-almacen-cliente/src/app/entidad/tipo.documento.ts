export class TipoDocumentoEntidad {

    id: number;
    nombre: string;
    abreviatura?: string;
    codigoSunat?: string;
    constructor() {

    }
}