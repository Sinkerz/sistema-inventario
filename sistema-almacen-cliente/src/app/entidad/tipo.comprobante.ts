export class TipoComprobanteEntidad {

    id: number;
    nombre: string;
    codigoSunat: string;
    abreviatura?: string;
    esComprobante?: boolean;
    constructor() {

    }
}