import { ProductoDTOEntidad } from "./producto.dto"
import * as Decimal from 'decimal.js';

export class DocumentoDTOEntidad {
    id?: number;
    persona?: string;
    precio?: decimal.Decimal;
    documento?: string;
    productos?: ProductoDTOEntidad[];
    sucursalOrigenId?: number;
    sucursalDestinoId?: number;
    constructor() {

    }
}