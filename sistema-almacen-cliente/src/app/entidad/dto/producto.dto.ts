import * as Decimal from 'decimal.js';

export class ProductoDTOEntidad {

    id?: number;
    nombre: String;
    codigo?: string;
    marca?: string;
    precioNormal?: decimal.Decimal;
    precioMinimo?: decimal.Decimal;
    cantidad?: decimal.Decimal;
    descuento?: decimal.Decimal;
    disponibleDecimales?: boolean;
    stock?: decimal.Decimal;
    afectoIgv?: boolean;
    tipo?: string;
    constructor() {
    }
}