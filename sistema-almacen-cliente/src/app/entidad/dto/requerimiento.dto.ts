import { ProductoDTOEntidad } from "./producto.dto"

export class RequerimientoDTOEntidad {
    id?: number;
    productos?: ProductoDTOEntidad[];
    sucursalOrigenId?: number;
    sucursalDestinoId?: number;
    constructor() {

    }
}