export class BusquedaPaginada<E> {

    totalRegistros?: number;
    totalPaginas?: number;
    cantidadPorPagina: number;
    paginaActual: number;
    buscar: any;
    registros?: E[];

    constructor() {
        this.totalRegistros = 1;
        this.totalPaginas = 1;
        this.cantidadPorPagina = 10;
        this.paginaActual = 1;
        this.buscar = {};
        this.registros = [];
    }
}