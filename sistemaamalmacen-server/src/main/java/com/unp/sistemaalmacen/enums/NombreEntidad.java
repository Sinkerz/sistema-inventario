package com.unp.sistemaalmacen.enums;

/**
 *
 * @author MARCOS BAYONA
 */
public enum NombreEntidad {
    
    CATEGORIA("Categoría"),
    INVENTARIO("Inventario"),
    MARCA("Marca"),
    PERSONA("Persona"),
    PRODUCTO("Producto"),
    PRODUCTO_SUCURSAL("Producto - Sucursal"),
    PROVEEDOR("Proveedor"),
    SUCURSAL("Sucursal"),
    TIPO_DOCUMENTO("Tipo de documento"),
    TIPO_MOTIVO("Tipo de motivo"),
    TRABAJADOR("Trabajador"),
    TRABAJADOR_SUCURSAL("Trabajador - Sucursal"),
    TRANSFERENCIA("Transferencia"),
    TIPO_USUARIO("Tipo de usuario"),
    TIPO_USUARIO_MENU_OPCION("Tipo usuario menú opción"),
    UNIDAD("Unidad"),
    USUARIO("Usuario");
    
    private String valor;

    private NombreEntidad(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
