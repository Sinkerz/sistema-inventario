package com.unp.sistemaalmacen.interceptor;

import com.unp.sistemaalmacen.entidad.AuditoriaEntidad;
import com.unp.sistemaalmacen.util.SistemaUtil;
import com.unp.sistemaalmacen.util.UsuarioSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 *
 * @author MARCOS BAYONA
 */
@Aspect
@Component
public class AuditarInterceptor {

    private Logger logger = LogManager.getLogger(getClass());

    private final static String METODO_CREAR = "crear";
    private final static String METODO_ACTUALIZAR = "actualizar";
    private final static String METODO_GRABAR_TODOS = "grabarTodos";

    @Before("execution(* com.unp.sistemaalmacen.repositorio.impl.*.*(..))")
    public void ejecutarAntesDeMetodo(JoinPoint joinPoint) {
        logger.info("ejecutarAntesDeMetodo inicio " + joinPoint.getSignature().getName());
        if (validarInvocacion(joinPoint)) {
            UsuarioSession usuarioSession = SistemaUtil.obtenerUsuarioSession();

            Object parametro = joinPoint.getArgs()[0];
            Date date = new Date();
            Timestamp fechaHora = new Timestamp(date.getTime());
            if (METODO_CREAR.equals(joinPoint.getSignature().getName())
                    || METODO_ACTUALIZAR.equals(joinPoint.getSignature().getName())) {
                AuditoriaEntidad auditoriaBean = ((AuditoriaEntidad) parametro);
                if (auditoriaBean.esNuevo()) {
                    auditoriaBean.setNick("ADMIN"/*usuarioSession.getLogin()*/);
                    auditoriaBean.setFecha(fechaHora);
                    auditoriaBean.setIp("127.0.0.1"/*usuarioSession.getIp()*/);
                    auditoriaBean.setPc("localhost"/*usuarioSession.getPc()*/);
                }
                auditoriaBean.setFechaMod(fechaHora);
                auditoriaBean.setNickMod("ADMIN"/*usuarioSession.getLogin()*/);
                auditoriaBean.setIpMod("127.0.0.1"/*usuarioSession.getIp()*/);
                auditoriaBean.setPcMod("localhost"/*usuarioSession.getPc()*/);
                if (SistemaUtil.esNulo(auditoriaBean.getEstado())) {
                    auditoriaBean.setEstado(Boolean.TRUE);
                }
            }
            if (METODO_GRABAR_TODOS.equals(joinPoint.getSignature().getName())) {
                List auditoriaBeans = (List) parametro;
                for (Object obj : auditoriaBeans) {
                    AuditoriaEntidad auditoriaBean;
                    if (obj instanceof AuditoriaEntidad) {
                        auditoriaBean = (AuditoriaEntidad) obj;
                    } else {
                        continue;
                    }
                    if (auditoriaBean.esNuevo()) {
                        auditoriaBean.setNick(usuarioSession.getLogin());
                        auditoriaBean.setFecha(fechaHora);
                        auditoriaBean.setIp(usuarioSession.getIp());
                        auditoriaBean.setPc(usuarioSession.getPc());
                        auditoriaBean.setEstado(Boolean.TRUE);
                    }
                    auditoriaBean.setFechaMod(fechaHora);
                    auditoriaBean.setNickMod(usuarioSession.getLogin());
                    auditoriaBean.setIpMod(usuarioSession.getIp());
                    auditoriaBean.setPcMod(usuarioSession.getPc());
                    if (SistemaUtil.esNulo(auditoriaBean.getEstado())) {
                        auditoriaBean.setEstado(Boolean.TRUE);
                    }
                }
            }
        }
        logger.info("ejecutarAntesDeMetodo fin " + joinPoint.getSignature().getName());
    }

    private boolean validarInvocacion(JoinPoint joinPoint) {
        if (joinPoint.getArgs().length == 1) {
            Object parametro = joinPoint.getArgs()[0];
            return (parametro instanceof AuditoriaEntidad || parametro instanceof List<?>)
                    && METODO_CREAR.equals(joinPoint.getSignature().getName())
                    || METODO_ACTUALIZAR.equals(joinPoint.getSignature().getName())
                    || METODO_GRABAR_TODOS.equals(joinPoint.getSignature().getName());
        }
        return false;
    }

}
