package com.unp.sistemaalmacen.dto;

/**
 *
 * @author MARCOS BAYONA
 */
public class SucursalDTO {

    private Long id;
    private String nombre;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
