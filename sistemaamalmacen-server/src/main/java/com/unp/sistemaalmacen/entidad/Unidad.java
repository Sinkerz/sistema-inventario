package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.List;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "unidad")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class Unidad extends AuditoriaEntidad {

    @Column(nullable = false, length = 10)
    private String abreviatura;

    @Column(nullable = false, length = 50)
    private String nombre;

    @JsonIgnoreProperties("productoList")
    @OneToMany(mappedBy = "unidad")
    private List<Producto> productoList;

    public Unidad() {
    }

    public String getAbreviatura() {
        return this.abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Producto> getProductoList() {
        return this.productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }

    public Producto addProducto(Producto producto) {
        getProductoList().add(producto);
        producto.setUnidad(this);

        return producto;
    }

    public Producto removeProducto(Producto producto) {
        getProductoList().remove(producto);
        producto.setUnidad(null);

        return producto;
    }

}
