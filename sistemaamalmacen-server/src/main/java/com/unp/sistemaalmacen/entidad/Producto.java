package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.List;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "producto")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class Producto extends AuditoriaEntidad {

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "nombre")
    private String nombre;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_categoria", nullable = false)
    private Categoria categoria;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_marca", nullable = false)
    private Marca marca;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_unidad", nullable = false)
    private Unidad unidad;

    @JsonIgnoreProperties("inventarioList")
    @OneToMany(mappedBy = "producto")
    private List<Inventario> inventarioList;

    @JsonIgnoreProperties("productoSucursalList")
    @OneToMany(mappedBy = "producto")
    private List<ProductoSucursal> productoSucursalList;

    @JsonIgnoreProperties("transferenciaList")
    @OneToMany(mappedBy = "producto")
    private List<Transferencia> transferenciaList;

    public Producto() {
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Inventario> getInventarioList() {
        return this.inventarioList;
    }

    public void setInventarioList(List<Inventario> inventarioList) {
        this.inventarioList = inventarioList;
    }

    public Inventario addInventario(Inventario inventario) {
        getInventarioList().add(inventario);
        inventario.setProducto(this);

        return inventario;
    }

    public Inventario removeInventario(Inventario inventario) {
        getInventarioList().remove(inventario);
        inventario.setProducto(null);

        return inventario;
    }

    public Categoria getCategoria() {
        return this.categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Marca getMarca() {
        return this.marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Unidad getUnidad() {
        return this.unidad;
    }

    public void setUnidad(Unidad unidad) {
        this.unidad = unidad;
    }

    public List<ProductoSucursal> getProductoSucursalList() {
        return this.productoSucursalList;
    }

    public void setProductoSucursalList(List<ProductoSucursal> productoSucursalList) {
        this.productoSucursalList = productoSucursalList;
    }

    public ProductoSucursal addProductoSucursal(ProductoSucursal productoSucursal) {
        getProductoSucursalList().add(productoSucursal);
        productoSucursal.setProducto(this);

        return productoSucursal;
    }

    public ProductoSucursal removeProductoSucursal(ProductoSucursal productoSucursal) {
        getProductoSucursalList().remove(productoSucursal);
        productoSucursal.setProducto(null);

        return productoSucursal;
    }

    public List<Transferencia> getTransferenciaList() {
        return this.transferenciaList;
    }

    public void setTransferenciaList(List<Transferencia> transferenciaList) {
        this.transferenciaList = transferenciaList;
    }

    public Transferencia addTransferencia(Transferencia transferencia) {
        getTransferenciaList().add(transferencia);
        transferencia.setProducto(this);

        return transferencia;
    }

    public Transferencia removeTransferencia(Transferencia transferencia) {
        getTransferenciaList().remove(transferencia);
        transferencia.setProducto(null);

        return transferencia;
    }

}
