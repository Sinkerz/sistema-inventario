package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.List;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "tipo_documento")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class TipoDocumento extends AuditoriaEntidad {

    @Column(length = 6)
    private String abreviatura;

    @Column(nullable = false, length = 50)
    private String nombre;

    @JsonIgnoreProperties("personaList")
    @OneToMany(mappedBy = "tipoDocumento")
    private List<Persona> personaList;

    public TipoDocumento() {
    }

    public String getAbreviatura() {
        return this.abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Persona> getPersonaList() {
        return this.personaList;
    }

    public void setPersonaList(List<Persona> personaList) {
        this.personaList = personaList;
    }

    public Persona addPersona(Persona persona) {
        getPersonaList().add(persona);
        persona.setTipoDocumento(this);

        return persona;
    }

    public Persona removePersona(Persona persona) {
        getPersonaList().remove(persona);
        persona.setTipoDocumento(null);

        return persona;
    }

}
