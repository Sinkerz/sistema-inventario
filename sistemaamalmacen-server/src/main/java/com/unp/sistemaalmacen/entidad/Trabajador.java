package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "trabajador")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class Trabajador extends AuditoriaEntidad {

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_ingreso")
    private Date fechaIngreso;

    @Column(name = "fecha_mod", nullable = false)
    private Timestamp fechaMod;

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_nacimiento", nullable = false)
    private Date fechaNacimiento;

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_salida")
    private Date fechaSalida;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_persona", nullable = false)
    private Persona persona;

    @JsonIgnoreProperties("trabajadorSucursalList")
    @OneToMany(mappedBy = "trabajador")
    private List<TrabajadorSucursal> trabajadorSucursalList;

    public Trabajador() {
    }

    public Date getFechaIngreso() {
        return this.fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Date getFechaNacimiento() {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Date getFechaSalida() {
        return this.fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Persona getPersona() {
        return this.persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<TrabajadorSucursal> getTrabajadorSucursalList() {
        return this.trabajadorSucursalList;
    }

    public void setTrabajadorSucursalList(List<TrabajadorSucursal> trabajadorSucursalList) {
        this.trabajadorSucursalList = trabajadorSucursalList;
    }

    public TrabajadorSucursal addTrabajadorSucursal(TrabajadorSucursal trabajadorSucursal) {
        getTrabajadorSucursalList().add(trabajadorSucursal);
        trabajadorSucursal.setTrabajador(this);

        return trabajadorSucursal;
    }

    public TrabajadorSucursal removeTrabajadorSucursal(TrabajadorSucursal trabajadorSucursal) {
        getTrabajadorSucursalList().remove(trabajadorSucursal);
        trabajadorSucursal.setTrabajador(null);

        return trabajadorSucursal;
    }

}
