package com.unp.sistemaalmacen.entidad;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "transferencia")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class Transferencia extends AuditoriaEntidad {

    @Column(nullable = false, precision = 9, scale = 2)
    private BigDecimal cantidad;

    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_transferencia", nullable = false)
    private Date fechaTransferencia;

    @Column(name = "id_sucursal_recibe", nullable = false)
    private Integer idSucursalRecibe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_producto", nullable = false)
    private Producto producto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sucursal", nullable = false)
    private Sucursal sucursal;

    public Transferencia() {
    }

    public BigDecimal getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechaTransferencia() {
        return this.fechaTransferencia;
    }

    public void setFechaTransferencia(Date fechaTransferencia) {
        this.fechaTransferencia = fechaTransferencia;
    }

    public Integer getIdSucursalRecibe() {
        return this.idSucursalRecibe;
    }

    public void setIdSucursalRecibe(Integer idSucursalRecibe) {
        this.idSucursalRecibe = idSucursalRecibe;
    }

    public Producto getProducto() {
        return this.producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Sucursal getSucursal() {
        return this.sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

}
