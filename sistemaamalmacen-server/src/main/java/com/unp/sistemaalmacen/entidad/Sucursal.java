package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.List;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "sucursal")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class Sucursal extends AuditoriaEntidad {

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "serie")
    private String serie;

    @Column(name = "telefono")
    private String telefono;

    @JsonIgnoreProperties("inventarioList")
    @OneToMany(mappedBy = "sucursal")
    private List<Inventario> inventarioList;

    @JsonIgnoreProperties("productoSucursalList")
    @OneToMany(mappedBy = "sucursal")
    private List<ProductoSucursal> productoSucursalList;

    @JsonIgnoreProperties("trabajadorSucursalList")
    @OneToMany(mappedBy = "sucursal")
    private List<TrabajadorSucursal> trabajadorSucursalList;

    @JsonIgnoreProperties("transferenciaList")
    @OneToMany(mappedBy = "sucursal")
    private List<Transferencia> transferenciaList;

    public Sucursal() {
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSerie() {
        return this.serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<Inventario> getInventarioList() {
        return this.inventarioList;
    }

    public void setInventarioList(List<Inventario> inventarioList) {
        this.inventarioList = inventarioList;
    }

    public Inventario addInventario(Inventario inventario) {
        getInventarioList().add(inventario);
        inventario.setSucursal(this);

        return inventario;
    }

    public Inventario removeInventario(Inventario inventario) {
        getInventarioList().remove(inventario);
        inventario.setSucursal(null);

        return inventario;
    }

    public List<ProductoSucursal> getProductoSucursalList() {
        return this.productoSucursalList;
    }

    public void setProductoSucursalList(List<ProductoSucursal> productoSucursalList) {
        this.productoSucursalList = productoSucursalList;
    }

    public ProductoSucursal addProductoSucursal(ProductoSucursal productoSucursal) {
        getProductoSucursalList().add(productoSucursal);
        productoSucursal.setSucursal(this);

        return productoSucursal;
    }

    public ProductoSucursal removeProductoSucursal(ProductoSucursal productoSucursal) {
        getProductoSucursalList().remove(productoSucursal);
        productoSucursal.setSucursal(null);

        return productoSucursal;
    }

    public List<TrabajadorSucursal> getTrabajadorSucursalList() {
        return this.trabajadorSucursalList;
    }

    public void setTrabajadorSucursalList(List<TrabajadorSucursal> trabajadorSucursalList) {
        this.trabajadorSucursalList = trabajadorSucursalList;
    }

    public TrabajadorSucursal addTrabajadorSucursal(TrabajadorSucursal trabajadorSucursal) {
        getTrabajadorSucursalList().add(trabajadorSucursal);
        trabajadorSucursal.setSucursal(this);

        return trabajadorSucursal;
    }

    public TrabajadorSucursal removeTrabajadorSucursal(TrabajadorSucursal trabajadorSucursal) {
        getTrabajadorSucursalList().remove(trabajadorSucursal);
        trabajadorSucursal.setSucursal(null);

        return trabajadorSucursal;
    }

    public List<Transferencia> getTransferenciaList() {
        return this.transferenciaList;
    }

    public void setTransferenciaList(List<Transferencia> transferenciaList) {
        this.transferenciaList = transferenciaList;
    }

    public Transferencia addTransferencia(Transferencia transferencia) {
        getTransferenciaList().add(transferencia);
        transferencia.setSucursal(this);

        return transferencia;
    }

    public Transferencia removeTransferencia(Transferencia transferencia) {
        getTransferenciaList().remove(transferencia);
        transferencia.setSucursal(null);

        return transferencia;
    }

}
