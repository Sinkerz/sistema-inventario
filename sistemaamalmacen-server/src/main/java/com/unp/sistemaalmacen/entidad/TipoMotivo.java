package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.List;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "tipo_motivo")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class TipoMotivo extends AuditoriaEntidad {

    @Column(nullable = false, length = 50)
    private String nombre;

    @JsonIgnoreProperties("inventarioList")
    @OneToMany(mappedBy = "tipoMotivo")
    private List<Inventario> inventarioList;

    public TipoMotivo() {
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Inventario> getInventarioList() {
        return this.inventarioList;
    }

    public void setInventarioList(List<Inventario> inventarioList) {
        this.inventarioList = inventarioList;
    }

    public Inventario addInventario(Inventario inventario) {
        getInventarioList().add(inventario);
        inventario.setTipoMotivo(this);

        return inventario;
    }

    public Inventario removeInventario(Inventario inventario) {
        getInventarioList().remove(inventario);
        inventario.setTipoMotivo(null);

        return inventario;
    }

}
