package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.List;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "categoria")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class Categoria extends AuditoriaEntidad {

    @Column(name = "nombre")
    private String nombre;

    @JsonIgnoreProperties("productoList")
    @OneToMany(mappedBy = "categoria")
    private List<Producto> productoList;

    public Categoria() {

    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Producto> getProductoList() {
        return this.productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }

}
