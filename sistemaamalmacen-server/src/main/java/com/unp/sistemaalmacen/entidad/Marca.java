package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.util.List;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "marca")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class Marca extends AuditoriaEntidad {

    @Column(name = "nombre")
    private String nombre;

    @JsonIgnoreProperties("productoList")
    @OneToMany(mappedBy = "marca")
    private List<Producto> productoList;

    public Marca() {
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Producto> getProductoList() {
        return this.productoList;
    }

    public void setProductoList(List<Producto> productoList) {
        this.productoList = productoList;
    }

    public Producto addProducto(Producto producto) {
        getProductoList().add(producto);
        producto.setMarca(this);

        return producto;
    }

    public Producto removeProducto(Producto producto) {
        getProductoList().remove(producto);
        producto.setMarca(null);

        return producto;
    }

}
