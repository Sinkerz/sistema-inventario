package com.unp.sistemaalmacen.entidad;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

/**
 *
 * @author MARCOS BAYONA
 */
@Entity
@Table(name = "usuario")
@DynamicUpdate(value = true)
@DynamicInsert(value = true)
@SelectBeforeUpdate
public class Usuario extends AuditoriaEntidad {

    @JsonProperty
    @Column(name = "clave", nullable = false)
    private String clave;

    @Column(name = "login")
    private String login;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idtrabajador", nullable = false)
    private Trabajador trabajador;

    @JsonIgnoreProperties("usuario")
    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
    private List<UsuarioAcceso> usuarioAccesos;

    @Transient
    private Long sucursalId;

    public Usuario() {
    }

    @JsonIgnore
    public String getClave() {
        return this.clave;
    }

    @JsonProperty
    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<UsuarioAcceso> getUsuarioAccesos() {
        return this.usuarioAccesos;
    }

    public void setUsuarioAccesos(List<UsuarioAcceso> usuarioAccesos) {
        this.usuarioAccesos = usuarioAccesos;
    }

    public UsuarioAcceso addUsuarioAcceso(UsuarioAcceso usuarioAcceso) {
        getUsuarioAccesos().add(usuarioAcceso);
        usuarioAcceso.setUsuario(this);
        return usuarioAcceso;
    }

    public UsuarioAcceso removeUsuarioAcceso(UsuarioAcceso usuarioAcceso) {
        getUsuarioAccesos().remove(usuarioAcceso);
        usuarioAcceso.setUsuario(null);
        return usuarioAcceso;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    public Long getSucursalId() {
        return sucursalId;
    }

    public void setSucursalId(Long sucursalId) {
        this.sucursalId = sucursalId;
    }

}
