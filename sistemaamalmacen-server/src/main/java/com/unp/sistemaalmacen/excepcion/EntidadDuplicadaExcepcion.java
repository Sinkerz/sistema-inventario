package com.unp.sistemaalmacen.excepcion;

/**
 *
 * @author MARCOS BAYONA
 */
public class EntidadDuplicadaExcepcion extends Exception {

    public EntidadDuplicadaExcepcion(String mensaje) {
        super(mensaje);
    }
}
