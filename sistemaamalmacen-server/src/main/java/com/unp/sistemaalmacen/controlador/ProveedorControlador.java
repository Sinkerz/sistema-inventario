package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Proveedor;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.ProveedorServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/proveedor")
public class ProveedorControlador extends BaseControladorImpl<Proveedor, Long> implements BaseControlador<Proveedor, Long> {

    @Autowired
    public ProveedorControlador(ProveedorServicio proveedorServicio) {
        super(proveedorServicio, NombreEntidad.PROVEEDOR.getValor());
    }

}
