package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Trabajador;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.TrabajadorServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/trabajador")
public class TrabajadorControlador extends BaseControladorImpl<Trabajador, Long> implements BaseControlador<Trabajador, Long> {

    @Autowired
    public TrabajadorControlador(TrabajadorServicio trabajadorServicio) {
        super(trabajadorServicio, NombreEntidad.TRABAJADOR.getValor());
    }

}
