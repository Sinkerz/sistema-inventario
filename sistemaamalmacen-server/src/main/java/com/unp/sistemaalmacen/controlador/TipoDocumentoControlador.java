package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.TipoDocumento;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.TipoDocumentoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/tipoDocumento")
public class TipoDocumentoControlador extends BaseControladorImpl<TipoDocumento, Long> implements BaseControlador<TipoDocumento, Long> {

    @Autowired
    public TipoDocumentoControlador(TipoDocumentoServicio tipoDocumentoServicio) {
        super(tipoDocumentoServicio, NombreEntidad.TIPO_DOCUMENTO.getValor());
    }

}
