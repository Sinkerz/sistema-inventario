package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.ProductoSucursal;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.ProductoSucursalServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/productoSucursal")
public class ProductoSucursalControlador extends BaseControladorImpl<ProductoSucursal, Long> implements BaseControlador<ProductoSucursal, Long> {

    @Autowired
    public ProductoSucursalControlador(ProductoSucursalServicio productoSucursalServicio) {
        super(productoSucursalServicio, NombreEntidad.PRODUCTO_SUCURSAL.getValor());
    }

}
