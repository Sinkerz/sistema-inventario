package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Unidad;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.UnidadServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/unidad")
public class UnidadControlador extends BaseControladorImpl<Unidad, Long> implements BaseControlador<Unidad, Long> {

    @Autowired
    public UnidadControlador(UnidadServicio unidadServicio) {
        super(unidadServicio, NombreEntidad.UNIDAD.getValor());
    }

}
