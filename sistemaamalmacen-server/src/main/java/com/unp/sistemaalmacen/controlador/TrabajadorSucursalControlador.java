package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.TrabajadorSucursal;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.TrabajadorSucursalServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/trabajadorSucursal")
public class TrabajadorSucursalControlador extends BaseControladorImpl<TrabajadorSucursal, Long> implements BaseControlador<TrabajadorSucursal, Long> {

    @Autowired
    public TrabajadorSucursalControlador(TrabajadorSucursalServicio trabajadorSucursalServicio) {
        super(trabajadorSucursalServicio, NombreEntidad.TRABAJADOR_SUCURSAL.getValor());
    }

}
