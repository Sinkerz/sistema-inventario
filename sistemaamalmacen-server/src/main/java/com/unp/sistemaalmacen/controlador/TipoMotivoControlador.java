package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.TipoMotivo;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.TipoMotivoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/tipoMotivo")
public class TipoMotivoControlador extends BaseControladorImpl<TipoMotivo, Long> implements BaseControlador<TipoMotivo, Long> {

    @Autowired
    public TipoMotivoControlador(TipoMotivoServicio tipoMotivoServicio) {
        super(tipoMotivoServicio, NombreEntidad.TIPO_MOTIVO.getValor());
    }

}
