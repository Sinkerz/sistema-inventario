package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Producto;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/producto")
public class ProductoControlador extends BaseControladorImpl<Producto, Long> implements BaseControlador<Producto, Long> {

    @Autowired
    public ProductoControlador(ProductoServicio productoServicio) {
        super(productoServicio, NombreEntidad.PRODUCTO.getValor());
    }

}
