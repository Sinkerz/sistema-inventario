package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.util.RespuestaControlador;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author MARCOS BAYONA
 */
public interface BaseControlador<Entidad, TipoLlave> {

    public ResponseEntity<RespuestaControlador> crear(Entidad entidad);

    public ResponseEntity<RespuestaControlador> obtener(TipoLlave id);

    public ResponseEntity<RespuestaControlador> actualizar(Entidad entidad);

    public ResponseEntity<RespuestaControlador> eliminar(TipoLlave entidadId);

    public ResponseEntity<RespuestaControlador> obtenerTodos();

}
