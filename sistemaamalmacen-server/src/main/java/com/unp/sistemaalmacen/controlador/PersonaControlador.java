package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Persona;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.PersonaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/persona")
public class PersonaControlador extends BaseControladorImpl<Persona, Long> implements BaseControlador<Persona, Long> {

    @Autowired
    public PersonaControlador(PersonaServicio personaServicio) {
        super(personaServicio, NombreEntidad.PERSONA.getValor());
    }

}
