package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.TipoUsuarioMenuOpcion;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.TipoUsuarioMenuOpcionServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/tipoUsuarioMenuOpcion")
public class TipoUsuarioMenuOpcionControlador extends BaseControladorImpl<TipoUsuarioMenuOpcion, Long> implements BaseControlador<TipoUsuarioMenuOpcion, Long> {

    @Autowired
    public TipoUsuarioMenuOpcionControlador(TipoUsuarioMenuOpcionServicio tipoUsuarioMenuOpcionServicio) {
        super(tipoUsuarioMenuOpcionServicio, NombreEntidad.TIPO_USUARIO_MENU_OPCION.getValor());
    }

}
