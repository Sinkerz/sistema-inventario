/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Marca;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.MarcaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/marca")
public class MarcaControlador extends BaseControladorImpl<Marca, Long> implements BaseControlador<Marca, Long> {

    @Autowired
    public MarcaControlador(MarcaServicio marcaServicio) {
        super(marcaServicio, NombreEntidad.MARCA.getValor());
    }

}
