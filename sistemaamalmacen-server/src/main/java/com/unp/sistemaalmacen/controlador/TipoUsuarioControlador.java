package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.TipoUsuario;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.TipoUsuarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/tipoUsuario")
public class TipoUsuarioControlador extends BaseControladorImpl<TipoUsuario, Long> implements BaseControlador<TipoUsuario, Long> {

    @Autowired
    public TipoUsuarioControlador(TipoUsuarioServicio tipoUsuarioServicio) {
        super(tipoUsuarioServicio, NombreEntidad.TIPO_USUARIO.getValor());
    }

}
