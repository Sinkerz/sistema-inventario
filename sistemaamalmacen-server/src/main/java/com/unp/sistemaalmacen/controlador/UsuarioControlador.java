package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Usuario;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.UsuarioServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/usuario")
public class UsuarioControlador extends BaseControladorImpl<Usuario, Long> implements BaseControlador<Usuario, Long> {

    private final UsuarioServicio usuarioServicio;

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    public UsuarioControlador(UsuarioServicio usuarioServicio) {
        super(usuarioServicio, NombreEntidad.USUARIO.getValor());
        this.usuarioServicio = usuarioServicio;
    }

    @PostMapping("logeo")
    public ResponseEntity<RespuestaControlador> logeo(@RequestBody Usuario usuario, HttpServletRequest request) {
        RespuestaControlador respuestaControlador;
        try {
            respuestaControlador = usuarioServicio.validarCredenciales(usuario, request);
        } catch (Exception exception) {
            logger.error(exception, exception);
            respuestaControlador = respuestaControladorServicio.obtenerRespuestaDeErrorObtener(nombreEntidad);
        }
        return new ResponseEntity<>(respuestaControlador, HttpStatus.OK);
    }
    
    @GetMapping("sucursal/{login}")
    public ResponseEntity<RespuestaControlador> obtenerSucursalesUsuario(@PathVariable("login") String login) {
        RespuestaControlador respuestaControlador;
        try {
            respuestaControlador = RespuestaControlador.obtenerRespuestaExitoConData(usuarioServicio.obtenerSucursalesUsuario(login));
        } catch (Exception exception) {
            logger.error(exception, exception);
            respuestaControlador = respuestaControladorServicio.obtenerRespuestaDeErrorObtener(nombreEntidad);
        }
        return new ResponseEntity<>(respuestaControlador, HttpStatus.OK);
    }

}
