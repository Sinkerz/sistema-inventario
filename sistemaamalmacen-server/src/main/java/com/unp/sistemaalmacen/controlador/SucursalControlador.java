package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Sucursal;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.SucursalServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/sucursal")
public class SucursalControlador extends BaseControladorImpl<Sucursal, Long> implements BaseControlador<Sucursal, Long> {

    @Autowired
    public SucursalControlador(SucursalServicio sucursalServicio) {
        super(sucursalServicio, NombreEntidad.SUCURSAL.getValor());
    }

}
