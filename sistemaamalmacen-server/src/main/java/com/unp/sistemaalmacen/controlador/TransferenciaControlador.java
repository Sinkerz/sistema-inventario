package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Transferencia;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.TransferenciaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/transferencia")
public class TransferenciaControlador extends BaseControladorImpl<Transferencia, Long> implements BaseControlador<Transferencia, Long> {

    @Autowired
    public TransferenciaControlador(TransferenciaServicio transferenciaServicio) {
        super(transferenciaServicio, NombreEntidad.TRANSFERENCIA.getValor());
    }

}
