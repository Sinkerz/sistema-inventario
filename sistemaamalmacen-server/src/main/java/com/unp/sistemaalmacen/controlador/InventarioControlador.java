package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Inventario;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.InventarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/inventario")
public class InventarioControlador extends BaseControladorImpl<Inventario, Long> implements BaseControlador<Inventario, Long> {

    @Autowired
    public InventarioControlador(InventarioServicio inventarioServicio) {
        super(inventarioServicio, NombreEntidad.INVENTARIO.getValor());
    }

}
