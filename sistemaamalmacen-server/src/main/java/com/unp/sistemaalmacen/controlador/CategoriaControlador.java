package com.unp.sistemaalmacen.controlador;

import com.unp.sistemaalmacen.entidad.Categoria;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.servicio.CategoriaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MARCOS BAYONA
 */
@RestController
@RequestMapping("/server/categoria")
public class CategoriaControlador extends BaseControladorImpl<Categoria, Long> implements BaseControlador<Categoria, Long> {

    @Autowired
    public CategoriaControlador(CategoriaServicio categoriaServicio) {
        super(categoriaServicio, NombreEntidad.CATEGORIA.getValor());
    }

}
