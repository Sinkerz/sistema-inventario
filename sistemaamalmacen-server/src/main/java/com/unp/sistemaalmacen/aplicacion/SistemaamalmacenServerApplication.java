package com.unp.sistemaalmacen.aplicacion;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.unp.sistemaalmacen"})
public class SistemaamalmacenServerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SistemaamalmacenServerApplication.class, args);
        System.out.println("                                                      ");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("+ Curso       :   Aplicaciones de metodologías       +");
        System.out.println("+                 para el desarrollo de software     +");
        System.out.println("+                                                    +");
        System.out.println("+ Sistema     :   Almacén                            +");
        System.out.println("+                                                    +");
        System.out.println("+ Integrantes :                                      +");
        System.out.println("+               * Bayona Rijalba, Marcos Mariano     +");
        System.out.println("+               * Mauricio Arévalo, Elí              +");
        System.out.println("+               * Merino Montero, Sianko             +");
        System.out.println("+               * Mijahuanga Jibaja, Jhon            +");
        System.out.println("+               * Rodríguez Rodríguez, Jean          +");
        System.out.println("+                                                    +");
        System.out.println("+ Estado      :   Sistema iniciado correctamente     +");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("                                                      ");
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SistemaamalmacenServerApplication.class);
    }
}
