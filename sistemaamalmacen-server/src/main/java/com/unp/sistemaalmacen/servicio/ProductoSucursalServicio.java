package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.ProductoSucursal;

/**
 *
 * @author MARCOS BAYONA
 */
public interface ProductoSucursalServicio extends BaseServicio<ProductoSucursal, Long> {

}
