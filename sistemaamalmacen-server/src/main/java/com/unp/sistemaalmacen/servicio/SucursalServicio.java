package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Sucursal;

/**
 *
 * @author MARCOS BAYONA
 */
public interface SucursalServicio extends BaseServicio<Sucursal, Long> {

}
