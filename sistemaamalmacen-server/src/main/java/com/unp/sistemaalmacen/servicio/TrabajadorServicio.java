package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Trabajador;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TrabajadorServicio extends BaseServicio<Trabajador, Long> {

}
