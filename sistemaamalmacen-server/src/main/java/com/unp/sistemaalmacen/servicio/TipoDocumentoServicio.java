package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.TipoDocumento;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TipoDocumentoServicio extends BaseServicio<TipoDocumento, Long> {

}
