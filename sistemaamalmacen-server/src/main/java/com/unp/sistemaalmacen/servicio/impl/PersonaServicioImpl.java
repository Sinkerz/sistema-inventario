package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Persona;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.PersonaRepositorio;
import com.unp.sistemaalmacen.servicio.PersonaServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class PersonaServicioImpl extends BaseServicioImpl<Persona, Long> implements PersonaServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private PersonaRepositorio personaRepositorio;

    @Autowired
    public PersonaServicioImpl(PersonaRepositorio personaRepositorio) {
        super(personaRepositorio);
    }

    @Override
    public RespuestaControlador crear(Persona persona) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(persona);
        this.personaRepositorio.crear(persona);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.PERSONA.getValor(), persona.getId());
    }

    @Override
    public RespuestaControlador actualizar(Persona persona) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(persona);
        this.personaRepositorio.actualizar(persona);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.PERSONA.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long personaId) {
        RespuestaControlador respuesta;
        Persona persona;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.PERSONA.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            persona = personaRepositorio.obtener(personaId);
            persona.setEstado(Boolean.FALSE);
            personaRepositorio.actualizar(persona);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.PERSONA.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Persona persona) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
