package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Transferencia;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.TransferenciaRepositorio;
import com.unp.sistemaalmacen.servicio.TransferenciaServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class TransferenciaServicioImpl extends BaseServicioImpl<Transferencia, Long> implements TransferenciaServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TransferenciaRepositorio transferenciaRepositorio;

    @Autowired
    public TransferenciaServicioImpl(TransferenciaRepositorio transferenciaRepositorio) {
        super(transferenciaRepositorio);
    }

    @Override
    public RespuestaControlador crear(Transferencia transferencia) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(transferencia);
        this.transferenciaRepositorio.crear(transferencia);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TRANSFERENCIA.getValor(), transferencia.getId());
    }

    @Override
    public RespuestaControlador actualizar(Transferencia transferencia) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(transferencia);
        this.transferenciaRepositorio.actualizar(transferencia);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TRANSFERENCIA.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long transferenciaId) {
        RespuestaControlador respuesta;
        Transferencia transferencia;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TRANSFERENCIA.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            transferencia = transferenciaRepositorio.obtener(transferenciaId);
            transferencia.setEstado(Boolean.FALSE);
            transferenciaRepositorio.actualizar(transferencia);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TRANSFERENCIA.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Transferencia transferencia) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
