package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Inventario;

/**
 *
 * @author MARCOS BAYONA
 */
public interface InventarioServicio extends BaseServicio<Inventario, Long> {

}
