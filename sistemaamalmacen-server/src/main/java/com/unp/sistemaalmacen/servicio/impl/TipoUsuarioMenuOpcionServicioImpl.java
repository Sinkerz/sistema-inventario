package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.TipoUsuarioMenuOpcion;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.TipoUsuarioMenuOpcionRepositorio;
import com.unp.sistemaalmacen.repositorio.TipoUsuarioRepositorio;
import com.unp.sistemaalmacen.servicio.TipoUsuarioMenuOpcionServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class TipoUsuarioMenuOpcionServicioImpl extends BaseServicioImpl<TipoUsuarioMenuOpcion, Long> implements TipoUsuarioMenuOpcionServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TipoUsuarioMenuOpcionRepositorio tipoUsuarioMenuOpcionRepositorio;

    @Autowired
    public TipoUsuarioMenuOpcionServicioImpl(TipoUsuarioMenuOpcionRepositorio tipoUsuarioMenuOpcionRepositorio) {
        super(tipoUsuarioMenuOpcionRepositorio);
    }

    @Override
    public RespuestaControlador crear(TipoUsuarioMenuOpcion tipoUsuarioMenuOpcion) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoUsuarioMenuOpcion);
        this.tipoUsuarioMenuOpcionRepositorio.crear(tipoUsuarioMenuOpcion);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TIPO_USUARIO_MENU_OPCION.getValor(), tipoUsuarioMenuOpcion.getId());
    }

    @Override
    public RespuestaControlador actualizar(TipoUsuarioMenuOpcion tipoUsuarioMenuOpcion) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoUsuarioMenuOpcion);
        this.tipoUsuarioMenuOpcionRepositorio.actualizar(tipoUsuarioMenuOpcion);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TIPO_USUARIO_MENU_OPCION.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long tipoUsuarioMenuOpcionId) {
        RespuestaControlador respuesta;
        TipoUsuarioMenuOpcion tipoUsuarioMenuOpcion;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TIPO_USUARIO_MENU_OPCION.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            tipoUsuarioMenuOpcion = tipoUsuarioMenuOpcionRepositorio.obtener(tipoUsuarioMenuOpcionId);
            tipoUsuarioMenuOpcion.setEstado(Boolean.FALSE);
            tipoUsuarioMenuOpcionRepositorio.actualizar(tipoUsuarioMenuOpcion);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TIPO_USUARIO_MENU_OPCION.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(TipoUsuarioMenuOpcion tipoUsuarioMenuOpcion) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
