package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Categoria;

/**
 *
 * @author MARCOS BAYONA
 */
public interface CategoriaServicio extends BaseServicio<Categoria, Long> {

}
