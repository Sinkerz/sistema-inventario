package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.ProductoSucursal;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.ProductoSucursalRepositorio;
import com.unp.sistemaalmacen.servicio.ProductoSucursalServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class ProductoSucursalServicioImpl extends BaseServicioImpl<ProductoSucursal, Long> implements ProductoSucursalServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private ProductoSucursalRepositorio productoSucursalRepositorio;

    @Autowired
    public ProductoSucursalServicioImpl(ProductoSucursalRepositorio productoSucursalRepositorio) {
        super(productoSucursalRepositorio);
    }

    @Override
    public RespuestaControlador crear(ProductoSucursal productoSucursal) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(productoSucursal);
        this.productoSucursalRepositorio.crear(productoSucursal);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.PRODUCTO_SUCURSAL.getValor(), productoSucursal.getId());
    }

    @Override
    public RespuestaControlador actualizar(ProductoSucursal productoSucursal) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(productoSucursal);
        this.productoSucursalRepositorio.actualizar(productoSucursal);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.PRODUCTO_SUCURSAL.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long productoSucursalId) {
        RespuestaControlador respuesta;
        ProductoSucursal productoSucursal;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.PRODUCTO_SUCURSAL.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            productoSucursal = productoSucursalRepositorio.obtener(productoSucursalId);
            productoSucursal.setEstado(Boolean.FALSE);
            productoSucursalRepositorio.actualizar(productoSucursal);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.PRODUCTO_SUCURSAL.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(ProductoSucursal productoSucursal) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
