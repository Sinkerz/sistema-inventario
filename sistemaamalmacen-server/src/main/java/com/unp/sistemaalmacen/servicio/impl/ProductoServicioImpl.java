package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Producto;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.ProductoRepositorio;
import com.unp.sistemaalmacen.servicio.ProductoServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class ProductoServicioImpl extends BaseServicioImpl<Producto, Long> implements ProductoServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private ProductoRepositorio productoRepositorio;

    @Autowired
    public ProductoServicioImpl(ProductoRepositorio productoRepositorio) {
        super(productoRepositorio);
    }

    @Override
    public RespuestaControlador crear(Producto producto) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(producto);
        this.productoRepositorio.crear(producto);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.PRODUCTO.getValor(), producto.getId());
    }

    @Override
    public RespuestaControlador actualizar(Producto producto) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(producto);
        this.productoRepositorio.actualizar(producto);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.PRODUCTO.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long productoId) {
        RespuestaControlador respuesta;
        Producto producto;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.PRODUCTO.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            producto = productoRepositorio.obtener(productoId);
            producto.setEstado(Boolean.FALSE);
            productoRepositorio.actualizar(producto);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.PRODUCTO.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Producto producto) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
