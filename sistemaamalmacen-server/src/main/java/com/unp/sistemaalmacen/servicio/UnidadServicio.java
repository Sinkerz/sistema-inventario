package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Unidad;

/**
 *
 * @author MARCOS BAYONA
 */
public interface UnidadServicio extends BaseServicio<Unidad, Long> {

}
