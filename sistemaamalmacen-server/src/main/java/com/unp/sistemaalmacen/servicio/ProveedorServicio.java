package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Proveedor;

/**
 *
 * @author MARCOS BAYONA
 */
public interface ProveedorServicio extends BaseServicio<Proveedor, Long> {

}
