package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.TrabajadorSucursal;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.TrabajadorSucursalRepositorio;
import com.unp.sistemaalmacen.servicio.TrabajadorSucursalServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class TrabajadorSucursalServicioImpl extends BaseServicioImpl<TrabajadorSucursal, Long> implements TrabajadorSucursalServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TrabajadorSucursalRepositorio trabajadorSucursalRepositorio;

    @Autowired
    public TrabajadorSucursalServicioImpl(TrabajadorSucursalRepositorio trabajadorSucursalRepositorio) {
        super(trabajadorSucursalRepositorio);
    }

    @Override
    public RespuestaControlador crear(TrabajadorSucursal trabajadorSucursal) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(trabajadorSucursal);
        this.trabajadorSucursalRepositorio.crear(trabajadorSucursal);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TRABAJADOR_SUCURSAL.getValor(), trabajadorSucursal.getId());
    }

    @Override
    public RespuestaControlador actualizar(TrabajadorSucursal trabajadorSucursal) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(trabajadorSucursal);
        this.trabajadorSucursalRepositorio.actualizar(trabajadorSucursal);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TRABAJADOR_SUCURSAL.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long trabajadorSucursalId) {
        RespuestaControlador respuesta;
        TrabajadorSucursal trabajadorSucursal;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TRABAJADOR_SUCURSAL.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            trabajadorSucursal = trabajadorSucursalRepositorio.obtener(trabajadorSucursalId);
            trabajadorSucursal.setEstado(Boolean.FALSE);
            trabajadorSucursalRepositorio.actualizar(trabajadorSucursal);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TRABAJADOR_SUCURSAL.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(TrabajadorSucursal trabajadorSucursal) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
