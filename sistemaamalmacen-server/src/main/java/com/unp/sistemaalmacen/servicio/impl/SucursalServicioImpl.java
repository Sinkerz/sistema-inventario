package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Sucursal;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.SucursalRepositorio;
import com.unp.sistemaalmacen.servicio.SucursalServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class SucursalServicioImpl extends BaseServicioImpl<Sucursal, Long> implements SucursalServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private SucursalRepositorio sucursalRepositorio;

    @Autowired
    public SucursalServicioImpl(SucursalRepositorio sucursalRepositorio) {
        super(sucursalRepositorio);
    }

    @Override
    public RespuestaControlador crear(Sucursal sucursal) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(sucursal);
        this.sucursalRepositorio.crear(sucursal);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.SUCURSAL.getValor(), sucursal.getId());
    }

    @Override
    public RespuestaControlador actualizar(Sucursal sucursal) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(sucursal);
        this.sucursalRepositorio.actualizar(sucursal);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.SUCURSAL.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long sucursalId) {
        RespuestaControlador respuesta;
        Sucursal sucursal;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.SUCURSAL.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            sucursal = sucursalRepositorio.obtener(sucursalId);
            sucursal.setEstado(Boolean.FALSE);
            sucursalRepositorio.actualizar(sucursal);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.SUCURSAL.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Sucursal sucursal) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
