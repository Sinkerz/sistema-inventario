package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.TipoUsuario;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.TipoUsuarioRepositorio;
import com.unp.sistemaalmacen.servicio.TipoUsuarioServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class TipoUsuarioServicioImpl extends BaseServicioImpl<TipoUsuario, Long> implements TipoUsuarioServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TipoUsuarioRepositorio tipoUsuarioRepositorio;

    @Autowired
    public TipoUsuarioServicioImpl(TipoUsuarioRepositorio tipoUsuarioRepositorio) {
        super(tipoUsuarioRepositorio);
    }

    @Override
    public RespuestaControlador crear(TipoUsuario tipoUsuario) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoUsuario);
        this.tipoUsuarioRepositorio.crear(tipoUsuario);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TIPO_USUARIO.getValor(), tipoUsuario.getId());
    }

    @Override
    public RespuestaControlador actualizar(TipoUsuario tipoUsuario) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoUsuario);
        this.tipoUsuarioRepositorio.actualizar(tipoUsuario);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TIPO_USUARIO.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long tipoUsuarioId) {
        RespuestaControlador respuesta;
        TipoUsuario tipoUsuario;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TIPO_USUARIO.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            tipoUsuario = tipoUsuarioRepositorio.obtener(tipoUsuarioId);
            tipoUsuario.setEstado(Boolean.FALSE);
            tipoUsuarioRepositorio.actualizar(tipoUsuario);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TIPO_USUARIO.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(TipoUsuario tipoUsuario) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
