package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.TipoDocumento;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.TipoDocumentoRepositorio;
import com.unp.sistemaalmacen.servicio.TipoDocumentoServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class TipoDocumentoServicioImpl extends BaseServicioImpl<TipoDocumento, Long> implements TipoDocumentoServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TipoDocumentoRepositorio tipoDocumentoRepositorio;

    @Autowired
    public TipoDocumentoServicioImpl(TipoDocumentoRepositorio tipoDocumentoRepositorio) {
        super(tipoDocumentoRepositorio);
    }

    @Override
    public RespuestaControlador crear(TipoDocumento tipoDocumento) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoDocumento);
        this.tipoDocumentoRepositorio.crear(tipoDocumento);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TIPO_DOCUMENTO.getValor(), tipoDocumento.getId());
    }

    @Override
    public RespuestaControlador actualizar(TipoDocumento tipoDocumento) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoDocumento);
        this.tipoDocumentoRepositorio.actualizar(tipoDocumento);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TIPO_DOCUMENTO.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long tipoDocumentoId) {
        RespuestaControlador respuesta;
        TipoDocumento tipoDocumento;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TIPO_DOCUMENTO.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            tipoDocumento = tipoDocumentoRepositorio.obtener(tipoDocumentoId);
            tipoDocumento.setEstado(Boolean.FALSE);
            tipoDocumentoRepositorio.actualizar(tipoDocumento);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TIPO_DOCUMENTO.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(TipoDocumento tipoDocumento) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
