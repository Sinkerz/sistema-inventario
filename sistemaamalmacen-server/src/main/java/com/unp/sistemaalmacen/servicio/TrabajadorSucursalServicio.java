package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.TrabajadorSucursal;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TrabajadorSucursalServicio extends BaseServicio<TrabajadorSucursal, Long> {

}
