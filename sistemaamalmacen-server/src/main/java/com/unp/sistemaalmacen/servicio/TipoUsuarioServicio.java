package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.TipoUsuario;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TipoUsuarioServicio extends BaseServicio<TipoUsuario, Long> {

}
