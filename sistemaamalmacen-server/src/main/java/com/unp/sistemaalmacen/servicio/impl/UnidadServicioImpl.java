package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Unidad;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.UnidadRepositorio;
import com.unp.sistemaalmacen.servicio.UnidadServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class UnidadServicioImpl extends BaseServicioImpl<Unidad, Long> implements UnidadServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private UnidadRepositorio unidadRepositorio;

    @Autowired
    public UnidadServicioImpl(UnidadRepositorio unidadRepositorio) {
        super(unidadRepositorio);
    }

    @Override
    public RespuestaControlador crear(Unidad unidad) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(unidad);
        this.unidadRepositorio.crear(unidad);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.UNIDAD.getValor(), unidad.getId());
    }

    @Override
    public RespuestaControlador actualizar(Unidad unidad) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(unidad);
        this.unidadRepositorio.actualizar(unidad);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.UNIDAD.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long unidadId) {
        RespuestaControlador respuesta;
        Unidad unidad;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.UNIDAD.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            unidad = unidadRepositorio.obtener(unidadId);
            unidad.setEstado(Boolean.FALSE);
            unidadRepositorio.actualizar(unidad);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.UNIDAD.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Unidad unidad) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
