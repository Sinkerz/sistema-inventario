package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Marca;

/**
 *
 * @author MARCOS BAYONA
 */
public interface MarcaServicio extends BaseServicio<Marca, Long> {

}
