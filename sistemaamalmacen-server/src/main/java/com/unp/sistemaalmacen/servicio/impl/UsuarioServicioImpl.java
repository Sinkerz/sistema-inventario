package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.dto.SucursalDTO;
import com.unp.sistemaalmacen.entidad.Usuario;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.UsuarioRepositorio;
import com.unp.sistemaalmacen.servicio.UsuarioServicio;
import com.unp.sistemaalmacen.util.Constantes;
import com.unp.sistemaalmacen.util.Criterio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import com.unp.sistemaalmacen.util.SistemaUtil;
import com.unp.sistemaalmacen.util.UsuarioSession;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class UsuarioServicioImpl extends BaseServicioImpl<Usuario, Long> implements UsuarioServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Autowired
    public UsuarioServicioImpl(UsuarioRepositorio usuarioRepositorio) {
        super(usuarioRepositorio);
    }

    @Override
    public RespuestaControlador crear(Usuario usuario) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(usuario);
        this.usuarioRepositorio.crear(usuario);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.USUARIO.getValor(), usuario.getId());
    }

    @Override
    public RespuestaControlador actualizar(Usuario usuario) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(usuario);
        this.usuarioRepositorio.actualizar(usuario);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.USUARIO.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long usuarioId) {
        RespuestaControlador respuesta;
        Usuario usuario;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.USUARIO.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            usuario = usuarioRepositorio.obtener(usuarioId);
            usuario.setEstado(Boolean.FALSE);
            usuarioRepositorio.actualizar(usuario);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.USUARIO.getValor());
        }

        return respuesta;
    }

    @Override
    public List<SucursalDTO> obtenerSucursalesUsuario(String login) {
        Criterio filtro = Criterio.forClass(Usuario.class);
        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
        filtro.add(Restrictions.eq("login", login));

        filtro.createAlias("trabajador", "t");
        filtro.createAlias("t.trabajadorSucursales", "ts");
        filtro.createAlias("ts.sucursal", "s");
        filtro.addOrder(Order.asc("s.nombre"));

        filtro.setProjection(Projections.projectionList()
                .add(Projections.property("s.nombre"), "nombre")
                .add(Projections.property("s.id"), "id"));

        return usuarioRepositorio.proyeccionPorCriteria(filtro, SucursalDTO.class);
    }
    
    public void validarDuplicado(Usuario usuario) throws EntidadDuplicadaExcepcion { 
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

    @Override
    public RespuestaControlador validarCredenciales(Usuario usuario, HttpServletRequest request) {
        RespuestaControlador respuesta = RespuestaControlador.obtenerRespuestaDeError(Constantes.RESPUESTA_CONTROLADOR.MENSAJE_ERROR_AUTENTICACION);
        Criterio filtro = Criterio.forClass(Usuario.class);
        filtro.add(Restrictions.eq("login", usuario.getLogin()));
        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
        filtro.add(Restrictions.eq("clave", usuario.getClave()));
        filtro.createAlias("trabajador", "t");
        filtro.createAlias("t.persona", "p");
        filtro.createAlias("t.trabajadorSucursales", "ts");
        filtro.createAlias("ts.sucursal", "s");
        filtro.add(Restrictions.eq("s.id", usuario.getSucursalId()));

        filtro.setProjection(Projections.projectionList()
                .add(Projections.property("s.nombre"), "sucursal")
                .add(Projections.property("login"), "login")
                .add(Projections.property("s.serie"), "serie")
                .add(Projections.property("id"), "id")
                .add(Projections.property("p.nombre"), "nombre")
                .add(Projections.property("p.apellidos"), "apellidos")
                .add(Projections.property("s.id"), "sucursalId"));

        UsuarioSession usuarioSession = (UsuarioSession) usuarioRepositorio.obtenerConResultSet(filtro, UsuarioSession.class);
        if (SistemaUtil.esNoNulo(usuarioSession)) {
            HttpSession session = request.getSession(true);
            usuarioSession.setToken(session.getId());

            UsuarioSession usuarioSessionSave = UsuarioSession.clonar(usuarioSession);
            usuarioSessionSave.setIp(SistemaUtil.getClientIpAddr(request));
            if (SistemaUtil.esNulo(usuarioSessionSave.getPc())) {
                usuarioSessionSave.setPc(request.getRemoteHost());
                usuarioSession.setPc("");
            }
            session.setAttribute("usuario", usuarioSessionSave);
            respuesta = RespuestaControlador.obtenerRespuestaExitoConData(usuarioSession);
        }
        return respuesta;
    }

}

