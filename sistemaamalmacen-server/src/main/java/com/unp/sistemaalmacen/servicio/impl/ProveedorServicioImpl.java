package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Proveedor;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.ProveedorRepositorio;
import com.unp.sistemaalmacen.servicio.ProveedorServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class ProveedorServicioImpl extends BaseServicioImpl<Proveedor, Long> implements ProveedorServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private ProveedorRepositorio proveedorRepositorio;

    @Autowired
    public ProveedorServicioImpl(ProveedorRepositorio proveedorRepositorio) {
        super(proveedorRepositorio);
    }

    @Override
    public RespuestaControlador crear(Proveedor proveedor) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(proveedor);
        this.proveedorRepositorio.crear(proveedor);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.PROVEEDOR.getValor(), proveedor.getId());
    }

    @Override
    public RespuestaControlador actualizar(Proveedor proveedor) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(proveedor);
        this.proveedorRepositorio.actualizar(proveedor);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.PROVEEDOR.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long proveedorId) {
        RespuestaControlador respuesta;
        Proveedor proveedor;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.PROVEEDOR.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            proveedor = proveedorRepositorio.obtener(proveedorId);
            proveedor.setEstado(Boolean.FALSE);
            proveedorRepositorio.actualizar(proveedor);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.PROVEEDOR.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Proveedor proveedor) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
