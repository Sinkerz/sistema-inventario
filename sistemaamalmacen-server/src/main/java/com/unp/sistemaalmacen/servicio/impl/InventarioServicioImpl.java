package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Inventario;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.InventarioRepositorio;
import com.unp.sistemaalmacen.servicio.InventarioServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class InventarioServicioImpl extends BaseServicioImpl<Inventario, Long> implements InventarioServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private InventarioRepositorio inventarioRepositorio;

    @Autowired
    public InventarioServicioImpl(InventarioRepositorio inventarioRepositorio) {
        super(inventarioRepositorio);
    }

    @Override
    public RespuestaControlador crear(Inventario inventario) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(inventario);
        this.inventarioRepositorio.crear(inventario);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.INVENTARIO.getValor(), inventario.getId());
    }

    @Override
    public RespuestaControlador actualizar(Inventario inventario) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(inventario);
        this.inventarioRepositorio.actualizar(inventario);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.INVENTARIO.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long inventarioId) {
        RespuestaControlador respuesta;
        Inventario inventario;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.INVENTARIO.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            inventario = inventarioRepositorio.obtener(inventarioId);
            inventario.setEstado(Boolean.FALSE);
            inventarioRepositorio.actualizar(inventario);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.INVENTARIO.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Inventario inventario) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}