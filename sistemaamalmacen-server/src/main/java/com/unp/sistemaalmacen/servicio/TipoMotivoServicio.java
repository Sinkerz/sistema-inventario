package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.TipoMotivo;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TipoMotivoServicio extends BaseServicio<TipoMotivo, Long> {

}
