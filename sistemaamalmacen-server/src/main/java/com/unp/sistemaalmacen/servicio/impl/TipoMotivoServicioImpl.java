package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.TipoMotivo;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.TipoMotivoRepositorio;
import com.unp.sistemaalmacen.servicio.TipoMotivoServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class TipoMotivoServicioImpl extends BaseServicioImpl<TipoMotivo, Long> implements TipoMotivoServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TipoMotivoRepositorio tipoMotivoRepositorio;

    @Autowired
    public TipoMotivoServicioImpl(TipoMotivoRepositorio tipoMotivoRepositorio) {
        super(tipoMotivoRepositorio);
    }

    @Override
    public RespuestaControlador crear(TipoMotivo tipoMotivo) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoMotivo);
        this.tipoMotivoRepositorio.crear(tipoMotivo);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TIPO_MOTIVO.getValor(), tipoMotivo.getId());
    }

    @Override
    public RespuestaControlador actualizar(TipoMotivo tipoMotivo) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoMotivo);
        this.tipoMotivoRepositorio.actualizar(tipoMotivo);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TIPO_MOTIVO.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long tipoMotivoId) {
        RespuestaControlador respuesta;
        TipoMotivo tipoMotivo;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TIPO_MOTIVO.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            tipoMotivo = tipoMotivoRepositorio.obtener(tipoMotivoId);
            tipoMotivo.setEstado(Boolean.FALSE);
            tipoMotivoRepositorio.actualizar(tipoMotivo);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TIPO_MOTIVO.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(TipoMotivo tipoMotivo) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
