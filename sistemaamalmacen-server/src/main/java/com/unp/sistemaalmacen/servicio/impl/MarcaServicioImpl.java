package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Marca;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.MarcaRepositorio;
import com.unp.sistemaalmacen.servicio.MarcaServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class MarcaServicioImpl extends BaseServicioImpl<Marca, Long> implements MarcaServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private MarcaRepositorio marcaRepositorio;

    @Autowired
    public MarcaServicioImpl(MarcaRepositorio marcaRepositorio) {
        super(marcaRepositorio);
    }

    @Override
    public RespuestaControlador crear(Marca marca) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(marca);
        this.marcaRepositorio.crear(marca);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.MARCA.getValor(), marca.getId());
    }

    @Override
    public RespuestaControlador actualizar(Marca marca) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(marca);
        this.marcaRepositorio.actualizar(marca);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.MARCA.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long marcaId) {
        RespuestaControlador respuesta;
        Marca marca;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.MARCA.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            marca = marcaRepositorio.obtener(marcaId);
            marca.setEstado(Boolean.FALSE);
            marcaRepositorio.actualizar(marca);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.MARCA.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Marca marca) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
