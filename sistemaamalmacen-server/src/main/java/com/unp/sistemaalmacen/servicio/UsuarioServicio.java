package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.dto.SucursalDTO;
import com.unp.sistemaalmacen.entidad.Usuario;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author MARCOS BAYONA
 */
public interface UsuarioServicio extends BaseServicio<Usuario, Long> {

    public List<SucursalDTO> obtenerSucursalesUsuario(String login);
    
    public RespuestaControlador validarCredenciales(Usuario usario, HttpServletRequest request);
    
}
