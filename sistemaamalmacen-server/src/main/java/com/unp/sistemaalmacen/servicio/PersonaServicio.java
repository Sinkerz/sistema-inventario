package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Persona;

/**
 *
 * @author MARCOS BAYONA
 */
public interface PersonaServicio extends BaseServicio<Persona, Long> {

}
