package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Categoria;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.CategoriaRepositorio;
import com.unp.sistemaalmacen.servicio.CategoriaServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class CategoriaServicioImpl extends BaseServicioImpl<Categoria, Long> implements CategoriaServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private CategoriaRepositorio categoriaRepositorio;

    @Autowired
    public CategoriaServicioImpl(CategoriaRepositorio categoriaRepositorio) {
        super(categoriaRepositorio);
    }

    @Override
    public RespuestaControlador crear(Categoria categoria) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(categoria);
        this.categoriaRepositorio.crear(categoria);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.CATEGORIA.getValor(), categoria.getId());
    }

    @Override
    public RespuestaControlador actualizar(Categoria categoria) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(categoria);
        this.categoriaRepositorio.actualizar(categoria);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.CATEGORIA.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long categoriaId) {
        RespuestaControlador respuesta;
        Categoria categoria;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.CATEGORIA.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            categoria = categoriaRepositorio.obtener(categoriaId);
            categoria.setEstado(Boolean.FALSE);
            categoriaRepositorio.actualizar(categoria);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.CATEGORIA.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Categoria categoria) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
