package com.unp.sistemaalmacen.servicio.impl;

import com.unp.sistemaalmacen.entidad.Trabajador;
import com.unp.sistemaalmacen.enums.NombreEntidad;
import com.unp.sistemaalmacen.excepcion.EntidadDuplicadaExcepcion;
import com.unp.sistemaalmacen.repositorio.TrabajadorRepositorio;
import com.unp.sistemaalmacen.servicio.TrabajadorServicio;
import com.unp.sistemaalmacen.util.RespuestaControlador;
import com.unp.sistemaalmacen.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author MARCOS BAYONA
 */
@Service
public class TrabajadorServicioImpl extends BaseServicioImpl<Trabajador, Long> implements TrabajadorServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TrabajadorRepositorio trabajadorRepositorio;

    @Autowired
    public TrabajadorServicioImpl(TrabajadorRepositorio trabajadorRepositorio) {
        super(trabajadorRepositorio);
    }

    @Override
    public RespuestaControlador crear(Trabajador trabajador) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(trabajador);
        this.trabajadorRepositorio.crear(trabajador);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TRABAJADOR.getValor(), trabajador.getId());
    }

    @Override
    public RespuestaControlador actualizar(Trabajador trabajador) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(trabajador);
        this.trabajadorRepositorio.actualizar(trabajador);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TRABAJADOR.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long trabajadorId) {
        RespuestaControlador respuesta;
        Trabajador trabajador;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TRABAJADOR.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            trabajador = trabajadorRepositorio.obtener(trabajadorId);
            trabajador.setEstado(Boolean.FALSE);
            trabajadorRepositorio.actualizar(trabajador);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TRABAJADOR.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Trabajador trabajador) throws EntidadDuplicadaExcepcion { // Excepciòn de entidad duplicada
//        Criterio filtro = Criterio.forClass(Alumno.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("persona.id", proveedor.getPersona().getId()));
//
//        // Si es una actualizacion
//        if (proveedor.getId() != null) {
//            filtro.add(Restrictions.ne("id", proveedor.getId()));
//        }
//        if (alumnoRepositorio.cantidadPorCriteri(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya existe otro " + NombreEntidad.ALUMNO.getValor() + " asignado a la misma persona.");
//        }
    }

}
