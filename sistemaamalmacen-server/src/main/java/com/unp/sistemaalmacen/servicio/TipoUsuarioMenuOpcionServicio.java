package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.TipoUsuarioMenuOpcion;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TipoUsuarioMenuOpcionServicio extends BaseServicio<TipoUsuarioMenuOpcion, Long> {

}
