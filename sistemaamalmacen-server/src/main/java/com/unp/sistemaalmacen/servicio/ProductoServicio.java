package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Producto;

/**
 *
 * @author MARCOS BAYONA
 */
public interface ProductoServicio extends BaseServicio<Producto, Long> {

}
