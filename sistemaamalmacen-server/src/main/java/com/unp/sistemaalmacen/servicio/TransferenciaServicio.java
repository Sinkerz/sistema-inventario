package com.unp.sistemaalmacen.servicio;

import com.unp.sistemaalmacen.entidad.Transferencia;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TransferenciaServicio extends BaseServicio<Transferencia, Long> {

}
