package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Transferencia;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TransferenciaRepositorio extends BaseRepositorio<Transferencia, Long> {

}
