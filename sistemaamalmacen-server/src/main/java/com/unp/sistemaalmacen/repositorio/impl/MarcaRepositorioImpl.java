package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Marca;
import com.unp.sistemaalmacen.repositorio.MarcaRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class MarcaRepositorioImpl extends BaseRepositorioImpl<Marca, Long> implements MarcaRepositorio {

}
