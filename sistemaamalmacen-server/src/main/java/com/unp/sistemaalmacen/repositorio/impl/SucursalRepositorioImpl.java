package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Sucursal;
import com.unp.sistemaalmacen.repositorio.SucursalRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class SucursalRepositorioImpl extends BaseRepositorioImpl<Sucursal, Long> implements SucursalRepositorio {

}
