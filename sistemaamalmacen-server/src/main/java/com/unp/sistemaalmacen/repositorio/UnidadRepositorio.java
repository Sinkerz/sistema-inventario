package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Unidad;

/**
 *
 * @author MARCOS BAYONA
 */
public interface UnidadRepositorio extends BaseRepositorio<Unidad, Long> {

}
