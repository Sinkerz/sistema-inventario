package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Persona;
import com.unp.sistemaalmacen.repositorio.PersonaRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class PersonaRepositorioImpl extends BaseRepositorioImpl<Persona, Long> implements PersonaRepositorio {

}
