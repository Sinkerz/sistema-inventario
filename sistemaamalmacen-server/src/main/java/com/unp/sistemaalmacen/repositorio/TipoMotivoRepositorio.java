package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.TipoMotivo;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TipoMotivoRepositorio extends BaseRepositorio<TipoMotivo, Long> {

}
