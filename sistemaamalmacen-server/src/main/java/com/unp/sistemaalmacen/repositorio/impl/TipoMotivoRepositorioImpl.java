package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.TipoMotivo;
import com.unp.sistemaalmacen.repositorio.TipoMotivoRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class TipoMotivoRepositorioImpl extends BaseRepositorioImpl<TipoMotivo, Long> implements TipoMotivoRepositorio {

}
