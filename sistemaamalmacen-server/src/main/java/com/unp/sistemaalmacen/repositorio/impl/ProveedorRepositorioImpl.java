package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Proveedor;
import com.unp.sistemaalmacen.repositorio.ProveedorRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class ProveedorRepositorioImpl extends BaseRepositorioImpl<Proveedor, Long> implements ProveedorRepositorio {

}
