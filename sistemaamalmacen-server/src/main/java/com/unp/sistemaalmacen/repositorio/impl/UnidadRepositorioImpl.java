package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Unidad;
import com.unp.sistemaalmacen.repositorio.UnidadRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class UnidadRepositorioImpl extends BaseRepositorioImpl<Unidad, Long> implements UnidadRepositorio {

}
