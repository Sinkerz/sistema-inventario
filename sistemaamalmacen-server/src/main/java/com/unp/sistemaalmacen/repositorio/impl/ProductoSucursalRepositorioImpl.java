package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.ProductoSucursal;
import com.unp.sistemaalmacen.repositorio.ProductoSucursalRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class ProductoSucursalRepositorioImpl extends BaseRepositorioImpl<ProductoSucursal, Long> implements ProductoSucursalRepositorio {

}
