package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Proveedor;

/**
 *
 * @author MARCOS BAYONA
 */
public interface ProveedorRepositorio extends BaseRepositorio<Proveedor, Long> {

}
