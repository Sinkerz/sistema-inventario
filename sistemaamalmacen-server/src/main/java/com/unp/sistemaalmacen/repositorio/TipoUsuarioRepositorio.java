package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.TipoUsuario;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TipoUsuarioRepositorio extends BaseRepositorio<TipoUsuario, Long> {

}
