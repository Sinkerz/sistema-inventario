package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.TipoDocumento;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TipoDocumentoRepositorio extends BaseRepositorio<TipoDocumento, Long> {

}
