package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Marca;

/**
 *
 * @author MARCOS BAYONA
 */
public interface MarcaRepositorio extends BaseRepositorio<Marca, Long> {

}
