package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Trabajador;
import com.unp.sistemaalmacen.repositorio.TrabajadorRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class TrabajadorRepositorioImpl extends BaseRepositorioImpl<Trabajador, Long> implements TrabajadorRepositorio {

}
