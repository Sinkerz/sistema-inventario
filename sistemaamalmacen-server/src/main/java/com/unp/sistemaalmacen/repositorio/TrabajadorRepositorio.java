package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Trabajador;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TrabajadorRepositorio extends BaseRepositorio<Trabajador, Long> {

}
