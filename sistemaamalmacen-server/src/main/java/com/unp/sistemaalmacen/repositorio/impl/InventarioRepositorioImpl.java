package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Inventario;
import com.unp.sistemaalmacen.repositorio.InventarioRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class InventarioRepositorioImpl extends BaseRepositorioImpl<Inventario, Long> implements InventarioRepositorio {

}
