package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Producto;

/**
 *
 * @author MARCOS BAYONA
 */
public interface ProductoRepositorio extends BaseRepositorio<Producto, Long> {

}
