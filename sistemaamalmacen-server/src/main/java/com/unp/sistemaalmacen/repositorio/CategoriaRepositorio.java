package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Categoria;

/**
 *
 * @author MARCOS BAYONA
 */
public interface CategoriaRepositorio extends BaseRepositorio<Categoria, Long> {

}
