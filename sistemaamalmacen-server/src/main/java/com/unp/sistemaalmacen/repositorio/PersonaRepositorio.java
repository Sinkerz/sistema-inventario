package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Persona;

/**
 *
 * @author MARCOS BAYONA
 */
public interface PersonaRepositorio extends BaseRepositorio<Persona, Long> {

}
