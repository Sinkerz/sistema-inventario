package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Usuario;

/**
 *
 * @author MARCOS BAYONA
 */
public interface UsuarioRepositorio extends BaseRepositorio<Usuario, Long> {
    
}
