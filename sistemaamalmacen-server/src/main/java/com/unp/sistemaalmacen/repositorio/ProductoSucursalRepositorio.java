package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.ProductoSucursal;

/**
 *
 * @author MARCOS BAYONA
 */
public interface ProductoSucursalRepositorio extends BaseRepositorio<ProductoSucursal, Long> {

}
