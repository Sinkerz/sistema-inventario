package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Categoria;
import com.unp.sistemaalmacen.repositorio.CategoriaRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class CategoriaRepositorioImpl extends BaseRepositorioImpl<Categoria, Long> implements CategoriaRepositorio {

}
