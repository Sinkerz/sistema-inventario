package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.TrabajadorSucursal;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TrabajadorSucursalRepositorio extends BaseRepositorio<TrabajadorSucursal, Long> {

}
