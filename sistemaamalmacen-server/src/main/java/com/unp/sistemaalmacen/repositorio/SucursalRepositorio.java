package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Sucursal;

/**
 *
 * @author MARCOS BAYONA
 */
public interface SucursalRepositorio extends BaseRepositorio<Sucursal, Long> {

}
