package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Usuario;
import com.unp.sistemaalmacen.repositorio.UsuarioRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class UsuarioRepositorioImpl extends BaseRepositorioImpl<Usuario, Long> implements UsuarioRepositorio {

}
