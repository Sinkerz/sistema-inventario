package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.TipoUsuarioMenuOpcion;
import com.unp.sistemaalmacen.repositorio.TipoUsuarioMenuOpcionRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class TipoUsuarioMenuOpcionRepositorioImpl extends BaseRepositorioImpl<TipoUsuarioMenuOpcion, Long> implements TipoUsuarioMenuOpcionRepositorio {

}
