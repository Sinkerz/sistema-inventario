package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Producto;
import com.unp.sistemaalmacen.repositorio.ProductoRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class ProductoRepositorioImpl extends BaseRepositorioImpl<Producto, Long> implements ProductoRepositorio {

}
