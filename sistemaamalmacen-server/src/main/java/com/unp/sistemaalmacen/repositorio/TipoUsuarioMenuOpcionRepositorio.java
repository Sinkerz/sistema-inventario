package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.TipoUsuarioMenuOpcion;

/**
 *
 * @author MARCOS BAYONA
 */
public interface TipoUsuarioMenuOpcionRepositorio extends BaseRepositorio<TipoUsuarioMenuOpcion, Long> {

}
