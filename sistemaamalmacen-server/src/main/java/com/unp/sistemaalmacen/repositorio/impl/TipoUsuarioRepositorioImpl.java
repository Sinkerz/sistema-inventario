package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.TipoUsuario;
import com.unp.sistemaalmacen.repositorio.TipoUsuarioRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class TipoUsuarioRepositorioImpl extends BaseRepositorioImpl<TipoUsuario, Long> implements TipoUsuarioRepositorio {

}
