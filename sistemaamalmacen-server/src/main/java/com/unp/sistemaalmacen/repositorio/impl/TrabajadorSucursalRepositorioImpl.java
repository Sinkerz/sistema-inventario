package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.TrabajadorSucursal;
import com.unp.sistemaalmacen.repositorio.TrabajadorSucursalRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class TrabajadorSucursalRepositorioImpl extends BaseRepositorioImpl<TrabajadorSucursal, Long> implements TrabajadorSucursalRepositorio {

}
