package com.unp.sistemaalmacen.repositorio;

import com.unp.sistemaalmacen.entidad.Inventario;

/**
 *
 * @author MARCOS BAYONA
 */
public interface InventarioRepositorio extends BaseRepositorio<Inventario, Long> {

}
