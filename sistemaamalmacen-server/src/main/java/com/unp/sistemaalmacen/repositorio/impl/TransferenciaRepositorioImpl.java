package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.Transferencia;
import com.unp.sistemaalmacen.repositorio.TransferenciaRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class TransferenciaRepositorioImpl extends BaseRepositorioImpl<Transferencia, Long> implements TransferenciaRepositorio {

}
