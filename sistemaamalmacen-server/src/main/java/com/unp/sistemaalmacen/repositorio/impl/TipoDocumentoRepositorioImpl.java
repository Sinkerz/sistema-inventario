package com.unp.sistemaalmacen.repositorio.impl;

import com.unp.sistemaalmacen.entidad.TipoDocumento;
import com.unp.sistemaalmacen.repositorio.TipoDocumentoRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MARCOS BAYONA
 */
@Repository
public class TipoDocumentoRepositorioImpl extends BaseRepositorioImpl<TipoDocumento, Long> implements TipoDocumentoRepositorio {

}
