package com.unp.sistemaalmacen.configuracion;

import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.unp.sistemaalmacen.util.AutenticacionInterceptor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author MARCOS BAYONA
 */
@Configuration
@AutoConfigureAfter(DispatcherServletAutoConfiguration.class)
public class CustomWebMvcAutoConfig extends WebMvcConfigurerAdapter {
    
    @Autowired
    AutenticacionInterceptor autenticacionInterceptor;

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.stream().filter((converter) -> (converter instanceof org.springframework.http.converter.json.MappingJackson2HttpMessageConverter)).map((converter) -> ((MappingJackson2HttpMessageConverter) converter).getObjectMapper()).forEachOrdered((mapper) -> {
            Hibernate5Module hm = new Hibernate5Module();
            hm.configure(Hibernate5Module.Feature.FORCE_LAZY_LOADING, false); // Cuando un atributo que es Entidad no tenga data, no se muestra
            mapper.registerModule(hm);
        });
    }
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(autenticacionInterceptor).addPathPatterns("/server/**")
                .excludePathPatterns("/server/usuario/logeo")
                .excludePathPatterns("/server/usuario/sucursal/**");
    }
}
