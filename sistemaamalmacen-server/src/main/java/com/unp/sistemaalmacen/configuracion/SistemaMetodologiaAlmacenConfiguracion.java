package com.unp.sistemaalmacen.configuracion;

import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author MARCOS BAYONA
 */
@Configuration
@EnableTransactionManagement
@EntityScan(basePackages = {"com.unp.sistemaalmacen.entidad"})
@EnableAutoConfiguration
public class SistemaMetodologiaAlmacenConfiguracion {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private Environment environment;

    @Bean
    public LocalSessionFactoryBean sessionFactory() throws Exception {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(this.dataSource);
        sessionFactoryBean.setPackagesToScan("com.unp.sistemaalmacen.entidad");
        sessionFactoryBean.setHibernateProperties(getHibernateProperties());
        return sessionFactoryBean;
    }

    @Bean
    Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", this.environment.getProperty("spring.jpa.properties.hibernate.dialect"));
        properties.setProperty("hibernate.show_sql", this.environment.getProperty("spring.jpa.properties.hibernate.show_sql"));
        properties.setProperty("hibernate.format_sql", this.environment.getProperty("spring.jpa.properties.hibernate.format_sql"));
        properties.setProperty("hibernate.id.new_generator_mappings", this.environment.getProperty("spring.jpa.properties.hibernate.id.new_generator_mappings"));
        return properties;
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }

}
